//
//  OrderListTransfered.m
//  KDS
//
//  Created by Tiseno on 11/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OrderListTransfered.h"



@implementation OrderListTransfered
 
@synthesize _tableViewHeader, _tableView, newssOrdersNotTransferedArr,loadingView,ssalesOrder;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self reloadSalesOrderArray];
    }
    return self;
}

-(BOOL)NetworkStatus
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus==NotReachable);
}

- (void)dealloc
{
    [newssOrdersNotTransferedArr release];
    [_tableView release];
    [loadingView release];
    [super dealloc];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.newssOrdersNotTransferedArr!=nil)
    {
        return [self.newssOrdersNotTransferedArr count];
    }
    return 0;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    OrderListTransferedCellViewController *cell = (OrderListTransferedCellViewController*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    SalesOrder* salesOrder=[self.newssOrdersNotTransferedArr objectAtIndex:indexPath.row];
    if(cell == nil)
    {
        cell = [[[OrderListTransferedCellViewController alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier]autorelease];
        //cell.salesOrder=salesOrder;
        cell.lineColor = [UIColor blackColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.customerNameLabel.text = salesOrder.customer.Customer_Name;
        NSString* grandPrice = [NSString stringWithFormat:@"RM %.2f", salesOrder.grand_Price];
        cell.priceLabel.text = grandPrice;
    }
    return cell; 
}
-(void)handleRecievedResponseMessage:(XMLResponse *)responseMessage
{    
    if(responseMessage!=nil && [responseMessage isKindOfClass:[NewOrderTransferResponse class]])
    {
        NSString *feedBackResult=((NewOrderTransferResponse*)responseMessage).getOrderResponse;
        NSLog(@"%@",feedBackResult);
        
        if ([feedBackResult isEqualToString:@"okay"]) 
        {
            KDSDataSalesOrder* dataSalesOrder=[[KDSDataSalesOrder alloc] init];
            [dataSalesOrder openConnection];
            dataSalesOrder.runBatch=YES;
            for (SalesOrder* salesOrder in self.newssOrdersNotTransferedArr) {
                [dataSalesOrder markSalesOrderAsUploaded:salesOrder];
            }
            [dataSalesOrder closeConnection];
            [dataSalesOrder release];
            [self reloadSalesOrderArray];
            [_tableView reloadData];
        }else {
            NSLog(@"server down2~");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Server Down. Please try again later. Thank you." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];

        }

    }else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Server Down. Please try again later. Thank you." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
        
        NSLog(@"server down1~");
    }
}

-(IBAction)transferNowButton_Tapped
{    
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. \n Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else if ([appDelegate.salesOrder order_Items] != nil) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Please clear your cart. \n Thank you." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }
    else
    { 
        UIView *selfView=self.view;
        LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
        self.loadingView=temploadingView;
        
        KDSDataSalesOrderItem* dataSalesOrderItem=[[KDSDataSalesOrderItem alloc] init];
        KDSAppDelegate* appdelegate=[UIApplication sharedApplication].delegate;
        
        for (SalesOrder* salesOrder in self.newssOrdersNotTransferedArr) 
        { 
            NSArray* orderItemArr=[dataSalesOrderItem selectOrderItemsForOrder:salesOrder];
            if([salesOrder NumberOfOderItems]==0)
            {
                for(SalesOrderItem* orderItem in orderItemArr)
                {
                    [salesOrder addOrderItem:orderItem];
                }
            } 
            NewOrderTransferRequest* request=[[NewOrderTransferRequest alloc] initWithSalesOrder:salesOrder SalesPersonCode:appdelegate.loggedInSalesPerson.SalesPerson_Code];
            NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
            [networkHandler setDelegate:self];
            [networkHandler request:request];
            [request release];
            [networkHandler release];
        }
        [dataSalesOrderItem release];
        
        /*KDSDataSalesOrder* dataSalesOrder=[[KDSDataSalesOrder alloc] init];
        [dataSalesOrder openConnection];
        dataSalesOrder.runBatch=YES;
        for (SalesOrder* salesOrder in self.newOrdersNotTransferedArr) {
            [dataSalesOrder markSalesOrderAsUploaded:salesOrder];
        }
        [dataSalesOrder closeConnection];
        [dataSalesOrder release];
        [self reloadSalesOrderArray];
        [_tableView reloadData];*/
        
        [self.loadingView removeView];
    }
    
}

-(void)TransferSaleOrderButton;
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }else 
    {
        //-(id)initWithSalesOrder:(SalesOrder*)isaleOrder SalesPersonCode:(NSString*)isalesPersonCode;
        
        /*TransferOrderPdfRequest* request=[[TransferOrderPdfRequest alloc] initWithSalesOrder:isaleOrder SalesPersonTransfer:isalesPersonCode];
        NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
        [networkHandler setDelegate:self];
        [networkHandler request:request];
        [request release];
        [networkHandler release]; 
        
        UIView *selfView=self.view;
        LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
        self.loadingView=temploadingView;
        
        KDSDataSalesOrderItem* dataSalesOrderItem=[[KDSDataSalesOrderItem alloc] init];
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        NSArray* orderItemArr=[dataSalesOrderItem selectOrderItemsForOrder:self.ssalesOrder];
        
        [dataSalesOrderItem release];
        
        //if([self.salesOrder NumberOfOderItems]==0)
        //{
        for(SalesOrderItem* orderItem in orderItemArr)
        {
            [self.ssalesOrder addOrderItem:orderItem];
        } 
        
        //NSLog(@"pdf tapped!!");
        
        appDelegate.salesOrder=self.ssalesOrder;
        appDelegate.currentCustomer=self.ssalesOrder.customer;
        
        NSLog(@"%d", self.ssalesOrder.Order_id);*/
        NSLog(@"%@", self.ssalesOrder.OrderDescription);

        
        NSLog(@"hello~%@", self.ssalesOrder.OrderDescription);
    } 
    
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(_tableViewHeader == nil)
    {
        if(_tableViewHeader == nil)
        {
            self._tableView.rowHeight = 45;
            UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y-30, self._tableView.frame.size.width, 30)];
            tableHeaderView.backgroundColor = [UIColor colorWithRed:0.0196 green:0.513 blue:0.949 alpha:1.0];
            
            UILabel *tableHeaderCustomerNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 4, 653, 21)];
            tableHeaderCustomerNameLabel.text = @"Customer Name";
            tableHeaderCustomerNameLabel.backgroundColor = [UIColor clearColor];
            tableHeaderCustomerNameLabel.textColor = [UIColor whiteColor];
            tableHeaderCustomerNameLabel.textAlignment = UITextAlignmentLeft;
            tableHeaderCustomerNameLabel.font=[UIFont boldSystemFontOfSize:17];
            [tableHeaderView addSubview:tableHeaderCustomerNameLabel];
            [tableHeaderCustomerNameLabel release];
            
            UILabel *tableHeaderPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(653, 4, 191, 21)];
            tableHeaderPriceLabel.text = @"Amount";
            tableHeaderPriceLabel.backgroundColor = [UIColor clearColor];
            tableHeaderPriceLabel.textColor = [UIColor whiteColor];
            tableHeaderPriceLabel.textAlignment = UITextAlignmentLeft;
            tableHeaderPriceLabel.font=[UIFont boldSystemFontOfSize:17];
            [tableHeaderView addSubview:tableHeaderPriceLabel];
            [tableHeaderPriceLabel release];
            
            
            
            _tableViewHeader = tableHeaderView;
            [self.view addSubview:_tableViewHeader];
            [tableHeaderView release];
            
            
            
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(void)reloadSalesOrderArray
{ 
    KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    KDSDataSalesOrder *dataSalesOrder=[[KDSDataSalesOrder alloc] init];
    NSArray* tNewSalesOrderArr=[dataSalesOrder selectSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:NO IsDeleted:NO];
    self.newssOrdersNotTransferedArr=tNewSalesOrderArr;
    [dataSalesOrder release];
}
@end
