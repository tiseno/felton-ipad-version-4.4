//
//  NewCustomerListViewController.h
//  KDS
//
//  Created by Tiseno Mac 2 on 2/6/13.
//
//

#import <UIKit/UIKit.h>
#import "CustomerListCell.h"
#import "Customer.h"
#import "KDSAppDelegate.h"
#import "KDSDataCustomer.h"

@interface NewCustomerListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>{
    
}

@property (nonatomic, retain) NSMutableArray *customerArray_Copied;
@property (retain, nonatomic) IBOutlet UITableView *companyTableView;
@property (nonatomic, retain) NSArray *customerArray;
@property (nonatomic, retain) UIButton *tOrderHistoryButton;
@end
