//
//  OrderPackagingRequest.h
//  KDS
//
//  Created by Tiseno Mac 2 on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLRequest.h"
#import "KDSAppDelegate.h"
@interface OrderPackagingRequest : XMLRequest{
    
}
@property (nonatomic,retain) NSString *SalespersonID;
@property (nonatomic,retain) NSString *datestart;
@property (nonatomic,retain) NSString *dateend;

//-(id)initWithItemTypes:(NSString*)iItemType;
-(id)initWithSalespersonID:(NSString*)isaleOrderID Salesdatestart:(NSString*)isalesdatestart Salesdateend:(NSString*)isalesdateend;

@end
