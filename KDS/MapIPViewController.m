//
//  MapIPViewController.m
//  KDS
//
//  Created by Tiseno Mac 2 on 2/19/13.
//
//

#import "MapIPViewController.h"

@interface MapIPViewController ()

@end

@implementation MapIPViewController
@synthesize txtMapip;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)dealloc
{
    [txtMapip release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    MapIPViewController *gMapIPViewController=[[[MapIPViewController alloc] initWithNibName:@"MapIPViewController" bundle:nil] autorelease];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"btn_close.png"] forState:UIControlStateNormal];
    rightButton.frame = CGRectMake(0, 0, 62, 33);
    [rightButton addTarget:self action:@selector(DoneTapped:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView: rightButton] autorelease];
    gMapIPViewController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    NSError *error;
    NSString *strhost=[appDelegate loadHostIntoString];
    NSString *settingFileContents=[NSString stringWithContentsOfFile:strhost encoding:NSASCIIStringEncoding error:&error];
    txtMapip.text=settingFileContents;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) DoneTapped:(id)gesture
{

    [self.navigationController dismissModalViewControllerAnimated:YES];
    
}

- (void)viewDidUnload
{
    [self setTxtMapip:nil];
    [super viewDidUnload];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    heightOfEditedView = textField.frame.size.height;
    heightOffset=textField.frame.origin.y+120;
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 400-(heightOffset+ heightOfEditedView), self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
    
}

-(void)keyboardWillHide:(NSNotification*)n
{
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [UIView beginAnimations:@"" context:nil];
    
    [UIView setAnimationDuration:0.2];
    
    self.view.frame = rectToShow;
    
    [UIView commitAnimations];
}

/*================host========================*/
-(IBAction)saveHost
{
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    [appDelegate saveHost:txtMapip.text];
    
    [self.navigationController dismissModalViewControllerAnimated:YES];
}

@end
