//
//  DownloadItemImagesRequestblind.m
//  KDS
//
//  Created by Tiseno Mac 2 on 5/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DownloadItemImagesRequestblind.h"

@implementation DownloadItemImagesRequestblind
@synthesize ItemType;

-(id)initWithItemTypes:(NSString*)iItemType;
{
    self=[super init];
    if(self)
    { 
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        NSError *error;
        NSString *strhost=[appDelegate loadHostIntoString];
        NSString *settingFileContents=[NSString stringWithContentsOfFile:strhost encoding:NSASCIIStringEncoding error:&error];
        NSString *HostIP = [NSString stringWithFormat:@"http://%@/KDSItemImage/service.asmx",settingFileContents];

        self.webserviceURL=HostIP;
        
        self.SOAPAction=@"getItemImage";
        self.requestType=WebserviceRequest;
        self.ItemType=iItemType;
    }
    return self;
}

-(void)dealloc
{
    [ItemType release];
    
    [super dealloc]; 
}

-(NSString*) generateHTTPPostMessage
{
    NSString *postMsg = [NSString stringWithFormat:@"ItemType=%@",self.ItemType];
    
    return postMsg;
    
}
@end
