//
//  ActiveCasemainItemsRequest.m
//  KDS
//
//  Created by Tiseno Mac 2 on 5/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ActiveCasemainItemsRequest.h"

@implementation ActiveCasemainItemsRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        NSError *error;
        NSString *strhost=[appDelegate loadHostIntoString];
        NSString *settingFileContents=[NSString stringWithContentsOfFile:strhost encoding:NSASCIIStringEncoding error:&error];
        NSString *HostIP = [NSString stringWithFormat:@"http://%@/KDSMainItem/service.asmx",settingFileContents];
        
        self.webserviceURL=HostIP;
        
        self.SOAPAction=@"GetActiveMainItem";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{
    return @"";
}
@end
