//
//  AsyncImageView.h
//  Hobbstyle2
//
//  Created by Jermin Bazazian on 1/14/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AsyncImageViewDelegate.h"

@interface AsyncImageView : UIView {
	NSURLConnection* connection;
    NSMutableData* data;
	id<AsyncImageViewDelegate> delegate;
}
@property (nonatomic,retain) id delegate;
- (void)loadImageFromURL:(NSURL*)url;
- (void)loadImageFromPath:(NSString*)path;
- (UIImage*) image;
@end
