//
//  NewCustomerListViewController.m
//  KDS
//
//  Created by Tiseno Mac 2 on 2/6/13.
//
//

#import "NewCustomerListViewController.h"

@interface NewCustomerListViewController ()

@end

@implementation NewCustomerListViewController
@synthesize companyTableView;
@synthesize customerArray_Copied, customerArray;
@synthesize tOrderHistoryButton;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        if(appDelegate.loggedInSalesPerson!=nil)
        {
            KDSDataCustomer *dataCustomer=[[[KDSDataCustomer alloc] init] autorelease];
            NSArray *customers=[dataCustomer selectCustomerWithSalespersonID:appDelegate.loggedInSalesPerson.SalesPerson_Code];

            self.customerArray=customers;
        }
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(customerArray_Copied == nil)
    {
        NSMutableArray *tcustomerArray_Copied = [[[NSMutableArray alloc] init] autorelease];
        self.customerArray_Copied = tcustomerArray_Copied;

    }
    for(int i=0;i<[self.customerArray count];i++)
    {
        [customerArray_Copied addObject:[customerArray objectAtIndex:i]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return [customerArray_Copied count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";

    CustomerListCell *cell = [[[CustomerListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];

    Customer *customer=[customerArray_Copied objectAtIndex:indexPath.row];
    
    cell.customer=customer;

    cell.companyNameIDLabel.text=customer.Customer_Id;
    cell.companyNameLabel.text = customer.Customer_Name;

   
    tOrderHistoryButton = [[[UIButton alloc] initWithFrame:CGRectMake(375, 0, 76, 42)] autorelease];
    //[tOrderHistoryButton setBackgroundImage:orderHistoryButtonImage forState:UIControlStateNormal];
    [tOrderHistoryButton addTarget:self action:@selector(orderHistory_ButtonTapped:)
                  forControlEvents:UIControlEventTouchDown];
    UIImage *orderHistoryButtonImage = [UIImage imageNamed:@"btn_order_history.png"];
    [tOrderHistoryButton setBackgroundImage:orderHistoryButtonImage forState:UIControlStateNormal];
    
    [cell addSubview:tOrderHistoryButton];
    
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

//-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    CustomerListCell *cell = (CustomerListCell*) [tableView cellForRowAtIndexPath:indexPath];
//    UIButton *tOrderHistoryButton = [[[UIButton alloc] initWithFrame:CGRectMake(375, 0, 76, 42)] autorelease];
//    [cell.orderHistoryButton setBackgroundImage:[UIImage imageNamed:@"btn_order_history.png"] forState:UIControlStateNormal];
//    [cell.orderHistoryButton addTarget:self action:@selector(orderHistory_ButtonTapped:) forControlEvents:UIControlEventTouchDown];
//    tOrderHistoryButton=cell.orderHistoryButton;
//    [cell addSubview:tOrderHistoryButton];
//    
//    NSLog(@"indexPath.row>>>>%d",indexPath.row);
//    /*
//    UIImage *orderHistoryButtonImage = [UIImage imageNamed:@"btn_order_history.png"];
//    UIButton *tOrderHistoryButton = [[[UIButton alloc] initWithFrame:CGRectMake(375, 0, 76, 42)] autorelease];
//    [tOrderHistoryButton setBackgroundImage:orderHistoryButtonImage forState:UIControlStateNormal];
//    [tOrderHistoryButton addTarget:self action:@selector(orderHistory_ButtonTapped:)
//                  forControlEvents:UIControlEventTouchDown];
//    //self.orderHistoryButton = tOrderHistoryButton;
//    
//    [cell addSubview:tOrderHistoryButton];*/
//    
//}

-(IBAction)orderHistory_ButtonTapped:(id)sender
{
     
    NSLog(@"btnhistory sender %@", sender);
}

-(void)generateOderHistoryTable:(NSIndexPath *)indexPath
{
    NSLog(@"%d",indexPath.row);
}
- (void)dealloc
{
    [tOrderHistoryButton release];
    [customerArray release];
    [customerArray_Copied release];
    [companyTableView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setCompanyTableView:nil];
    [super viewDidUnload];
}
@end
