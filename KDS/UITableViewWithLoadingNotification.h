//
//  UITableViewWithLoadingNotification.h
//  KDS
//
//  Created by Tiseno on 1/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UITableViewLoadingDelegate.h"

@interface UITableViewWithLoadingNotification : UITableView {
    id<UITableViewLoadingDelegate> loadingDelegate;
}
@property (nonatomic, retain) id<UITableViewLoadingDelegate> loadingDelegate;
@end
 