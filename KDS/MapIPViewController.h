//
//  MapIPViewController.h
//  KDS
//
//  Created by Tiseno Mac 2 on 2/19/13.
//
//

#import <UIKit/UIKit.h>
#import "KDSAppDelegate.h"
@interface MapIPViewController : UIViewController{
    int heightOfEditedView;
    int heightOfEditedArea;
    int heightOffset;
}
@property (retain, nonatomic) IBOutlet UITextField *txtMapip;
-(IBAction)saveHost;
@end
