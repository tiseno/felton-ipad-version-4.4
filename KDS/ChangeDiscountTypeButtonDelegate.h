//
//  ChangeDiscountTypeButtonDelegate.h
//  KDS
//
//  Created by Tiseno on 12/9/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol ChangeDiscountTypeButtonDelegate <NSObject>

-(void)changeDiscountType:(id)sender TextField:(UITextField*) textField;
@end
