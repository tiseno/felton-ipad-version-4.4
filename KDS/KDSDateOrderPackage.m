//
//  KDSDateOrderPackage.m
//  KDS
//
//  Created by Tiseno Mac 2 on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KDSDateOrderPackage.h"

@implementation KDSDateOrderPackage
@synthesize runBatch;

-(void)dealloc
{
    [super dealloc];
}

-(DataBaseInsertionResult)insertOrderPackageArray:(NSArray*)iorderpackage 
{
    self.runBatch=YES;
    DataBaseInsertionResult insertionResult=DataBaseInsertionSuccessful;
    //NSArray *existingCustomers=[self selectCustomerWithSalespersonID:salesPersonID];
    [self openConnection];
    
    for (OrderPackaging* orderp in iorderpackage) 
    {
        
        insertionResult=[self insertPackage:orderp];
        
        if(insertionResult==DataBaseInsertionFailed)
        {
            break;
        }
    } 
    
    [self closeConnection];
    return insertionResult;
}

-(DataBaseInsertionResult)insertPackage:(OrderPackaging*)tPackage
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch) 
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    } 
    if(connectionIsOpenned)
    {
        NSString *insertSQL;
        

            //NSString *itemDescription=[tItem.Description stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"]; 
            //NSString *itemNo=[tItem.Item_No stringByReplacingOccurrencesOfString:@"'" withString:@"tisenocodeapos"];
            insertSQL = [NSString stringWithFormat:@"insert into SalesOrderPackage(orderID, packageID) values('%@','%@');",tPackage.orderID, tPackage.packageID];
            
            
            NSLog(@"tItem.Description--->%@",tPackage.orderID );
        

        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {

            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        //NSLog(@"%@",insertSQL);
        //NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}

-(DataBaseDeletionResult)deletOrderpackage
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        
        NSString *deleteSQL = [NSString stringWithFormat:@"delete from SalesOrderPackage;"];
        sqlite3_stmt *statement;
        const char *insert_stmt = [deleteSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection];
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to delete with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return DataBaseDeletionFailed;
}

-(NSArray*)selectOrderPackageID 
{ 
    NSMutableArray* ordersarr=nil;
    if([self openConnection]==DataBaseConnectionOpened)
    {
        ordersarr=[[[NSMutableArray alloc] init] autorelease];
        NSString *selectSQL=[NSString stringWithFormat:@"select orderID from SalesOrderPackage order by orderID DESC"];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        while(sqlite3_step(statement) == SQLITE_ROW) 
        {
            OrderPackaging* orderPackage=[[[OrderPackaging alloc] init]autorelease];

            
            
            NSString *orderLocation=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 0)];
            
            orderPackage.orderID= orderLocation;
            
 
            [ordersarr addObject:orderPackage];
            //[orderPackage release];

        }
        [self closeConnection];
    }

    return ordersarr;
}

@end
