//
//  ActiveCaseItemsRequest.m
//  KDS
//
//  Created by Tiseno Mac 2 on 4/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ActiveCaseItemsRequest.h"

@implementation ActiveCaseItemsRequest

-(id)init
{
    self=[super init];
    if(self)
    {
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        NSError *error;
        NSString *strhost=[appDelegate loadHostIntoString];
        NSString *settingFileContents=[NSString stringWithContentsOfFile:strhost encoding:NSASCIIStringEncoding error:&error];
        NSString *HostIP = [NSString stringWithFormat:@"http://%@/KDSAllActiveItems/service.asmx",settingFileContents];

        self.webserviceURL=HostIP;
        self.SOAPAction=@"GetActiveItem";
        self.requestType=WebserviceRequest;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{
    return @"";
}
@end
