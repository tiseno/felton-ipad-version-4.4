//
//  TransferOrderTableViewController.h
//  KDS
//
//  Created by Jermin Bazazian on 5/28/12.
//  Copyright (c) 2012 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderListTransferedCellViewController.h"
#import "KDSDataSalesOrder.h"
#import "KDSAppDelegate.h"
#import "SalesOrderType.h"
#import "KDSDataBlindSalesOrder.h"
//#import "OrderListTransferedsystem.h"


@class OrderListTransferedsystem;
@interface TransferOrderTableViewController : UITableViewController<UITableViewDataSource,UITableViewDelegate>{
    
    NSArray *newtransferSalesOrderArr;
    OrderListTransferedsystem* orderListViewControllersystm;
}
@property (nonatomic, retain) OrderListTransferedsystem* orderListViewControllersystm;
@property (nonatomic, retain) NSArray *newtransferSalesOrderArr;
@property (nonatomic, retain) NSArray* newtransferOrdersNotTransferedArr;
-(void)initializeTableData;
@end
