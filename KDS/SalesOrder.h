//
//  SalesOrder.h
//  KDS
//
//  Created by Jermin Bazazian on 12/8/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SalesOrderType.h"
#import "Address.h"
#import "Customer.h"
#import "SalesOrderItem.h"
#import "BlindSalesOrderItem.h"
#import "GDataXMLNode.h"
//#import "BlindSalesOrder.h"
#import "SalesPerson.h"


@interface SalesOrder : NSObject { 
    Customer* customer;
    int _Order_id;
    SalesOrderType _Type;
    NSString *remark;
    NSDate* _Date;
    NSString* _Location;
    NSString* systemOrderId;
    NSMutableArray* _Order_Items;
    BOOL _Is_Uploaded;
    BOOL isEditable;
    BOOL isDeleted;
    float grand_Price;
    float order_discount;
    NSString *DeliveryDate;
    NSString *OrderDescription;
    Address* Caddress;
    NSString *Shipping_Address;
    NSString *Order_Reference;
    NSString *CashRemarks;
    
    //BlindSalesOrder* gBlindSalesOrder;
}
@property (nonatomic,retain) NSString *CashRemarks;
//@property (nonatomic,retain) BlindSalesOrder* gBlindSalesOrder;
@property (nonatomic,retain) NSString *Order_Reference;
@property (nonatomic,retain) NSString *Shipping_Address;
@property (nonatomic, retain) NSString *OrderDescription;
@property (nonatomic, retain) NSString *DeliveryDate;
@property (nonatomic, retain) NSString *remark;
@property (nonatomic, retain) NSString* systemOrderId;
@property (nonatomic, retain) Customer* customer;
@property (nonatomic, retain) Address* Caddress;
@property (nonatomic) int Order_id;
@property (nonatomic, readonly) SalesOrderType Type;
@property (nonatomic, retain) NSDate* Date;
@property (nonatomic, retain) NSString *Location;
@property (nonatomic) BOOL Is_Uploaded;
@property (nonatomic) BOOL isEditable;
@property (nonatomic) BOOL isDeleted;
@property (nonatomic) float grand_Price;
@property (nonatomic) float order_discount;
@property (nonatomic, retain) NSString *OrderPackageID;

-(NSMutableArray*) order_Items;
-(BOOL)isMainOrderType:(SalesOrderItem*)orderItem;
//-(void) addOrderItem:(SalesOrderItem*)orderItem;
-(BOOL) addOrderItem:(SalesOrderItem*)orderItem;
-(BOOL) removeOrderItemAtIndex:(NSUInteger)index;
-(BOOL) replaceOrderItemAtIndexblind:(NSUInteger)index withObject:(BlindSalesOrderItem*)orderid;
-(BOOL) replaceOrderItemAtIndex:(NSUInteger)index withObject:(SalesOrderItem*)orderid;
-(NSInteger)NumberOfOderItems;
-(NSString*)convertToXMLWithSalesPersonCode:(SalesPerson*)salesPersonCode;
-(NSString*)convertToXMLWithSalesPersonPDF:(SalesPerson*)salesPersonCode;
-(void)addInheritanceDetails:(GDataXMLElement*)salesOrderNode;
-(void)addInheritancePDFDetails:(GDataXMLElement*)salesOrderNode;
@end
