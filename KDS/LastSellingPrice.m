//
//  LastSellingPrice.m
//  KDS
//
//  Created by Jermin Bazazian on 12/9/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "LastSellingPrice.h"


@implementation LastSellingPrice
@synthesize Price=_Price,Date=_Date,UOM=_UOM,Min_Hight=_Min_Hight,Min_SQFT=_Min_SQFT;
-(void)dealloc
{
    [_Date release];
    [_UOM release];
    [super dealloc];
}
@end
