//
//  KDSDataSalesOrderItem.h
//  KDS
//
//  Created by Tiseno on 12/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KDSDataLayerBase.h"
#import "SalesOrderItem.h"
#import "SalesOrder.h"
#import "AppWideConstants.h"
//#import "BlindItem.h"


@interface KDSDataSalesOrderItem : KDSDataLayerBase {
    BOOL runBatch;
}
@property (nonatomic) BOOL runBatch;
-(id)initWithDataBase:(sqlite3*)dataBase;
-(NSString*)generateInsertQueryForSaleOrderItem:(SalesOrderItem*)salesOrederItem AndSalesOrderID:(int)salesOrderID;
-(DataBaseInsertionResult)insertSalesOrderItem:(SalesOrderItem*)salesOrderItem ForSalesOrderID:(int)salesOrderID;
-(NSArray*)selectOrderItemsForOrder:(SalesOrder*)saleOrder;
-(DataBaseDeletionResult)deleteFromSalesOrderItemForOrder:(SalesOrder*)salesOrder;
@end
