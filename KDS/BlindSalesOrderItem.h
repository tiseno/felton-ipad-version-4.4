//
//  BlindSalesOrderItem.h
//  KDS
//
//  Created by Jermin Bazazian on 12/8/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SalesOrderItem.h"
//#import "BlindItem.h"


@interface BlindSalesOrderItem : SalesOrderItem {
    float _Width;
    float _Height;
    float _Quantity_in_Set;
    NSString* _Control;
    NSString* _Color;
    SalesOrderItem* SalesOrderItems;
    //BlindItem* blindItems;
     
}
@property (nonatomic, retain) SalesOrderItem* SalesOrderItems;
@property (nonatomic) float Width,Height,Quantity_in_Set;
@property (nonatomic, retain) NSString* Control,*Color;

//@property (nonatomic,retain) BlindItem* blindItems;
@end
