//
//  CustomersBySalespersonIDRequest.h
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/6/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLRequest.h"


@interface CustomersBySalespersonIDRequest : XMLRequest {
    NSString *salesPersonID;
}
@property (nonatomic,retain) NSString *salesPersonID;
-(id)initWithSalesPersonID:(NSString*)isalesPersonID;
@end
