//
//  UITableViewLoadingDelegate.h
//  KDS
//
//  Created by Tiseno on 1/4/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol UITableViewLoadingDelegate <NSObject>

-(void)loadDataFinished; 
@end
 