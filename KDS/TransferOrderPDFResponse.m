//
//  TransferOrderPDFResponse.m
//  KDS
//
//  Created by Tiseno Mac 2 on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TransferOrderPDFResponse.h"

@implementation TransferOrderPDFResponse
@synthesize getPDFPath;

-(void)dealloc
{
    [getPDFPath release];
    [super dealloc];
}

@end
