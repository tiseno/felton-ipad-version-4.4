//
//  SalesOrder.m
//  KDS
//
//  Created by Jermin Bazazian on 12/8/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "SalesOrder.h"


@implementation SalesOrder
@synthesize Order_id=_Order_id,Type=_Type,Date=_Date,Location=_Location,customer,Is_Uploaded=_Is_Uploaded,isEditable, grand_Price,order_discount,isDeleted, systemOrderId, remark;
//@synthesize gBlindSalesOrder;
@synthesize DeliveryDate,OrderDescription,Caddress,Shipping_Address,Order_Reference,CashRemarks;
@synthesize OrderPackageID;


-(id)init
{
    self=[super init];
    if(self)
    {
        _Is_Uploaded=NO;
        _Type=SalesOrderMain;
        isEditable=YES;
        isDeleted=NO;
        _Order_id=-1;
    }
    return  self;
}
-(void)dealloc
{
    [OrderPackageID release];
    [Caddress release];
    [OrderDescription release];
    [DeliveryDate release];
    //[gBlindSalesOrder release];
    [remark release];
    [systemOrderId release];
    [_Location release];
    [_Date release];
    [customer release];
    [_Order_Items release]; 
    [Shipping_Address release];
    [Order_Reference release];
    [super dealloc];
} 
-(NSMutableArray*) order_Items
{ 
    return _Order_Items;
}
-(NSInteger)NumberOfOderItems
{
    return [_Order_Items count];
}
/*-(void) addOrderItem:(SalesOrderItem*)orderItem
{
    if([self isMainOrderType:orderItem])
    {
        if(_Order_Items==nil)
        {
            _Order_Items=[[NSMutableArray alloc] init];
        }
        [_Order_Items addObject:orderItem];
    }
    else
    {
        //This section is never reached for BlindOrders
        NSAssert1(0, @"Failed to add with message '%s'.", "Blind Order Items can't be added to normal Orders");
    } 
}*/
-(BOOL) addOrderItem:(SalesOrderItem*)orderItem
{
    if([self isMainOrderType:orderItem])
    { 
        if(_Order_Items==nil)
        {
            _Order_Items=[[NSMutableArray alloc] init];
        }
        [_Order_Items addObject:orderItem];
        return YES;
    } 
    else
    {
        //This section is never reached for BlindOrders
        //NSAssert1(0, @"Failed to add with message '%s'.", "Blind Order Items can't be added to normal Orders");
        return NO;
    }
}

-(BOOL)isMainOrderType:(SalesOrderItem*)orderItem
{
    if([orderItem isKindOfClass:[SalesOrderItem class]] && ![orderItem isKindOfClass:[BlindSalesOrderItem class]])
    {
        return YES; 
    }
    return NO;
}

-(BOOL) replaceOrderItemAtIndexblind:(NSUInteger)index withObject:(BlindSalesOrderItem*)orderid;
{
    if(isEditable)
    {
        //[_Order_Items replaceObjectAtIndex:index withObject:obj];
        [_Order_Items replaceObjectAtIndex:index withObject:orderid ];
        return YES;
    }
    else
    {
        return NO;
    }
    
    
}
             
-(BOOL) replaceOrderItemAtIndex:(NSUInteger)index withObject:(SalesOrderItem*)orderid;
{
    if(isEditable)
    {
        //[_Order_Items replaceObjectAtIndex:index withObject:obj];
        [_Order_Items replaceObjectAtIndex:index withObject:orderid ];
        return YES;
    }
    else
    {
        return NO;
    }
    
    
}

-(BOOL) removeOrderItemAtIndex:(NSUInteger)index
{
    if(isEditable)
    {
        [_Order_Items removeObjectAtIndex:index];
        return YES;
    }
    else
    {
        return NO;
    }
} 
 
-(NSString*)convertToXMLWithSalesPersonCode:(SalesPerson*)salesPersonCode
{
    GDataXMLElement* salesPersongCodeElement=[GDataXMLNode elementWithName:@"SalesPersonCode" stringValue:salesPersonCode.SalesPerson_Code];
    NSString *remarks=[self.remark stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* remarkElement=[GDataXMLNode elementWithName:@"Remark" stringValue:remarks];
    
    /*=================================*/
    GDataXMLElement* customerIdElement=[GDataXMLNode elementWithName:@"CustomerID" stringValue:self.customer.Customer_Id];

    NSString *customerName=[self.customer.Customer_Name stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    //NSLog(@"%@",customerName);
    GDataXMLElement* customerCustomer_NameElement=[GDataXMLNode elementWithName:@"CustomerName" stringValue:customerName];
    
    NSString *CustomerAdd1=[self.Caddress.Street_1 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    NSString *CustomerAdd2=[self.Caddress.Street_2 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    NSString *CustomerAdd3=[self.Caddress.Street_3 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    NSString *CustomerAdd4=[self.Caddress.Street_4 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerAddressStreet_1Element=[GDataXMLNode elementWithName:@"CustomerAdd1" stringValue:CustomerAdd1];
    GDataXMLElement* customerAddressStreet_2Element=[GDataXMLNode elementWithName:@"CustomerAdd2" stringValue:CustomerAdd2];
    GDataXMLElement* customerAddressStreet_3Element=[GDataXMLNode elementWithName:@"CustomerAdd3" stringValue:CustomerAdd3];
    GDataXMLElement* customerAddressStreet_4Element=[GDataXMLNode elementWithName:@"CustomerAdd4" stringValue:CustomerAdd4];
    
    NSString *City=[self.customer.Address.City stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerAddressCityElement=[GDataXMLNode elementWithName:@"City" stringValue:City];
    
    NSString *ZipCode=[self.customer.Address.ZipCode stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerAddressZipCodeElement=[GDataXMLNode elementWithName:@"ZipCode" stringValue:ZipCode];
    NSString *PostalCode=[self.customer.Address.PostalCode stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerAddressPostalCodeElement=[GDataXMLNode elementWithName:@"PostalCode" stringValue:PostalCode];
    NSString *State=[self.customer.Address.State stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerAddressStateElement=[GDataXMLNode elementWithName:@"State" stringValue:State];
    NSString *Country=[self.customer.Address.Country stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerAddressCountryElement=[GDataXMLNode elementWithName:@"Country" stringValue:Country];/**/
    
    NSString *Phone_1=[self.customer.Phone_1 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerPhone_1Element=[GDataXMLNode elementWithName:@"CustomerTel" stringValue:Phone_1];
    NSString *Phone_2=[self.customer.Phone_2 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerPhone_2Element=[GDataXMLNode elementWithName:@"CustomerFax" stringValue:Phone_2];
    NSString *Location=[self.Location stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* locationElement=[GDataXMLNode elementWithName:@"Location" stringValue:Location];
    //NSString *Contact_Name=[self.customer.Contact_Name stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    //GDataXMLElement* CusNameElement=[GDataXMLNode elementWithName:@"CustContactName" stringValue:Contact_Name];
    
    GDataXMLElement* systemOrderIdElement=[GDataXMLNode elementWithName:@"SystemOrderID" stringValue:self.systemOrderId];
    NSString* orderIdStr = [NSString stringWithFormat:@"%d", self.Order_id];
    GDataXMLElement* orderIdElement=[GDataXMLNode elementWithName:@"OrderID" stringValue:orderIdStr];
    NSString* salesOrderTypeStr=@"";
    if(self.Type == SalesOrderMain)
    {
        salesOrderTypeStr = @"SalesOrderMain";
    }
    else
    {
        salesOrderTypeStr = @"SalesOrderBlind";
    }
    GDataXMLElement* salesOrderTypeElement=[GDataXMLNode elementWithName:@"SalesOrderType" stringValue:salesOrderTypeStr];
    NSString* grandPriceStr = [NSString stringWithFormat:@"%.2f", self.grand_Price];
    GDataXMLElement* grandPriceElement=[GDataXMLNode elementWithName:@"GrandPrice" stringValue:grandPriceStr];
    NSString* orderDiscountStr = [NSString stringWithFormat:@"%.2f", self.order_discount];
    GDataXMLElement* orderDiscountElement=[GDataXMLNode elementWithName:@"OrderDiscount" stringValue:orderDiscountStr];
    
    /*NSDate *today = [NSDate date];
     NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
     [dateFormat setDateFormat:@"yyyyMMddHHmmss"];
     NSString *dateString = [dateFormat stringFromDate:today];
     //NSLog(@"date: %@", dateString);
     [dateFormat release];*/
    
    NSDateFormatter* formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    NSString* dateStr=[formatter stringFromDate:self.Date];
    [formatter release];
    NSString *DeliveryDates=[self.DeliveryDate stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* xDeliveryDate=[GDataXMLNode elementWithName:@"DeliveryDate" stringValue:DeliveryDates];
    NSString *CashRemarkss=[self.CashRemarks stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* xcustRemark=[GDataXMLNode elementWithName:@"custRem" stringValue:CashRemarkss];
    NSString *Order_Referencess=[self.Order_Reference stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* xorderReference=[GDataXMLNode elementWithName:@"orderRef" stringValue:Order_Referencess];
    NSString *OrderDescriptiondd=[self.OrderDescription stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* xOrderDescription=[GDataXMLNode elementWithName:@"OrderDescription" stringValue:OrderDescriptiondd];
    
    NSLog(@"self.Date>>>>%@",self.Date);
    
    NSLog(@"dateStr>>>>%@",dateStr);
    
    GDataXMLElement* dateElement=[GDataXMLNode elementWithName:@"Date" stringValue:dateStr];
    
    GDataXMLElement* salesOrderNode=[GDataXMLNode elementWithName:@"SalesOrder"];
    [salesOrderNode addChild:salesPersongCodeElement];
    [salesOrderNode addChild:remarkElement];
    [salesOrderNode addChild:customerIdElement];
    
    [salesOrderNode addChild:customerCustomer_NameElement];
    [salesOrderNode addChild:customerAddressStreet_1Element];
    [salesOrderNode addChild:customerAddressStreet_2Element];
    [salesOrderNode addChild:customerAddressStreet_3Element];
    [salesOrderNode addChild:customerAddressStreet_4Element];
    
    [salesOrderNode addChild:customerAddressCityElement];
    [salesOrderNode addChild:customerAddressZipCodeElement];
    [salesOrderNode addChild:customerAddressPostalCodeElement];
    [salesOrderNode addChild:customerAddressStateElement];
    [salesOrderNode addChild:customerAddressCountryElement];/**/
    
    [salesOrderNode addChild:customerPhone_1Element];
    [salesOrderNode addChild:customerPhone_2Element];
    
    [salesOrderNode addChild:locationElement];
    //[salesOrderNode addChild:CusNameElement];
    
    [salesOrderNode addChild:systemOrderIdElement];
    [salesOrderNode addChild:orderIdElement];
    [salesOrderNode addChild:salesOrderTypeElement];
    [salesOrderNode addChild:grandPriceElement];
    [salesOrderNode addChild:orderDiscountElement];
    
    [self addInheritancePDFDetails:salesOrderNode];
    
    if ([salesOrderTypeStr isEqualToString:@"SalesOrderMain"]) 
    {
       
        NSString *Shipping_Addressaa=[self.Shipping_Address stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        GDataXMLElement* addressElement=[GDataXMLNode elementWithName:@"InstallationAddress" stringValue:Shipping_Addressaa];
        
        GDataXMLElement* addressCityElement=[GDataXMLNode elementWithName:@"InstallationDate" stringValue:@""];
        //GDataXMLElement* addressPostalCodeElement=[GDataXMLNode elementWithName:@"PostalCode" stringValue:@""];        
        GDataXMLElement* addressStateElement=[GDataXMLNode elementWithName:@"InstallationTime" stringValue:@""];
        //GDataXMLElement* addressCountryElement=[GDataXMLNode elementWithName:@"Country" stringValue:@""];
        GDataXMLElement* installationPhoneElement=[GDataXMLNode elementWithName:@"InstallationPhone" stringValue:@""];
        GDataXMLElement* installationFaxElement=[GDataXMLNode elementWithName:@"InstallationFax" stringValue:@""];
        GDataXMLElement* installationEmailElement=[GDataXMLNode elementWithName:@"InstallationEmail" stringValue:@""];
        GDataXMLElement* installationNameElement=[GDataXMLNode elementWithName:@"InstallationName" stringValue:@""];

        [salesOrderNode addChild:addressCityElement];
        [salesOrderNode addChild:addressStateElement];        
        [salesOrderNode addChild:addressElement];
        [salesOrderNode addChild:installationPhoneElement];
        [salesOrderNode addChild:installationFaxElement];        
        [salesOrderNode addChild:installationEmailElement];        
        [salesOrderNode addChild:installationNameElement];
        
        GDataXMLElement* transportationCostElement=[GDataXMLNode elementWithName:@"Transportation" stringValue:@"0.00"];
        GDataXMLElement* installationCostElement=[GDataXMLNode elementWithName:@"Installation" stringValue:@"0.00"];
        
        [salesOrderNode addChild:transportationCostElement];
        [salesOrderNode addChild:installationCostElement];
        
        
    }
    
    
    [salesOrderNode addChild:xDeliveryDate];
    [salesOrderNode addChild:xcustRemark];
    [salesOrderNode addChild:xorderReference];
    [salesOrderNode addChild:xOrderDescription];
    [salesOrderNode addChild:dateElement];
    

    
    
    GDataXMLElement* salesOrderItemsNode=[GDataXMLNode elementWithName:@"SalesOrderItems"];
    for(SalesOrderItem* orderItem in _Order_Items)
    {
        GDataXMLElement* salesOrderItemNode=[GDataXMLNode elementWithName:@"SalesOrderItem"];
        
        NSString *gItemNumber=[orderItem.item.Item_No stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        GDataXMLElement* itemNumberElement=[GDataXMLNode elementWithName:@"ItemNumber" stringValue:gItemNumber];
        [salesOrderItemNode addChild:itemNumberElement];
        
        NSString *gItemDescription=[orderItem.item.Description stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        
        GDataXMLElement* itemDescElement=[GDataXMLNode elementWithName:@"ItemDescription" stringValue:gItemDescription];
        [salesOrderItemNode addChild:itemDescElement];
        
        GDataXMLElement* quantityElement=[GDataXMLNode elementWithName:@"Quantity" stringValue:[NSString stringWithFormat:@"%.2f",orderItem.Quantity]];
        [salesOrderItemNode addChild:quantityElement];
        GDataXMLElement* unitPriceElement=[GDataXMLNode elementWithName:@"UnitPrice" stringValue:[NSString stringWithFormat:@"%.2f",orderItem.Unit_Price]];
        [salesOrderItemNode addChild:unitPriceElement];
        GDataXMLElement* orderUOMElement=[GDataXMLNode elementWithName:@"OrderUOM" stringValue:orderItem.Order_UOM];
        [salesOrderItemNode addChild:orderUOMElement];
        GDataXMLElement* discountPercentageElement=[GDataXMLNode elementWithName:@"DiscountPercentage" stringValue:[NSString stringWithFormat:@"%.2f",orderItem.Discount_Percent]];
        [salesOrderItemNode addChild:discountPercentageElement];
        GDataXMLElement* discountElement=[GDataXMLNode elementWithName:@"DiscountAmount" stringValue:[NSString stringWithFormat:@"%.2f",orderItem.Discount_Amount]];
        [salesOrderItemNode addChild:discountElement];
        
        NSString *gComment=[orderItem.Comment stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        GDataXMLElement* commentElement=[GDataXMLNode elementWithName:@"Comment" stringValue:gComment];
        [salesOrderItemNode addChild:commentElement];
        /*GDataXMLElement* TotalPriceElement=[GDataXMLNode elementWithName:@"TotalPrice" stringValue:[NSString stringWithFormat:@"%.2f",orderItem.TotalPrice]];
         [salesOrderItemNode addChild:TotalPriceElement];*/
        if ([salesOrderTypeStr isEqualToString:@"SalesOrderMain"]) 
        {
            GDataXMLElement* widthElement=[GDataXMLNode elementWithName:@"Width" stringValue:[NSString stringWithFormat:@"%.2f"]];
            [salesOrderItemNode addChild:widthElement];
            GDataXMLElement* heightElement=[GDataXMLNode elementWithName:@"Height" stringValue:[NSString stringWithFormat:@"%.2f"]];
            [salesOrderItemNode addChild:heightElement];
            GDataXMLElement* quanityInSetElement=[GDataXMLNode elementWithName:@"QuantityInSet" stringValue:[NSString stringWithFormat:@"%.2f"]];
            [salesOrderItemNode addChild:quanityInSetElement];
            
            GDataXMLElement* controlElement=[GDataXMLNode elementWithName:@"Control" stringValue:@""];
            [salesOrderItemNode addChild:controlElement];
            GDataXMLElement* colorElement=[GDataXMLNode elementWithName:@"Color" stringValue:@""];
            [salesOrderItemNode addChild:colorElement];
            
        }
        NSString *SpecialItemDescriptions=[orderItem.SpecialItemDescription stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        GDataXMLElement* SpecialItemDesc=[GDataXMLNode elementWithName:@"SpeDesc" stringValue:SpecialItemDescriptions];
        [salesOrderItemNode addChild:SpecialItemDesc];

        
        
        [orderItem addInheritancePDFDetails:salesOrderItemNode];
        [salesOrderItemsNode addChild:salesOrderItemNode];
    }
    [salesOrderNode addChild:salesOrderItemsNode];
    GDataXMLDocument* document=[[GDataXMLDocument alloc] initWithRootElement:salesOrderNode];
    NSData* xmlData=document.XMLData;
    [document release];
    NSString* xmlWithHeaderStr=[[NSString alloc] initWithData:xmlData encoding:NSUTF8StringEncoding];
    
    NSString* xmlStr=[xmlWithHeaderStr substringFromIndex:22];
    [xmlWithHeaderStr release];
    return xmlStr;
}

-(NSString*)convertToXMLWithSalesPersonPDF:(SalesPerson*)salesPersonCode
{
    GDataXMLElement* salesPersongCodeElement=[GDataXMLNode elementWithName:@"SalesPersonCode" stringValue:salesPersonCode.SalesPerson_Name];
    
    NSString *cremarkElement=[self.remark stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* remarkElement=[GDataXMLNode elementWithName:@"Remark" stringValue:cremarkElement];
    
    /*=================================*/
    GDataXMLElement* customerIdElement=[GDataXMLNode elementWithName:@"CustomerID" stringValue:self.customer.Customer_Id];

    NSString *customerName=[self.customer.Customer_Name stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerCustomer_NameElement=[GDataXMLNode elementWithName:@"CustomerName" stringValue:customerName];
        
    NSString *CustomerAdd1=[self.Caddress.Street_1 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    NSString *CustomerAdd2=[self.Caddress.Street_2 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    NSString *CustomerAdd3=[self.Caddress.Street_3 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    NSString *CustomerAdd4=[self.Caddress.Street_4 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerAddressStreet_1Element=[GDataXMLNode elementWithName:@"CustomerAdd1" stringValue:CustomerAdd1];
    GDataXMLElement* customerAddressStreet_2Element=[GDataXMLNode elementWithName:@"CustomerAdd2" stringValue:CustomerAdd2];
    GDataXMLElement* customerAddressStreet_3Element=[GDataXMLNode elementWithName:@"CustomerAdd3" stringValue:CustomerAdd3];
    GDataXMLElement* customerAddressStreet_4Element=[GDataXMLNode elementWithName:@"CustomerAdd4" stringValue:CustomerAdd4];
    
    NSString *Citys=[self.customer.Address.City stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerAddressCityElement=[GDataXMLNode elementWithName:@"City" stringValue:Citys];
    NSString *ZipCodea=[self.customer.Address.ZipCode stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerAddressZipCodeElement=[GDataXMLNode elementWithName:@"ZipCode" stringValue:ZipCodea];
    NSString *PostalCodes=[self.customer.Address.PostalCode stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerAddressPostalCodeElement=[GDataXMLNode elementWithName:@"PostalCode" stringValue:PostalCodes];
    NSString *States=[self.customer.Address.State stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerAddressStateElement=[GDataXMLNode elementWithName:@"State" stringValue:States];
    NSString *Countrys=[self.customer.Address.Country stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerAddressCountryElement=[GDataXMLNode elementWithName:@"Country" stringValue:Countrys];/**/
    NSString *Phone_1s=[self.customer.Phone_1 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerPhone_1Element=[GDataXMLNode elementWithName:@"CustomerTel" stringValue:Phone_1s];
    NSString *Phone_2s=[self.customer.Phone_2 stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* customerPhone_2Element=[GDataXMLNode elementWithName:@"CustomerFax" stringValue:Phone_2s];
    
    NSString *locationElementd=[self.Location stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* locationElement=[GDataXMLNode elementWithName:@"Location" stringValue:locationElementd];
    NSString *CusNameElementd=[self.customer.Contact_Name stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* CusNameElement=[GDataXMLNode elementWithName:@"CustContactName" stringValue:CusNameElementd];
    
    GDataXMLElement* systemOrderIdElement=[GDataXMLNode elementWithName:@"SystemOrderID" stringValue:self.systemOrderId];
    NSString* orderIdStr = [NSString stringWithFormat:@"%d", self.Order_id];
    GDataXMLElement* orderIdElement=[GDataXMLNode elementWithName:@"OrderID" stringValue:orderIdStr];
    NSString* salesOrderTypeStr=@"";
    if(self.Type == SalesOrderMain)
    {
        salesOrderTypeStr = @"SalesOrderMain";
    }
    else
    {
        salesOrderTypeStr = @"SalesOrderBlind";
    }
    GDataXMLElement* salesOrderTypeElement=[GDataXMLNode elementWithName:@"SalesOrderType" stringValue:salesOrderTypeStr];
    NSString* grandPriceStr = [NSString stringWithFormat:@"%.2f", self.grand_Price];
    GDataXMLElement* grandPriceElement=[GDataXMLNode elementWithName:@"GrandPrice" stringValue:grandPriceStr];
    NSString* orderDiscountStr = [NSString stringWithFormat:@"%.2f", self.order_discount];
    GDataXMLElement* orderDiscountElement=[GDataXMLNode elementWithName:@"OrderDiscount" stringValue:orderDiscountStr];
    
    NSDateFormatter* formatter=[[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy/MM/dd"];
    NSString* dateStr=[formatter stringFromDate:self.Date];
    [formatter release];
    NSString *xDeliveryDated=[self.DeliveryDate stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* xDeliveryDate=[GDataXMLNode elementWithName:@"DeliveryDate" stringValue:xDeliveryDated];
    NSString *xcustRemarkd=[self.CashRemarks stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* xcustRemark=[GDataXMLNode elementWithName:@"custRem" stringValue:xcustRemarkd];
    NSString *xorderReferenced=[self.Order_Reference stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* xorderReference=[GDataXMLNode elementWithName:@"orderRef" stringValue:xorderReferenced];
    
    NSString *xOrderDescriptiond=[self.OrderDescription stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
    GDataXMLElement* xOrderDescription=[GDataXMLNode elementWithName:@"OrderDescription" stringValue:xOrderDescriptiond];
    
    GDataXMLElement* dateElement=[GDataXMLNode elementWithName:@"Date" stringValue:dateStr];
    
    GDataXMLElement* salesOrderNode=[GDataXMLNode elementWithName:@"SalesOrder"];
    [salesOrderNode addChild:salesPersongCodeElement];
    [salesOrderNode addChild:remarkElement];
    [salesOrderNode addChild:customerIdElement];
    
    [salesOrderNode addChild:customerCustomer_NameElement];
    [salesOrderNode addChild:customerAddressStreet_1Element];
    [salesOrderNode addChild:customerAddressStreet_2Element];
     [salesOrderNode addChild:customerAddressStreet_3Element];
     [salesOrderNode addChild:customerAddressStreet_4Element];
    
     [salesOrderNode addChild:customerAddressCityElement];
     [salesOrderNode addChild:customerAddressZipCodeElement];
     [salesOrderNode addChild:customerAddressPostalCodeElement];
     [salesOrderNode addChild:customerAddressStateElement];
     [salesOrderNode addChild:customerAddressCountryElement];/**/
    
    [salesOrderNode addChild:customerPhone_1Element];
    [salesOrderNode addChild:customerPhone_2Element];
    
    [salesOrderNode addChild:locationElement];
    [salesOrderNode addChild:CusNameElement];
    
    [salesOrderNode addChild:systemOrderIdElement];
    [salesOrderNode addChild:orderIdElement];
    [salesOrderNode addChild:salesOrderTypeElement];
    [salesOrderNode addChild:grandPriceElement];
    [salesOrderNode addChild:orderDiscountElement];
    
    [self addInheritancePDFDetails:salesOrderNode];
      
    if ([salesOrderTypeStr isEqualToString:@"SalesOrderMain"]) 
    {
        NSString *customerNameadd=[self.Shipping_Address stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        GDataXMLElement* addressElement=[GDataXMLNode elementWithName:@"InstallationAddress" stringValue:customerNameadd];
        
        GDataXMLElement* addressCityElement=[GDataXMLNode elementWithName:@"InstallationDate" stringValue:@""];
        //GDataXMLElement* addressPostalCodeElement=[GDataXMLNode elementWithName:@"PostalCode" stringValue:@""];        
        GDataXMLElement* addressStateElement=[GDataXMLNode elementWithName:@"InstallationTime" stringValue:@""];
        //GDataXMLElement* addressCountryElement=[GDataXMLNode elementWithName:@"Country" stringValue:@""];
        GDataXMLElement* installationPhoneElement=[GDataXMLNode elementWithName:@"InstallationPhone" stringValue:@""];
        GDataXMLElement* installationFaxElement=[GDataXMLNode elementWithName:@"InstallationFax" stringValue:@""];
        GDataXMLElement* installationEmailElement=[GDataXMLNode elementWithName:@"InstallationEmail" stringValue:@""];
        GDataXMLElement* installationNameElement=[GDataXMLNode elementWithName:@"InstallationName" stringValue:@""];
        
        [salesOrderNode addChild:addressCityElement];
        [salesOrderNode addChild:addressStateElement];        
        [salesOrderNode addChild:addressElement];
        [salesOrderNode addChild:installationPhoneElement];
        [salesOrderNode addChild:installationFaxElement];        
        [salesOrderNode addChild:installationEmailElement];        
        [salesOrderNode addChild:installationNameElement];
        
        GDataXMLElement* transportationCostElement=[GDataXMLNode elementWithName:@"Transportation" stringValue:@"0.00"];
        GDataXMLElement* installationCostElement=[GDataXMLNode elementWithName:@"Installation" stringValue:@"0.00"];
        
        [salesOrderNode addChild:transportationCostElement];
        [salesOrderNode addChild:installationCostElement];
        
        
    }
    
    
    [salesOrderNode addChild:xDeliveryDate];
    [salesOrderNode addChild:xcustRemark];
    [salesOrderNode addChild:xorderReference];
    [salesOrderNode addChild:xOrderDescription];
    [salesOrderNode addChild:dateElement];
    
    
    GDataXMLElement* salesOrderItemsNode=[GDataXMLNode elementWithName:@"SalesOrderItems"];
    for(SalesOrderItem* orderItem in _Order_Items)
    {
        GDataXMLElement* salesOrderItemNode=[GDataXMLNode elementWithName:@"SalesOrderItem"];
        
        NSString *gItemNumber=[orderItem.item.Item_No stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        GDataXMLElement* itemNumberElement=[GDataXMLNode elementWithName:@"ItemNumber" stringValue:gItemNumber];
        [salesOrderItemNode addChild:itemNumberElement];
        
        NSString *gItemDescription=[orderItem.item.Description stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        GDataXMLElement* itemDescElement=[GDataXMLNode elementWithName:@"ItemDescription" stringValue:gItemDescription];
        [salesOrderItemNode addChild:itemDescElement];
        
        GDataXMLElement* quantityElement=[GDataXMLNode elementWithName:@"Quantity" stringValue:[NSString stringWithFormat:@"%.2f",orderItem.Quantity]];
        [salesOrderItemNode addChild:quantityElement];
        GDataXMLElement* unitPriceElement=[GDataXMLNode elementWithName:@"UnitPrice" stringValue:[NSString stringWithFormat:@"%.2f",orderItem.Unit_Price]];
        [salesOrderItemNode addChild:unitPriceElement];
        GDataXMLElement* orderUOMElement=[GDataXMLNode elementWithName:@"OrderUOM" stringValue:orderItem.Order_UOM];
        [salesOrderItemNode addChild:orderUOMElement];
        GDataXMLElement* discountPercentageElement=[GDataXMLNode elementWithName:@"DiscountPercentage" stringValue:[NSString stringWithFormat:@"%.2f",orderItem.Discount_Percent]];
        [salesOrderItemNode addChild:discountPercentageElement];
        GDataXMLElement* discountElement=[GDataXMLNode elementWithName:@"DiscountAmount" stringValue:[NSString stringWithFormat:@"%.2f",orderItem.Discount_Amount]];
        [salesOrderItemNode addChild:discountElement];
        
        NSString *gComment=[orderItem.Comment stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        GDataXMLElement* commentElement=[GDataXMLNode elementWithName:@"Comment" stringValue:gComment];
        [salesOrderItemNode addChild:commentElement];
        /*GDataXMLElement* TotalPriceElement=[GDataXMLNode elementWithName:@"TotalPrice" stringValue:[NSString stringWithFormat:@"%.2f",orderItem.TotalPrice]];
         [salesOrderItemNode addChild:TotalPriceElement];*/
        if ([salesOrderTypeStr isEqualToString:@"SalesOrderMain"]) 
        {
        GDataXMLElement* widthElement=[GDataXMLNode elementWithName:@"Width" stringValue:[NSString stringWithFormat:@"%.2f"]];
        [salesOrderItemNode addChild:widthElement];
        GDataXMLElement* heightElement=[GDataXMLNode elementWithName:@"Height" stringValue:[NSString stringWithFormat:@"%.2f"]];
        [salesOrderItemNode addChild:heightElement];
        GDataXMLElement* quanityInSetElement=[GDataXMLNode elementWithName:@"QuantityInSet" stringValue:[NSString stringWithFormat:@"%.2f"]];
        [salesOrderItemNode addChild:quanityInSetElement];
            
        GDataXMLElement* controlElement=[GDataXMLNode elementWithName:@"Control" stringValue:@""];
        [salesOrderItemNode addChild:controlElement];
        GDataXMLElement* colorElement=[GDataXMLNode elementWithName:@"Color" stringValue:@""];
        [salesOrderItemNode addChild:colorElement];

        }
        NSString *gSpecialItemDesc=[orderItem.SpecialItemDescription stringByReplacingOccurrencesOfString:@"&" withString:@"tisenocodeand"];
        GDataXMLElement* SpecialItemDesc=[GDataXMLNode elementWithName:@"SpeDesc" stringValue:gSpecialItemDesc];
        [salesOrderItemNode addChild:SpecialItemDesc];
        
        [orderItem addInheritancePDFDetails:salesOrderItemNode];
        [salesOrderItemsNode addChild:salesOrderItemNode];
    }
    [salesOrderNode addChild:salesOrderItemsNode];
    GDataXMLDocument* document=[[GDataXMLDocument alloc] initWithRootElement:salesOrderNode];
    NSData* xmlData=document.XMLData;
    [document release];
    NSString* xmlWithHeaderStr=[[NSString alloc] initWithData:xmlData encoding:NSUTF8StringEncoding];
    
    NSString* xmlStr=[xmlWithHeaderStr substringFromIndex:22];
    [xmlWithHeaderStr release];
    return xmlStr;
}

-(void)addInheritanceDetails:(GDataXMLElement*)salesOrderNode
{
    
}

-(void)addInheritancePDFDetails:(GDataXMLElement*)salesOrderNode
{
    
}

@end
