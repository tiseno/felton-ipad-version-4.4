//
//  KDSDataSalesPerson.m
//  KDS
//
//  Created by Jermin Bazazian on 12/12/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "KDSDataSalesPerson.h"


@implementation KDSDataSalesPerson
@synthesize runBatch;
-(DataBaseInsertionResult)insertSalesPerson:(SalesPerson*)salesPerson
{BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *insertSQL = [NSString stringWithFormat:@"insert into Salesperson(code,name,password,username) values('%@','%@','%@','%@');",salesPerson.SalesPerson_Code,salesPerson.SalesPerson_Name,salesPerson.SalesPerson_Password,salesPerson.SalesPerson_Username];
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
    
}
-(NSArray*)selectSalespeople
{
    NSMutableArray *salesPersonArray = [[[NSMutableArray alloc] init] autorelease];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select code,name,password,username from Salesperson"];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            SalesPerson *salesPerson =[[SalesPerson alloc] init];
            NSString *code=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 0)];
            salesPerson.SalesPerson_Code = code;
            NSString *salesPerson_name=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
            salesPerson.SalesPerson_Name = salesPerson_name;
            NSString *password=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
            salesPerson.SalesPerson_Password = password;
            NSString *username=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
            salesPerson.SalesPerson_Username = username;
            [salesPersonArray addObject:salesPerson];
            [salesPerson release];
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return salesPersonArray;
}
-(SalesPerson*)selectSalesPersonWithUsername:(NSString*)username Password:(NSString*)password
{
    SalesPerson *salesPerson = nil;
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        NSString *selectSQL=[NSString stringWithFormat:@"select code,name,password,username from Salesperson where username='%@' and password='%@'",username, password];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        if(sqlite3_step(statement) == SQLITE_ROW)
        {
            salesPerson =[[[SalesPerson alloc] init] autorelease];
            NSString *code=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 0)];
            salesPerson.SalesPerson_Code = code;
            NSString *salesPerson_name=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
            salesPerson.SalesPerson_Name = salesPerson_name;
            NSString *password=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
            salesPerson.SalesPerson_Password = password;
            NSString *username=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 3)];
            salesPerson.SalesPerson_Username = username;
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return salesPerson;
}
-(DataBaseUpdateResult)updateSalesPerson:(SalesPerson*)salesPerson
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update Salesperson set name='%@',password='%@',username='%@' where code='%@';",salesPerson.SalesPerson_Name,salesPerson.SalesPerson_Password,salesPerson.SalesPerson_Username,salesPerson.SalesPerson_Code];
        sqlite3_stmt *statement;
        const char *update_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, update_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseUpdateSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseUpdateFailed;
}
-(DataBaseInsertionResult)insertSalesPersonArray:(NSArray*)salesPersonArr
{
    self.runBatch=YES;
    DataBaseInsertionResult insertionResult=DataBaseInsertionSuccessful;
    NSArray *existingSalesPeopl=[self selectSalespeople];
    [self openConnection];
    for(SalesPerson* salesPerson in salesPersonArr)
    {
        BOOL foundAMatch=NO;
        SalesPerson* matchedSalesPerson=nil;
        for(SalesPerson* eSalesPerson in existingSalesPeopl)
        {
            if([eSalesPerson.SalesPerson_Code isEqualToString:salesPerson.SalesPerson_Code])
            {
                foundAMatch=YES;
                matchedSalesPerson=eSalesPerson;
                break;
            }
        }
        if(!foundAMatch)
        {
            insertionResult=[self insertSalesPerson:salesPerson];
            if(insertionResult==DataBaseInsertionFailed)
            {
                break;
            }
        }
        else
        {
            if(matchedSalesPerson!=nil)
            {
                if(![matchedSalesPerson.SalesPerson_Password isEqualToString:salesPerson.SalesPerson_Password] ||
                   ![matchedSalesPerson.SalesPerson_Username isEqualToString:salesPerson.SalesPerson_Username])
                {
                    [self updateSalesPerson:salesPerson];
                }
            }
        }
    }
    [self closeConnection];
    return insertionResult;
}
-(DataBaseUpdateResult)updateSalesPersonId:(SalesPerson*)salesPerson
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *updateSQL = [NSString stringWithFormat:@"update Salesperson set order_id_prefix='%@' where code='%@';",salesPerson.orderId_Prefix, salesPerson.SalesPerson_Code];
        sqlite3_stmt *statement;
        const char *update_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, update_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseUpdateSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseUpdateFailed;
}
@end
