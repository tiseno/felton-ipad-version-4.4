//
//  OrderListTransfered.h
//  KDS
//
//  Created by Tiseno on 11/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderListTransferedCellViewController.h"
#import "KDSAppDelegate.h"
#import "KDSDataSalesOrder.h"
#import "NewOrderTransferRequest.h"
#import "NetworkHandler.h"
#import "LoadingView.h"
#import "NewOrderTransferResponse.h"
#import "Reachability.h"
#import "SalesOrder.h"
#import "OrderListTransferedDelegate.h"



@interface OrderListTransfered : UIViewController<NetworkHandlerDelegate> {
    
    UIView *_tableViewHeader;
    UITableView *_tableView;
    NSArray* newssOrdersNotTransferedArr;
    
    LoadingView *loadingView;
}

@property (nonatomic, retain) UIView *_tableViewHeader;
@property (nonatomic, retain) UITableView *_tableView;
@property (nonatomic, retain) NSArray* newssOrdersNotTransferedArr;
@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic, retain) SalesOrder* ssalesOrder;
-(void)reloadSalesOrderArray;
@end
