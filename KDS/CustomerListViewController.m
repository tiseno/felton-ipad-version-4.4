//
//  CustomerListViewController.m
//  KDS
//
//  Created by Tiseno on 12/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CustomerListViewController.h"



@implementation CustomerListViewController

@synthesize companyTableView, search, customerArray, customerArray_Copied;
@synthesize companyNameLabel, picLabel, contactNoLabel, telLabel, emailLabel, addressTextView, companyTableViewHeader, orderHistoryTableViewHeader, orderHistoryTableView, customerInfoView;
@synthesize orderHistoryTableViewController, currentSelectedCell;
@synthesize listGallery;
@synthesize lstGallery;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        if(appDelegate.loggedInSalesPerson!=nil)
        {
            KDSDataCustomer *dataCustomer=[[[KDSDataCustomer alloc] init] autorelease];
            NSArray *customers=[dataCustomer selectCustomerWithSalespersonID:appDelegate.loggedInSalesPerson.SalesPerson_Code];
            self.customerArray=customers;
        }
        
        //NSArray *items=[[NSArray alloc]init];
        //items=appDelegate.loadListGallerySatatus;
        
        //listGallery=appDelegate.loadListGallerySatatus;
        //lstGallery=appDelegate.loadListGallerySatatus;
        
        //NSLog(@"lstGallery222-->%@",lstGallery);
        
    }
    return self;
}

- (void)dealloc
{
    [lstGallery release];
    [companyTableView release];
    [search release];
    [customerArray release];
    [companyNameLabel release];
    [picLabel release];
    [contactNoLabel release]; 
    [telLabel release];
    [emailLabel release];
    [addressTextView release];
    [companyTableViewHeader release];
    [orderHistoryTableViewHeader release]; 
    [orderHistoryTableView release];
    [customerInfoView release];
    [orderHistoryTableViewController release];
    [customerArray_Copied release];
    if(customerArray!=nil)
        [customerArray release];
    [currentSelectedCell release];
    [super dealloc];
}
-(IBAction)chooseButtonTapped
{
    ((KDSAppDelegate*)[UIApplication sharedApplication].delegate).currentCustomer = currentSelectedCell;
    
    KDSAppDelegate *appDelegate=[UIApplication sharedApplication].delegate;

    NSString *listgalllery=[[NSString alloc]init];

    listgalllery=appDelegate.loadListGallerySatatus;
     

    if ([listgalllery length]==0) {
        listgalllery=@"list";
        
        KDSDataListGallery *dataItem=[[KDSDataListGallery alloc] init];
        [dataItem insertItem:listgalllery];

        [self listView];
    }
    else if([listgalllery isEqualToString:@"list"])
    {

        
        [self listView];
    }
    else if([listgalllery isEqualToString:@"gallery"])
    {

        
        [self galleryView];
    }  

    
    
    
    

    [listgalllery release];
}

-(void)listView
{
    NewOrderViewController *newOrderViewController=[[NewOrderViewController alloc] initWithNibName:@"NewOrderViewController" bundle:nil];
    newOrderViewController.title=@"Felton";
    
    [self.navigationController pushViewController:newOrderViewController animated:YES];
    [newOrderViewController release];
}

-(void)galleryView
{
    /*KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    SalesOrder* tempSalesOrder=appDelegate.salesOrder;
    [tempSalesOrder retain];
    
    NewOrderCategoryViewController *newOrderViewController=[[NewOrderCategoryViewController alloc] initWithNibName:@"NewOrder_CategoryViewController" bundle:nil];
    //NewOrderViewController *newOrderCategoryViewController=[[NewOrderViewController alloc] initWithNibName:@"NewOrderViewController" bundle:nil];
    appDelegate.salesOrder=tempSalesOrder;
    [tempSalesOrder release];
    
    */NewOrderCategoryViewController *newOrderViewController=[[NewOrderCategoryViewController alloc] initWithNibName:@"NewOrder_CategoryViewController" bundle:nil];     
     newOrderViewController.title=@"Felton";
     [self.navigationController pushViewController:newOrderViewController animated:YES];
     [newOrderViewController release]; 
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
        
    
    customerInfoView.alpha = 0.0;
    search.autocapitalizationType = UITextAutocapitalizationTypeNone;
    if(customerArray_Copied == nil)
    {
        NSMutableArray *tcustomerArray_Copied = [[[NSMutableArray alloc] init] autorelease];
        self.customerArray_Copied = tcustomerArray_Copied;
    }
    for(int i=0;i<[self.customerArray count];i++)
    {
        [customerArray_Copied addObject:[customerArray objectAtIndex:i]];
    }
    orderHistoryTableView.alpha = 0.0;
    /**/
    if(companyTableViewHeader == nil)
    {
        UIView *tCompanyTableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(companyTableView.frame.origin.x, companyTableView.frame.origin.y-47, companyTableView.frame.size.width, 46)];
        tCompanyTableHeaderView.backgroundColor = [UIColor whiteColor];
        
        UILabel *tCompanyTableHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, companyTableView.frame.size.width, 46)];
        tCompanyTableHeaderLabel.text = @"Company Name";
        tCompanyTableHeaderLabel.backgroundColor = [UIColor clearColor];
        tCompanyTableHeaderLabel.textColor = [UIColor colorWithRed:0.21 green:0.38 blue:0.43 alpha:1.0];
        tCompanyTableHeaderLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:19];
        tCompanyTableHeaderLabel.textAlignment = UITextAlignmentLeft;
        [tCompanyTableHeaderView addSubview:tCompanyTableHeaderLabel];
        
        self.companyTableViewHeader = tCompanyTableHeaderView;
        [self.view addSubview:self.companyTableViewHeader];
        [tCompanyTableHeaderView release];
        [tCompanyTableHeaderLabel release];
    }
    
    if(orderHistoryTableViewHeader == nil)
    {
        UIView *tOrderHistoryTableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(orderHistoryTableView.frame.origin.x, orderHistoryTableView.frame.origin.y-47, orderHistoryTableView.frame.size.width, 46)];
        tOrderHistoryTableHeaderView.backgroundColor = [UIColor whiteColor];
        
        UILabel *tOderHistoryTableHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, orderHistoryTableView.frame.size.width, 46)];
        tOderHistoryTableHeaderLabel.text = @"Order History";
        tOderHistoryTableHeaderLabel.backgroundColor = [UIColor clearColor];
        tOderHistoryTableHeaderLabel.textColor = [UIColor blackColor];
        tOderHistoryTableHeaderLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:19];
        tOderHistoryTableHeaderLabel.textAlignment = UITextAlignmentLeft;
        [tOrderHistoryTableHeaderView addSubview:tOderHistoryTableHeaderLabel];
        
        self.orderHistoryTableViewHeader = tOrderHistoryTableHeaderView ;
        [self.view addSubview:self.orderHistoryTableViewHeader];
        [tOrderHistoryTableHeaderView release];
        [tOderHistoryTableHeaderLabel release];
    }   
    orderHistoryTableViewHeader.alpha = 0.0;
    for(UIView* subView in search.subviews)
    {
        if([subView conformsToProtocol:@protocol(UITextInputTraits)])
        {
            [(UITextField*)subView setClearButtonMode:UITextFieldViewModeNever];
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
 
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [customerArray_Copied count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *sectionTableIdentifier = @"SectionsTableIdentifier";
    
    CompanyTableCellViewController *cell = (CompanyTableCellViewController*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
    if(cell == nil)
    {
        cell = [[[CompanyTableCellViewController alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier] autorelease];
        cell.orderHistoryGeneratorDelegate=self;
        
    }
    Customer *customer=[customerArray_Copied objectAtIndex:indexPath.row];
    
    cell.customer=customer;
    cell.lineColor = [UIColor blackColor];
    cell.companyNameIDLabel.text=customer.Customer_Id;
    cell.companyNameLabel.text = customer.Customer_Name;
    cell.picLabel.text = customer.Contact_Name;
    
    if(indexPath.row == 0)
        cell.topCell = YES;
    else
        cell.topCell = NO;

    
    return cell;
}
/**/
-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [search resignFirstResponder];
    return indexPath;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CompanyTableCellViewController *selectedCell = (CompanyTableCellViewController*) [tableView cellForRowAtIndexPath:indexPath];
    currentSelectedCell = selectedCell.customer; 
    self.companyNameLabel.text = selectedCell.customer.Customer_Name;
    self.picLabel.text = selectedCell.customer.Contact_Name;
    self.contactNoLabel.text = selectedCell.customer.Phone_1;
    self.telLabel.text = selectedCell.customer.Phone_2;
    self.emailLabel.text = selectedCell.customer.Email;
    self.addressTextView.text = [NSString stringWithFormat:@"%@\n%@\n%@", selectedCell.customer.Address.Street_1, selectedCell.customer.Address.Street_2, selectedCell.customer.Address.Street_3];
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    customerInfoView.alpha = 1.0;
    [UIView commitAnimations];
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CompanyTableCellViewController *selectedCell = (CompanyTableCellViewController*) [tableView cellForRowAtIndexPath:indexPath];
    [selectedCell.orderHistoryButton setBackgroundImage:[UIImage imageNamed:@"btn_order_history.png"] forState:UIControlStateNormal];
    
    if(orderHistoryTableViewController != nil)
    {
        orderHistoryTableViewController = nil;
    }
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    orderHistoryTableView.alpha = 0.0;
    orderHistoryTableViewHeader.alpha = 0.0;
    customerInfoView.alpha = 1.0;
    [UIView commitAnimations];
}

-(void)generateOderHistoryTable:(id)sender
{
    if(orderHistoryTableView.alpha == 0.0)
    {
        [((UIButton*)sender) setBackgroundImage:[UIImage imageNamed:@"btn_customer_info.png"] forState:UIControlStateNormal];
        if(orderHistoryTableViewController == nil)
        {
            OrderHistoryTableViewController *tOrderHistoryTableViewController = [[OrderHistoryTableViewController alloc] init];
            self.orderHistoryTableViewController = tOrderHistoryTableViewController;
            [tOrderHistoryTableViewController release];
        }
        [orderHistoryTableView setDelegate:self.orderHistoryTableViewController];
        [orderHistoryTableView setDataSource:self.orderHistoryTableViewController];
        self.orderHistoryTableViewController.view = self.orderHistoryTableViewController.tableView;
        [orderHistoryTableViewController orderHistoryForCustomer:currentSelectedCell];

        [self.orderHistoryTableView reloadData];
        
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        orderHistoryTableViewHeader.alpha = 1.0;
        orderHistoryTableView.alpha = 1.0;
        customerInfoView.alpha = 0.0;
        [UIView commitAnimations];
    }else
    { 
        [((UIButton*)sender) setBackgroundImage:[UIImage imageNamed:@"btn_order_history.png"] forState:UIControlStateNormal];
        [UIView beginAnimations:@"" context:nil];
        [UIView setAnimationDuration:0.2];
        orderHistoryTableViewHeader.alpha = 0.0;
        orderHistoryTableView.alpha = 0.0;
        customerInfoView.alpha = 1.0;
        [UIView commitAnimations];
    }
    
}

-(void)handleSearchForTerm:(NSString*)searchTerm
{
    [customerArray_Copied removeAllObjects];
    for(Customer* customer in customerArray)
    {
        if([customer.Customer_Name rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location != NSNotFound)
            [customerArray_Copied addObject:customer];
    }
    [companyTableView reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *searchTerm = [searchBar text];
    [self handleSearchForTerm:searchTerm];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText isEqualToString:@""] || searchText == nil)
    {
        [customerArray_Copied removeAllObjects];
        [customerArray_Copied addObjectsFromArray:(customerArray)];
        [companyTableView reloadData];
        return;
    }
    [self handleSearchForTerm:searchText];
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if([searchBar.text isEqualToString:@""] || searchBar.text==nil)
    {
        
        [companyTableView reloadData];
        return;
    }
    [self handleSearchForTerm:searchBar.text];
}
/**/
@end
