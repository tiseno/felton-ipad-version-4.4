//
//  YourCartTableCellDelegate.h
//  KDS
//
//  Created by Jermin Bazazian on 12/11/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol YourCartTableCellDelegate <NSObject>
-(void)tableCell:(UITableViewCell*)cell ReplaceOrderItemAtIndex:(int)orderItemIndex;
-(void)tableCell:(UITableViewCell*)cell RemovedOrderItemAtIndex:(int)orderItemIndex;
-(void)updateAlertText;
@end
