//
//  KDSDataListGallery.m
//  KDS
//
//  Created by Tiseno Mac 2 on 8/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "KDSDataListGallery.h"

@implementation KDSDataListGallery
@synthesize runBatch;

-(DataBaseInsertionResult)insertItem:(NSString*)tlistGstatus
{
    BOOL connectionIsOpenned=YES;
    if(!runBatch)
    {
        if([self openConnection] != DataBaseConnectionOpened)
            connectionIsOpenned=NO;
    }
    if(connectionIsOpenned)
    {
        NSString *insertSQL;
        //NSLog(@"tlogin.member_id-->%@",tlogin.member_id);
        if([tlistGstatus length]!=0)
        {
            //NSString *itemDescription=[tItem.Description stringByReplacingOccurrencesOfString:@"'" withString:@"\""]; 
            //NSString *itemNo=[tItem.Item_No stringByReplacingOccurrencesOfString:@"'" withString:@"\""];
            
            insertSQL = [NSString stringWithFormat:@"insert into tblGallery(ListGallery) values('%@');",tlistGstatus];
            
            
        }
        sqlite3_stmt *statement;
        const char *insert_stmt = [insertSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            
            sqlite3_finalize(statement);
            if(!runBatch)
            {
                [self closeConnection];
            }
            return DataBaseInsertionSuccessful;
        }
        //NSLog(@"%@",insertSQL);
        //NSAssert1(0, @"Failed to insert with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        if(!runBatch)
        {
            [self closeConnection];
        }
    }
    return DataBaseInsertionFailed;
}

-(NSArray*)selectItem
{
    NSMutableArray *itemarr = [[[NSMutableArray alloc] init] autorelease];
    
    if([self openConnection]==DataBaseConnectionOpened)
    {
        //NSString *selectSQL=[NSString stringWithFormat:@"select code,name,password,username from Salesperson"];
        NSString *selectSQL=[NSString stringWithFormat:@"select g_id, ListGallery from tblGallery"];
        
        sqlite3_stmt *statement;
        const char *select_stmt = [selectSQL UTF8String];
        sqlite3_prepare_v2(dbKDS, select_stmt, -1, &statement, NULL);
        
        
        
        while(sqlite3_step(statement) == SQLITE_ROW)
        {
            
            
            /**/listgallery *tlistG= [[listgallery alloc] init];
            
            //tlogin.login_id=curItemID;
            
            //int itemID=sqlite3_column_int(statement, 0);
            //tlistG.login_id=itemID;
            
            NSString *listGstatus=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
            
            //int listGstatus=sqlite3_column_int(statement, 1);
            tlistG.listgallerystatus=listGstatus;
            
            
            /*//NSLog(@"controlCStr--->%@",controlCStr);
            if(controlCStr)
            {
                //NSString *memberid=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 1)];
                NSString *memberid=[NSString stringWithUTF8String:(char*)controlCStr];
                tlogin.member_id = memberid;
            }
            
            
            //NSString *rememberid=[NSString stringWithUTF8String:(char*)sqlite3_column_text(statement, 2)];
            int rememberid=sqlite3_column_int(statement, 2);
            tlogin.remember_id = rememberid;
            
            
            */
            
            [itemarr addObject:tlistG];

            [tlistG release];
            
            
            
            
        }
        sqlite3_finalize(statement);
        [self closeConnection];
    }
    return itemarr;
}

-(DataBaseUpdateResult)updateLogin:(NSString*)tllistg
{
    if([self openConnection]==DataBaseConnectionOpened)
    {
        
        //NSLog(@"db tlogin.remember_id----->%d",tlogin.remember_id);
        
        //NSString *rememberid=[[NSString alloc]initWithFormat:@"%d",tlogin.remember_id];
        
        NSString* updateSQL=[NSString stringWithFormat:@"update tblGallery set ListGallery='%@' where g_id=1 ;", tllistg];
        sqlite3_stmt *statement;
        const char *insert_stmt = [updateSQL UTF8String];
        
        sqlite3_prepare_v2(dbKDS, insert_stmt, -1, &statement, NULL);
        if(sqlite3_step(statement) == SQLITE_DONE)
        {
            sqlite3_finalize(statement);
            [self closeConnection];
            return DataBaseDeletionSuccessful;
        }
        NSAssert1(0, @"Failed to update with message '%s'.", sqlite3_errmsg(dbKDS));
        sqlite3_finalize(statement);
        [self closeConnection];
        return DataBaseUpdateSuccessful;
    }
    else
    {
        return DataBaseUpdateFailed;
    }
    
}

@end
