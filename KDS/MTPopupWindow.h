//
//  MTPopupWindow.h
//  KDS
//
//  Created by Tiseno Mac 2 on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ItemSelecteddelegate.h"
#import "Item.h"

@interface MTPopupWindow : NSObject
{
    UIView* bgView;
    UIView* bigPanelView;
    id<ItemSelecteddelegate> delegate;
}

+(void)showWindowWithHTMLFile:(Item *)fileName insideView:(UIView*)view;

@property (nonatomic, retain) id<ItemSelecteddelegate> delegate;
@property (nonatomic, retain) Item *selectedItem;

@end
