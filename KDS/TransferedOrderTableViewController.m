//
//  TransferedOrderTableViewController.m
//  KDS
//
//  Created by Tiseno on 11/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TransferedOrderTableViewController.h"



@implementation TransferedOrderTableViewController
@synthesize transferedOrderArr,OrdertransferedViewController;
@synthesize tTransferecSalesOrderPackageArr;

-(id)init
{
    self=[super init];
    if(self)
    { 
        KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        KDSDataSalesOrder *dataSalesOrder=[[KDSDataSalesOrder alloc] init];
        NSArray* tTransferecSalesOrderArr=[dataSalesOrder selectSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:YES IsDeleted:NO];
        self.transferedOrderArr=tTransferecSalesOrderArr;
        [dataSalesOrder release];
        
        //KDSDateOrderPackage *dataSalesOrderPackage=[[KDSDateOrderPackage alloc] init];
        //NSArray* tTransferecSalesOrderPackageArr=[dataSalesOrder selectOrderPackageID];
        //tTransferecSalesOrderPackageArr=[dataSalesOrderPackage selectOrderPackageID];
    }
    return  self;
}

- (void)dealloc
{
    [tTransferecSalesOrderPackageArr release];
    [transferedOrderArr release];
    [super dealloc];
}
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}
-(NSInteger) tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
	if(self.transferedOrderArr!=nil)
    {
        return [self.transferedOrderArr count];
    }
    return 0;
} 
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    static NSString *CellIdentifier = @"Cell";
    //TransferedOrderTableViewCell *cell = (TransferedOrderTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    SalesOrder* salesOrder=[self.transferedOrderArr objectAtIndex:indexPath.row];
    //if(cell == nil)
    //{
        TransferedOrderTableViewCell* cell = [[TransferedOrderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        //cell = [[TransferedOrderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.lineColor = [UIColor blackColor];
        cell.salesOrder = salesOrder;
        cell.delegate = self.OrdertransferedViewController;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.nameLabel.text = salesOrder.customer.Customer_Name;
    
    //salesOrder.systemOrderId
    
    //KDSDateOrderPackage *dataSalesOrder=[[KDSDateOrderPackage alloc] init];
    //NSArray* tTransferecSalesOrderPackageArr=[dataSalesOrder selectOrderPackageID];

    /*for(OrderPackaging* orderpItem in tTransferecSalesOrderPackageArr)
    {
        //[self.salesOrder addOrderItem:orderItem]; 
        NSLog(@"orderItem-->%@",orderpItem.orderID);
    }*/ 
    
    //[dataSalesOrder release];
    
    //NSLog(@"orderItem-->%@",salesOrder.OrderPackageID);

    cell.PackageIDLabel.text=salesOrder.OrderPackageID;
        
    NSString *fullString = salesOrder.Order_Reference;
    NSString *prefix = nil;
    
    if ([fullString length] >= 22)
    {
        prefix = [fullString substringToIndex:22];
    }else {
        prefix=@" ";
    }

        cell.lblTransferredDate.text=prefix;
        NSString *grandPrice = [NSString stringWithFormat:@"RM %.2f", salesOrder.grand_Price];
        cell.priceLabel.text = grandPrice; 
     
    NSString *createdate = salesOrder.systemOrderId;
    
    NSString *prefixdd = nil;
    NSString *prefixmm = nil;
    NSString *prefixyy = nil;
    
    NSString *prefixcd = nil;
    

    if ([createdate length] >= 10)
    {
        prefixdd= [createdate substringFromIndex:6];
        prefixdd= [prefixdd substringToIndex:2];
        
        prefixmm= [createdate substringFromIndex:4];
        prefixmm= [prefixmm substringToIndex:2];
        
        //prefixyy= [createdate substringFromIndex:4];
        prefixyy= [createdate substringToIndex:4];
        
        prefixcd = [createdate substringToIndex:10];
        
    }else {
        prefixcd=@" ";
    }
    
    NSString *createdarws = [NSString stringWithFormat:@"Created %@-%@-%@", prefixdd, prefixmm, prefixyy ];
    NSLog(@"%@",prefixcd);
    cell.lblCreatedDate.text=createdarws;
    
    
        
        if(indexPath.row == 0)
            cell.topCell = YES;
        else
            cell.topCell = NO;
    //}
    return cell;
}
-(void)initializeTableDataR
{
    KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    KDSDataSalesOrder *dataSalesOrder;
    if(appDelegate.TransferOrdersalesOrder.Type==SalesOrderMain)
    {
        dataSalesOrder = [[KDSDataSalesOrder alloc] init];
    }
    else //if(appDelegate.TransferOrdersalesOrder.Type==SalesOrderBlind)
    {
        dataSalesOrder = [[KDSDataBlindSalesOrder alloc] init];
    }
    NSArray* tTransferdSalesOrderArr=[dataSalesOrder selectSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:YES IsDeleted:NO];
    self.transferedOrderArr=tTransferdSalesOrderArr;
    [dataSalesOrder release];
}

@end
