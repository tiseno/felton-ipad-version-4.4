//
//  ResponseTranslator.m
//  KDSWSLayer
//
//  Created by Jermin Bazazian on 12/5/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//

#import "ResponseTranslator.h"


@implementation ResponseTranslator
-(XMLResponse*)translate:(NSData*) xmlData
{
    XMLResponse *message=nil;
    NSError *error;
    if([xmlData length]!=0)
    {
        GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
        if(doc)
        {
            if([[doc.rootElement name] isEqualToString:@"ArrayOfSalesperson"])
            {
                message=[self translateAllSalesPeople:xmlData];
            }
            else if([[doc.rootElement name] isEqualToString:@"ArrayOfCustomer"])
            {
                message=[self translateCustomersBySalespersonID:xmlData];
            }
            else if([[doc.rootElement name] isEqualToString:@"ArrayOfItem"])
            {
                message=[self translateAllItemDetails:xmlData];
            }
            else if([[doc.rootElement name] isEqualToString:@"BizEntityConvertor"])
            {
                message=[self translategetPDFOrder:xmlData];
            }
            else if([[doc.rootElement name] isEqualToString:@"BizEntityQuotationConvertor"])
            {
                message=[self translategetPDFQuotationr:xmlData];
            }
            else if([[doc.rootElement name] isEqualToString:@"BizEntityTransfer"])
            {
                message=[self translateOrder:xmlData];
                
            }else if([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityItemImage"])
            {
                message=[self translateImagePath:xmlData];
                
            }else if([[doc.rootElement name] isEqualToString:@"ArrayOfMainItem"]) 
            {
                message=[self translateMainItemDetails:xmlData];
                
            }else if([[doc.rootElement name] isEqualToString:@"ArrayOfBlindsItem"]) 
            {
                message=[self translateBlindItemDetails:xmlData];
                
            }else if([[doc.rootElement name] isEqualToString:@"ArrayOfBizEntityPackagingID"]) 
            {
                message=[self translateOrderPackage:xmlData];
                
            }
            
            
            [doc release];
        }
        else 
        {
            NSLog(@"%@: Error decoding the document: %@", [self class], [error localizedDescription]);
        }
    }
    return message;
}

-(XMLResponse*)translateImagePath:(NSData *)xmlData
{
    NSError *error;
	GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    DownloadItemImagesReaponse *msg =nil;
    
    if (doc) 
    {
        NSArray *itemArr = [doc.rootElement elementsForName:@"BizEntityItemImage"];
        if (itemArr.count>0) 
        {
            msg=[[[DownloadItemImagesReaponse alloc] init] autorelease];
            NSMutableArray *items=[[NSMutableArray alloc] initWithCapacity:itemArr.count];
            for(GDataXMLElement *itemElement in itemArr)
            {
                /*DownloadImages *item=[[DownloadImages alloc]init];
                NSArray *imagePathArr = [itemElement elementsForName:@"Path"];
                if (imagePathArr.count>0) 
                {
                    GDataXMLElement *imagePathElement=(GDataXMLElement *) [imagePathArr objectAtIndex:0];
                    item.Image_Path=[imagePathElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    //To be a complete offline solution we need to load the images while sync is in progress.
                    AsyncImageView *asyncImageView=[[AsyncImageView alloc] init];
                    NSString *itemImageLocalPath=[item.Image_Path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                    NSString *itemImageURLPath=[NSString stringWithFormat:@"http://219.94.43.102/FeltonCMS/images/products/%@",itemImageLocalPath];
                    [asyncImageView setDelegate:item];
                    [asyncImageView loadImageFromPath:itemImageURLPath];
                    [asyncImageView release];
                    NSLog(@"Image downloaded");
                    
                    AsyncImageView *asyncImageView=[[AsyncImageView alloc] init];
                    NSString *itemImageLocalPath=[item.Image_Path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                    NSString *itemImageURLPath=[NSString stringWithFormat:@"http://219.94.43.102/FeltonCMS/images/products/%@",itemImageLocalPath];
                    [asyncImageView setDelegate:item];
                    [asyncImageView loadImageFromPath:itemImageURLPath];
                    [asyncImageView release];
                    NSLog(@"Image downloaded");
                }*/
                
                DownloadImages *item=[[DownloadImages alloc]init];
                NSArray *imagePathArr = [itemElement elementsForName:@"Path"];
                if (imagePathArr.count>0) 
                {
                    GDataXMLElement *imagePathElement=(GDataXMLElement *) [imagePathArr objectAtIndex:0];
                    item.Image_Path=[imagePathElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    //To be a complete offline solution we need to load the images while sync is in progress.
                    /**///AsyncImageView *asyncImageView=[[AsyncImageView alloc] init];
                    NSString *itemImageLocalPath=[item.Image_Path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                    //NSString *itemImageURLPath=[NSString stringWithFormat:@"http://219.94.43.102/feltonCMS/images/products/%@",itemImageLocalPath];
                    /*[asyncImageView setDelegate:item];
                     [asyncImageView loadImageFromPath:itemImageURLPath];
                     [asyncImageView release];
                     //NSLog(@"Image downloaded");
                     
                     NSData* pngData = UIImagePNGRepresentation(image);*/
                    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
                    NSError *error;
                    NSString *strhost=[appDelegate loadHostIntoString];
                    NSString *settingFileContents=[NSString stringWithContentsOfFile:strhost encoding:NSASCIIStringEncoding error:&error];
                    NSString *HostIP = [NSString stringWithFormat:@"http://%@/feltonCMS/images/products/%@",settingFileContents,itemImageLocalPath];
                    
                    NSString *imgurl=[[NSString alloc]initWithFormat:@"%@",HostIP];
                    
                    UIImageView *imgview =[[UIImageView alloc]initWithFrame:CGRectMake(69, 348, 200, 200)];
                    imgview.image=[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imgurl]]];
                    
                    NSData* pngData = UIImagePNGRepresentation(imgview.image);
                    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString* documentsPath = [paths objectAtIndex:0];
                    NSString* filePath = [documentsPath stringByAppendingPathComponent:item.Image_Path];
                    [pngData writeToFile:filePath atomically:YES];
                    
                    //NSString *xxx=[[NSString alloc]initWithFormat:@"Image Saved--->%@",item.Image_Path];
                    //NSLog(@"%@ done.",xxx);    
                    //[imgview release];
                }
                
                [items addObject:item];
                [item release];
                
            }  
            msg.ImagePathArr=items;
            [items release];
        }
        [doc release];
    } 
    return msg;
}

-(XMLResponse*) translateOrder:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    NewOrderTransferResponse *msg =nil;
    
    if (doc) {
        NSArray *FeedBackResponseResultArr = [doc.rootElement elementsForName:@"response"];
        if (FeedBackResponseResultArr.count > 0) 
        {
            msg=[[[NewOrderTransferResponse alloc] init] autorelease];
            GDataXMLElement *FeedBackResponseResultElement=(GDataXMLElement *)[FeedBackResponseResultArr objectAtIndex:0];
            msg.getOrderResponse=FeedBackResponseResultElement.stringValue;
            
        }
        [doc release];
    } 
    return msg;
}


-(XMLResponse*) translategetPDFOrder:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    TransferOrderPDFResponse *msg =nil;
    
    if (doc) {
        NSArray *FeedBackResponseResultArr = [doc.rootElement elementsForName:@"filenam"];
        if (FeedBackResponseResultArr.count > 0)
        {
            msg=[[[TransferOrderPDFResponse alloc] init] autorelease];
            GDataXMLElement *FeedBackResponseResultElement=(GDataXMLElement *)[FeedBackResponseResultArr objectAtIndex:0];
            msg.getPDFPath=FeedBackResponseResultElement.stringValue;
            
        }
        [doc release];
    }
    return msg;
}

-(XMLResponse*) translategetPDFQuotationr:(NSData *)xmlData
{
    NSError *error;
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    QuotationFormPdfResponse *msg =nil;
    
    if (doc) {
        NSArray *FeedBackResponseResultArr = [doc.rootElement elementsForName:@"filenam"];
        if (FeedBackResponseResultArr.count > 0)
        {
            msg=[[[QuotationFormPdfResponse alloc] init] autorelease];
            GDataXMLElement *FeedBackResponseResultElement=(GDataXMLElement *)[FeedBackResponseResultArr objectAtIndex:0];
            msg.getPDFPath=FeedBackResponseResultElement.stringValue;
            
        }
        [doc release];
    }
    return msg;
}

-(XMLResponse*)translateOrderPackage:(NSData*) xmlData;
{
    NSError *error;
	GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    OrderPackagingResponse *message=nil;
	if(doc)
	{
        NSArray *salePersonArr = [doc.rootElement elementsForName:@"BizEntityPackagingID"];
        if (salePersonArr.count>0) 
        {
            message=[[[OrderPackagingResponse alloc] init] autorelease];
            NSMutableArray *MuTblOrderPackage=[[NSMutableArray alloc] initWithCapacity:salePersonArr.count];
            for(GDataXMLElement *salePersonElement in salePersonArr)
            {
                //SalesPerson *salesPerson=[[SalesPerson alloc] init];
                
                SalesOrder *Orderpakaging=[[SalesOrder alloc]init];
                
                NSArray *salespersonIDArr = [salePersonElement elementsForName:@"orderID"];
                if (salespersonIDArr.count>0) {
                    GDataXMLElement *salespersonIDElement=(GDataXMLElement *) [salespersonIDArr objectAtIndex:0];
                    Orderpakaging.systemOrderId=[salespersonIDElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *salespersonNameArr = [salePersonElement elementsForName:@"packageID"];
                if (salespersonNameArr.count>0) {
                    GDataXMLElement *salespersonNameElement=(GDataXMLElement *) [salespersonNameArr objectAtIndex:0];
                    Orderpakaging.OrderPackageID=[salespersonNameElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
 
                [MuTblOrderPackage addObject:Orderpakaging];
                [Orderpakaging release];
            }
            message.OrderPackage=MuTblOrderPackage; 
            [MuTblOrderPackage release];
        }
        [doc release];
    }
    return message;
}

-(XMLResponse*)translateAllSalesPeople:(NSData*) xmlData
{
    NSError *error;
	GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    AllSalesPeopleResponse *message=nil;
	if(doc)
	{
        NSArray *salePersonArr = [doc.rootElement elementsForName:@"Salesperson"];
        if (salePersonArr.count>0) 
        {
            message=[[[AllSalesPeopleResponse alloc] init] autorelease];
            NSMutableArray *salesPeople=[[NSMutableArray alloc] initWithCapacity:salePersonArr.count];
            for(GDataXMLElement *salePersonElement in salePersonArr)
            {
                SalesPerson *salesPerson=[[SalesPerson alloc] init];
                NSArray *salespersonIDArr = [salePersonElement elementsForName:@"SalespersonID"];
                if (salespersonIDArr.count>0) {
                    GDataXMLElement *salespersonIDElement=(GDataXMLElement *) [salespersonIDArr objectAtIndex:0];
                    salesPerson.SalesPerson_Code=[salespersonIDElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSLog(@"salesPerson.SalesPerson_Code>>>>>%@",salesPerson.SalesPerson_Code);
                }
                NSArray *salespersonNameArr = [salePersonElement elementsForName:@"SalespersonName"];
                if (salespersonNameArr.count>0) {
                    GDataXMLElement *salespersonNameElement=(GDataXMLElement *) [salespersonNameArr objectAtIndex:0];
                    salesPerson.SalesPerson_Name=[salespersonNameElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSLog(@"salesPerson.SalesPerson_Name>>>>>%@",salesPerson.SalesPerson_Name);
                }
                NSArray *salesPersonUserNameArr = [salePersonElement elementsForName:@"SalesPersonUserName"];
                if (salesPersonUserNameArr.count>0) {
                    GDataXMLElement *salesPersonUserNameElement=(GDataXMLElement *) [salesPersonUserNameArr objectAtIndex:0];
                    salesPerson.SalesPerson_Username=[salesPersonUserNameElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSLog(@"salesPerson.SalesPerson_Username>>>>>%@",salesPerson.SalesPerson_Username);
                }
                NSArray *salesPersonPasswordArr = [salePersonElement elementsForName:@"SalesPersonPassword"];
                if (salesPersonPasswordArr.count>0) {
                    GDataXMLElement *salesPersonPasswordElement=(GDataXMLElement *) [salesPersonPasswordArr objectAtIndex:0];
                    salesPerson.SalesPerson_Password=[salesPersonPasswordElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSLog(@"salesPerson.SalesPerson_Password>>>>>%@",salesPerson.SalesPerson_Password);
                }
                NSArray *salesPersonOrderIdPrefixArr = [salePersonElement elementsForName:@"SalesPersonOrderIdPrefix"];
                if (salesPersonOrderIdPrefixArr.count>0) {
                    GDataXMLElement *salesPersonOrderIdPrefixElement=(GDataXMLElement *) [salesPersonOrderIdPrefixArr objectAtIndex:0];
                    salesPerson.orderId_Prefix=[salesPersonOrderIdPrefixElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                [salesPeople addObject:salesPerson];
                [salesPerson release];
            }
            message.salesPeople=salesPeople;
            [salesPeople release];
        }
        [doc release];
    }
    return message;
}


-(XMLResponse*)translateAllItemDetails:(NSData *)xmlData
{
    NSError *error;
	GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    AllItemDetailsResponse *message=nil;
	if(doc)
	{
        NSArray *itemArr = [doc.rootElement elementsForName:@"Item"];
        if (itemArr.count>0) {
            message=[[[AllItemDetailsResponse alloc] init] autorelease];
            NSMutableArray *items=[[NSMutableArray alloc] initWithCapacity:itemArr.count];
            for(GDataXMLElement *itemElement in itemArr)
            {
                Item *item=nil;
                NSArray *categoryArr = [itemElement elementsForName:@"Category"];
                if (categoryArr.count>0) {
                    GDataXMLElement *categoryElement=(GDataXMLElement *) [categoryArr objectAtIndex:0];
                    NSString* categoryStr=[categoryElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    if([categoryStr isEqualToString:kBlindCategory] || [categoryStr isEqualToString:kblindACategoryA])
                    {
                        item=[[BlindItem alloc] init];
                    }
                    else
                    {
                        item=[[Item alloc] init];
                    }
                    item.Category=categoryStr;
                }
                NSArray *itemNoArr = [itemElement elementsForName:@"ItemNo"];
                if (itemNoArr.count>0) {
                    GDataXMLElement *itemNoElement=(GDataXMLElement *) [itemNoArr objectAtIndex:0];
                    item.Item_No=[itemNoElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *descriptionArr = [itemElement elementsForName:@"Description"];
                if (descriptionArr.count>0) {
                    GDataXMLElement *descriptionElement=(GDataXMLElement *) [descriptionArr objectAtIndex:0];
                    item.Description=[descriptionElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *stockUnitArr = [itemElement elementsForName:@"StockUnit"];
                if (stockUnitArr.count>0) {
                    GDataXMLElement *stockUnitElement=(GDataXMLElement *) [stockUnitArr objectAtIndex:0];
                    item.Stock_Unit=[stockUnitElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *imagePathArr = [itemElement elementsForName:@"ImagePath"];
                if (imagePathArr.count>0) {
                    GDataXMLElement *imagePathElement=(GDataXMLElement *) [imagePathArr objectAtIndex:0];
                    item.Image_Path=[imagePathElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    //To be a complete offline solution we need to load the images while sync is in progress.
                    /*AsyncImageView *asyncImageView=[[AsyncImageView alloc] init];
                    NSString *itemImageLocalPath=[item.Image_Path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                    NSString *itemImageURLPath=[NSString stringWithFormat:@"http://219.94.43.102/KDSCMS/images/products/%@",itemImageLocalPath];
                    [asyncImageView setDelegate:item];
                    [asyncImageView loadImageFromPath:itemImageURLPath];
                    [asyncImageView release];
                    NSLog(@"Image downloaded");*/
                }
                NSArray *UOMAndConversionArr = [itemElement elementsForName:@"UOMAndConversion"];
                if (UOMAndConversionArr.count>0) {
                    GDataXMLElement *UOMAndConversionElement=(GDataXMLElement *) [UOMAndConversionArr objectAtIndex:0];
                    NSArray *UOMAndConversionArrL2 = [UOMAndConversionElement elementsForName:@"UOMAndConversion"];
                    if(UOMAndConversionArrL2.count>0)
                    {
                        NSMutableArray *uoms=[[NSMutableArray alloc] initWithCapacity:UOMAndConversionArrL2.count];
                        for(GDataXMLElement *UOMAndConversionL2Element in UOMAndConversionArrL2)
                        {
                            UOMAndConversion *uom=[[UOMAndConversion alloc] init];
                            NSArray *UOMArr = [UOMAndConversionL2Element elementsForName:@"UOM"];
                            if (UOMArr.count>0) {
                                GDataXMLElement *UOMElement=(GDataXMLElement *) [UOMArr objectAtIndex:0];
                                uom.Uom=[UOMElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            }
                            NSArray *conversionArr = [UOMAndConversionL2Element elementsForName:@"Conversion"];
                            if (conversionArr.count>0) {
                                GDataXMLElement *conversionElement=(GDataXMLElement *) [conversionArr objectAtIndex:0];
                                uom.Converstion=[conversionElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            }
                            [uoms addObject:uom];
                            [uom release];
                        }
                        item.UOMAndConversion=uoms;
                        [uoms release];
                    }
                }
                NSArray *defaultPriceAndUOMArr = [itemElement elementsForName:@"DefaultPriceAndUOM"];
                if (defaultPriceAndUOMArr.count>0) {
                    GDataXMLElement *defaultPriceAndUOMElement=(GDataXMLElement *) [defaultPriceAndUOMArr objectAtIndex:0];
                    NSArray *defaultPriceAndUOMArrL2 = [defaultPriceAndUOMElement elementsForName:@"DefaultPriceAndUOM"];
                    if(defaultPriceAndUOMArrL2.count>0)
                    {
                        NSMutableArray *defaultPrices=[[NSMutableArray alloc] initWithCapacity:defaultPriceAndUOMArrL2.count];
                        for(GDataXMLElement *defaultPriceAndUOML2Element in defaultPriceAndUOMArrL2)
                        {
                            DefaultPriceAndUOM *defaultPrice=[[DefaultPriceAndUOM alloc] init];
                            NSArray *defaultPriceArr = [defaultPriceAndUOML2Element elementsForName:@"DefaultPrice"];
                            if (defaultPriceArr.count>0) {
                                GDataXMLElement *defaultPriceElement=(GDataXMLElement *) [defaultPriceArr objectAtIndex:0];
                                defaultPrice.Default_Price=[defaultPriceElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            }
                            NSArray *UOMArr = [defaultPriceAndUOML2Element elementsForName:@"UOM"];
                            if (UOMArr.count>0) {
                                GDataXMLElement *UOMElement=(GDataXMLElement *) [UOMArr objectAtIndex:0];
                                defaultPrice.Uom=[UOMElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            }
                            [defaultPrices addObject:defaultPrice];
                            [defaultPrice release];
                        }
                        item.Default_Price=defaultPrices;
                        [defaultPrices release];
                    }
                }
                if([item.Category isEqualToString:kBlindCategory])
                {
                    NSArray *minHeightArr = [itemElement elementsForName:@"MinHeight"];
                    if (minHeightArr.count>0) {
                        GDataXMLElement *minHeightElement=(GDataXMLElement *) [minHeightArr objectAtIndex:0];
                        ((BlindItem*)item).min_Height=[minHeightElement.stringValue floatValue];
                    }
                    NSArray *minSQFTArr = [itemElement elementsForName:@"MinSqFt"];
                    if (minSQFTArr.count>0) {
                        GDataXMLElement *minSQFTElement=(GDataXMLElement *) [minSQFTArr objectAtIndex:0];
                        ((BlindItem*)item).min_SqFt=[minSQFTElement.stringValue floatValue];
                    }
                }
                [items addObject:item];
                [item release];
            }
            message.items=items;
            [items release];
        }
        [doc release];
    }
    return message;
}

-(XMLResponse*)translateCustomersBySalespersonID:(NSData *)xmlData
{
    NSError *error;
	GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    CustomersBySalespersonIDResponse *message=nil;
	if(doc)
	{
        NSArray *customerArr = [doc.rootElement elementsForName:@"Customer"];
        if (customerArr.count>0) {
            message=[[[CustomersBySalespersonIDResponse alloc] init] autorelease];
            NSMutableArray *customers=[[NSMutableArray alloc] initWithCapacity:customerArr.count];
            for(GDataXMLElement *customerElement in customerArr)
            {
                Customer *customer=[[Customer alloc] init];
                NSArray *customerIDArr = [customerElement elementsForName:@"CustomerID"];
                if (customerIDArr.count>0) {
                    GDataXMLElement *customerIDElement=(GDataXMLElement *) [customerIDArr objectAtIndex:0];
                    customer.Customer_Id=[customerIDElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *customerNameArr = [customerElement elementsForName:@"CustomerName"];
                if (customerNameArr.count>0) {
                    GDataXMLElement *customerNameElement=(GDataXMLElement *) [customerNameArr objectAtIndex:0];
                    customer.Customer_Name=[customerNameElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *contactNameArr = [customerElement elementsForName:@"ContactName"];
                if (contactNameArr.count>0) {
                    GDataXMLElement *contactNameElement=(GDataXMLElement *) [contactNameArr objectAtIndex:0];
                    customer.Contact_Name=[contactNameElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *street1Arr = [customerElement elementsForName:@"Street1"];
                if (street1Arr.count>0) {
                    Address *address=[[Address alloc] init];
                    GDataXMLElement *street1Element=(GDataXMLElement *) [street1Arr objectAtIndex:0];
                    address.Street_1=[street1Element.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    NSArray *street2Arr = [customerElement elementsForName:@"Street2"];
                    if (street2Arr.count>0) {
                        GDataXMLElement *street2Element=(GDataXMLElement *) [street2Arr objectAtIndex:0];
                        address.Street_2=[street2Element.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    }
                    NSArray *street3Arr = [customerElement elementsForName:@"Street3"];
                    if (street3Arr.count>0) {
                        GDataXMLElement *street3Element=(GDataXMLElement *) [street3Arr objectAtIndex:0];
                        address.Street_3=[street3Element.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    }
                    NSArray *cityArr = [customerElement elementsForName:@"City"];
                    if (cityArr.count>0) {
                        GDataXMLElement *cityElement=(GDataXMLElement *) [cityArr objectAtIndex:0];
                        address.City=[cityElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    }
                    NSArray *zipCodeArr = [customerElement elementsForName:@"ZipCode"];
                    if (zipCodeArr.count>0) {
                        GDataXMLElement *zipCodeElement=(GDataXMLElement *) [zipCodeArr objectAtIndex:0];
                        address.ZipCode=[zipCodeElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    }
                    NSArray *postalCodeArr = [customerElement elementsForName:@"PostalCode"];
                    if (postalCodeArr.count>0) {
                        GDataXMLElement *postalCodeElement=(GDataXMLElement *) [postalCodeArr objectAtIndex:0];
                        address.PostalCode=[postalCodeElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    }
                    NSArray *countryArr = [customerElement elementsForName:@"Country"];
                    if (countryArr.count>0) {
                        GDataXMLElement *countryElement=(GDataXMLElement *) [countryArr objectAtIndex:0];
                        address.Country=[countryElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    }
                    customer.Address=address;
                    [address release];
                }
                NSArray *phone1Arr = [customerElement elementsForName:@"Phone1"];
                if (phone1Arr.count>0) {
                    GDataXMLElement *phone1Element=(GDataXMLElement *) [phone1Arr objectAtIndex:0];
                    customer.Phone_1=[phone1Element.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *phone2Arr = [customerElement elementsForName:@"Phone2"];
                if (phone2Arr.count>0) {
                    GDataXMLElement *phone2Element=(GDataXMLElement *) [phone2Arr objectAtIndex:0];
                    customer.Phone_2=[phone2Element.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *emailArr = [customerElement elementsForName:@"Email"];
                if (emailArr.count>0) {
                    GDataXMLElement *emailElement=(GDataXMLElement *) [emailArr objectAtIndex:0];
                    customer.Email=[emailElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                [customers addObject:customer];
                [customer release];
            }
            message.customers=customers;
            [customers release];
        }
        [doc release];
    }
    return message;
}

-(XMLResponse*)translateMainItemDetails:(NSData *)xmlData
{
    NSError *error;
	GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    AllItemDetailsResponse *message=nil;
	if(doc)
	{
        NSArray *itemArr = [doc.rootElement elementsForName:@"MainItem"];
        if (itemArr.count>0) {
            message=[[[AllItemDetailsResponse alloc] init] autorelease];
            NSMutableArray *items=[[NSMutableArray alloc] initWithCapacity:itemArr.count];
            for(GDataXMLElement *itemElement in itemArr)
            {
                Item *item=nil;
                NSArray *categoryArr = [itemElement elementsForName:@"Category"];
                if (categoryArr.count>0) {
                    GDataXMLElement *categoryElement=(GDataXMLElement *) [categoryArr objectAtIndex:0];
                    NSString* categoryStr=[categoryElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    if([categoryStr isEqualToString:kBlindCategory] || [categoryStr isEqualToString:kblindACategoryA])
                    {
                        item=[[BlindItem alloc] init];
                    }
                    else
                    {
                        item=[[Item alloc] init];
                    }
                    item.Category=categoryStr;
                }
                NSArray *itemNoArr = [itemElement elementsForName:@"ItemNo"];
                if (itemNoArr.count>0) {
                    GDataXMLElement *itemNoElement=(GDataXMLElement *) [itemNoArr objectAtIndex:0];
                    item.Item_No=[itemNoElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *descriptionArr = [itemElement elementsForName:@"Description"];
                if (descriptionArr.count>0) {
                    GDataXMLElement *descriptionElement=(GDataXMLElement *) [descriptionArr objectAtIndex:0];
                    item.Description=[descriptionElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *stockUnitArr = [itemElement elementsForName:@"StockUnit"];
                if (stockUnitArr.count>0) {
                    GDataXMLElement *stockUnitElement=(GDataXMLElement *) [stockUnitArr objectAtIndex:0];
                    item.Stock_Unit=[stockUnitElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *imagePathArr = [itemElement elementsForName:@"ImagePath"];
                if (imagePathArr.count>0) {
                    GDataXMLElement *imagePathElement=(GDataXMLElement *) [imagePathArr objectAtIndex:0];
                    item.Image_Path=[imagePathElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    //To be a complete offline solution we need to load the images while sync is in progress.
                    /*AsyncImageView *asyncImageView=[[AsyncImageView alloc] init];
                     NSString *itemImageLocalPath=[item.Image_Path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                     NSString *itemImageURLPath=[NSString stringWithFormat:@"http://219.94.43.102/KDSCMS/images/products/%@",itemImageLocalPath];
                     [asyncImageView setDelegate:item];
                     [asyncImageView loadImageFromPath:itemImageURLPath];
                     [asyncImageView release];
                     NSLog(@"Image downloaded");*/
                }
                NSArray *UOMAndConversionArr = [itemElement elementsForName:@"UOMAndConversion"];
                if (UOMAndConversionArr.count>0) {
                    GDataXMLElement *UOMAndConversionElement=(GDataXMLElement *) [UOMAndConversionArr objectAtIndex:0];
                    NSArray *UOMAndConversionArrL2 = [UOMAndConversionElement elementsForName:@"UOMAndConversion"];
                    if(UOMAndConversionArrL2.count>0)
                    {
                        NSMutableArray *uoms=[[NSMutableArray alloc] initWithCapacity:UOMAndConversionArrL2.count];
                        for(GDataXMLElement *UOMAndConversionL2Element in UOMAndConversionArrL2)
                        {
                            UOMAndConversion *uom=[[UOMAndConversion alloc] init];
                            NSArray *UOMArr = [UOMAndConversionL2Element elementsForName:@"UOM"];
                            if (UOMArr.count>0) {
                                GDataXMLElement *UOMElement=(GDataXMLElement *) [UOMArr objectAtIndex:0];
                                uom.Uom=[UOMElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            }
                            NSArray *conversionArr = [UOMAndConversionL2Element elementsForName:@"Conversion"];
                            if (conversionArr.count>0) {
                                GDataXMLElement *conversionElement=(GDataXMLElement *) [conversionArr objectAtIndex:0];
                                uom.Converstion=[conversionElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            }
                            [uoms addObject:uom];
                            [uom release];
                        }
                        item.UOMAndConversion=uoms;
                        [uoms release];
                    }
                }
                NSArray *defaultPriceAndUOMArr = [itemElement elementsForName:@"DefaultPriceAndUOM"];
                if (defaultPriceAndUOMArr.count>0) {
                    GDataXMLElement *defaultPriceAndUOMElement=(GDataXMLElement *) [defaultPriceAndUOMArr objectAtIndex:0];
                    NSArray *defaultPriceAndUOMArrL2 = [defaultPriceAndUOMElement elementsForName:@"DefaultPriceAndUOM"];
                    if(defaultPriceAndUOMArrL2.count>0)
                    {
                        NSMutableArray *defaultPrices=[[NSMutableArray alloc] initWithCapacity:defaultPriceAndUOMArrL2.count];
                        for(GDataXMLElement *defaultPriceAndUOML2Element in defaultPriceAndUOMArrL2)
                        {
                            DefaultPriceAndUOM *defaultPrice=[[DefaultPriceAndUOM alloc] init];
                            NSArray *defaultPriceArr = [defaultPriceAndUOML2Element elementsForName:@"DefaultPrice"];
                            if (defaultPriceArr.count>0) {
                                GDataXMLElement *defaultPriceElement=(GDataXMLElement *) [defaultPriceArr objectAtIndex:0];
                                defaultPrice.Default_Price=[defaultPriceElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            }
                            NSArray *UOMArr = [defaultPriceAndUOML2Element elementsForName:@"UOM"];
                            if (UOMArr.count>0) {
                                GDataXMLElement *UOMElement=(GDataXMLElement *) [UOMArr objectAtIndex:0];
                                defaultPrice.Uom=[UOMElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            }
                            [defaultPrices addObject:defaultPrice];
                            [defaultPrice release];
                        }
                        item.Default_Price=defaultPrices;
                        [defaultPrices release];
                    }
                }
                if([item.Category isEqualToString:kBlindCategory] || [item.Category isEqualToString:kblindACategoryA])
                {
                    NSArray *minHeightArr = [itemElement elementsForName:@"MinHeight"];
                    if (minHeightArr.count>0) {
                        GDataXMLElement *minHeightElement=(GDataXMLElement *) [minHeightArr objectAtIndex:0];
                        ((BlindItem*)item).min_Height=[minHeightElement.stringValue floatValue];
                    }
                    NSArray *minSQFTArr = [itemElement elementsForName:@"MinSqFt"];
                    if (minSQFTArr.count>0) {
                        GDataXMLElement *minSQFTElement=(GDataXMLElement *) [minSQFTArr objectAtIndex:0];
                        ((BlindItem*)item).min_SqFt=[minSQFTElement.stringValue floatValue];
                    }
                }
                [items addObject:item];
                [item release];
            }
            message.items=items;
            [items release];
        }
        [doc release];
    }
    return message;
}

-(XMLResponse*)translateBlindItemDetails:(NSData *)xmlData
{
    NSError *error;
	GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];
    AllItemDetailsResponse *message=nil;
	if(doc)
	{
        NSArray *itemArr = [doc.rootElement elementsForName:@"BlindsItem"];
        if (itemArr.count>0) {
            message=[[[AllItemDetailsResponse alloc] init] autorelease];
            NSMutableArray *items=[[NSMutableArray alloc] initWithCapacity:itemArr.count];
            for(GDataXMLElement *itemElement in itemArr)
            {
                Item *item=nil;
                NSArray *categoryArr = [itemElement elementsForName:@"Category"];
                if (categoryArr.count>0) {
                    GDataXMLElement *categoryElement=(GDataXMLElement *) [categoryArr objectAtIndex:0];
                    NSString* categoryStr=[categoryElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    if([categoryStr isEqualToString:kBlindCategory] || [categoryStr isEqualToString:kblindACategoryA])
                    {
                        item=[[BlindItem alloc] init];
                    }
                    else
                    {
                        item=[[Item alloc] init];
                    }
                    item.Category=categoryStr;
                }
                NSArray *itemNoArr = [itemElement elementsForName:@"ItemNo"];
                if (itemNoArr.count>0) {
                    GDataXMLElement *itemNoElement=(GDataXMLElement *) [itemNoArr objectAtIndex:0];
                    item.Item_No=[itemNoElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *descriptionArr = [itemElement elementsForName:@"Description"];
                if (descriptionArr.count>0) {
                    GDataXMLElement *descriptionElement=(GDataXMLElement *) [descriptionArr objectAtIndex:0];
                    item.Description=[descriptionElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *stockUnitArr = [itemElement elementsForName:@"StockUnit"];
                if (stockUnitArr.count>0) {
                    GDataXMLElement *stockUnitElement=(GDataXMLElement *) [stockUnitArr objectAtIndex:0];
                    item.Stock_Unit=[stockUnitElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                }
                NSArray *imagePathArr = [itemElement elementsForName:@"ImagePath"];
                if (imagePathArr.count>0) {
                    GDataXMLElement *imagePathElement=(GDataXMLElement *) [imagePathArr objectAtIndex:0];
                    item.Image_Path=[imagePathElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    //To be a complete offline solution we need to load the images while sync is in progress.
                    /*AsyncImageView *asyncImageView=[[AsyncImageView alloc] init];
                     NSString *itemImageLocalPath=[item.Image_Path stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
                     NSString *itemImageURLPath=[NSString stringWithFormat:@"http://219.94.43.102/KDSCMS/images/products/%@",itemImageLocalPath];
                     [asyncImageView setDelegate:item];
                     [asyncImageView loadImageFromPath:itemImageURLPath];
                     [asyncImageView release];
                     NSLog(@"Image downloaded");*/
                }
                NSArray *UOMAndConversionArr = [itemElement elementsForName:@"UOMAndConversion"];
                if (UOMAndConversionArr.count>0) {
                    GDataXMLElement *UOMAndConversionElement=(GDataXMLElement *) [UOMAndConversionArr objectAtIndex:0];
                    NSArray *UOMAndConversionArrL2 = [UOMAndConversionElement elementsForName:@"UOMAndConversion"];
                    if(UOMAndConversionArrL2.count>0)
                    {
                        NSMutableArray *uoms=[[NSMutableArray alloc] initWithCapacity:UOMAndConversionArrL2.count];
                        for(GDataXMLElement *UOMAndConversionL2Element in UOMAndConversionArrL2)
                        {
                            UOMAndConversion *uom=[[UOMAndConversion alloc] init];
                            NSArray *UOMArr = [UOMAndConversionL2Element elementsForName:@"UOM"];
                            if (UOMArr.count>0) {
                                GDataXMLElement *UOMElement=(GDataXMLElement *) [UOMArr objectAtIndex:0];
                                uom.Uom=[UOMElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            }
                            NSArray *conversionArr = [UOMAndConversionL2Element elementsForName:@"Conversion"];
                            if (conversionArr.count>0) {
                                GDataXMLElement *conversionElement=(GDataXMLElement *) [conversionArr objectAtIndex:0];
                                uom.Converstion=[conversionElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            }
                            [uoms addObject:uom];
                            [uom release];
                        }
                        item.UOMAndConversion=uoms;
                        [uoms release];
                    }
                }
                NSArray *defaultPriceAndUOMArr = [itemElement elementsForName:@"DefaultPriceAndUOM"];
                if (defaultPriceAndUOMArr.count>0) {
                    GDataXMLElement *defaultPriceAndUOMElement=(GDataXMLElement *) [defaultPriceAndUOMArr objectAtIndex:0];
                    NSArray *defaultPriceAndUOMArrL2 = [defaultPriceAndUOMElement elementsForName:@"DefaultPriceAndUOM"];
                    if(defaultPriceAndUOMArrL2.count>0)
                    {
                        NSMutableArray *defaultPrices=[[NSMutableArray alloc] initWithCapacity:defaultPriceAndUOMArrL2.count];
                        for(GDataXMLElement *defaultPriceAndUOML2Element in defaultPriceAndUOMArrL2)
                        {
                            DefaultPriceAndUOM *defaultPrice=[[DefaultPriceAndUOM alloc] init];
                            NSArray *defaultPriceArr = [defaultPriceAndUOML2Element elementsForName:@"DefaultPrice"];
                            if (defaultPriceArr.count>0) {
                                GDataXMLElement *defaultPriceElement=(GDataXMLElement *) [defaultPriceArr objectAtIndex:0];
                                defaultPrice.Default_Price=[defaultPriceElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            }
                            NSArray *UOMArr = [defaultPriceAndUOML2Element elementsForName:@"UOM"];
                            if (UOMArr.count>0) {
                                GDataXMLElement *UOMElement=(GDataXMLElement *) [UOMArr objectAtIndex:0];
                                defaultPrice.Uom=[UOMElement.stringValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                            }
                            [defaultPrices addObject:defaultPrice];
                            [defaultPrice release];
                        }
                        item.Default_Price=defaultPrices;
                        [defaultPrices release];
                    }
                }
                if([item.Category isEqualToString:kBlindCategory])
                {
                    NSArray *minHeightArr = [itemElement elementsForName:@"MinHeight"];
                    if (minHeightArr.count>0) {
                        GDataXMLElement *minHeightElement=(GDataXMLElement *) [minHeightArr objectAtIndex:0];
                        ((BlindItem*)item).min_Height=[minHeightElement.stringValue floatValue];
                    }
                    NSArray *minSQFTArr = [itemElement elementsForName:@"MinSqFt"];
                    if (minSQFTArr.count>0) {
                        GDataXMLElement *minSQFTElement=(GDataXMLElement *) [minSQFTArr objectAtIndex:0];
                        ((BlindItem*)item).min_SqFt=[minSQFTElement.stringValue floatValue];
                    }
                }
                [items addObject:item];
                [item release];
            }
            message.items=items;
            [items release];
        }
        [doc release];
    }
    return message;
}
@end
