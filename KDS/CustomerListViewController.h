//
//  CustomerListViewController.h
//  KDS
//
//  Created by Tiseno on 12/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewOrderViewController.h"
#import "NSDictionary-MutableDeepCopy.h"
#import "CompanyTableCellViewController.h"
#import "OrderHistoryTableViewController.h"
#import "OrderHistoryTableGeneratorDelegate.h"
#import "NewOrderCategoryViewController.h"
#import "KDSDataCustomer.h"
#import "Customer.h"
#import "KDSAppDelegate.h"
#import "KDSDataListGallery.h"
#import "listgallery.h"

@interface CustomerListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, OrderHistoryTableGeneratorDelegate>{
    
    UILabel *companyNameLabel;
    UILabel *picLabel;
    UILabel *contactNoLabel;
    UILabel *telLabel;
    UILabel *emailLabel;
    
    UIView *companyTableViewHeader;
    UIView *orderHistoryTableViewHeader;
    UIView *customerInfoView;
    
    UITextView *addressTextView;
    UITableView *companyTableView;
    UITableView *orderHistoryTableView;
    
    UISearchBar *search;
    
    Customer *currentSelectedCell;
    
    NSArray *customerArray;
    NSMutableArray *customerArray_Copied;

    OrderHistoryTableViewController *orderHistoryTableViewController;
}
@property (nonatomic, retain) NSString *lstGallery;
@property (nonatomic) int *listGallery;
@property (nonatomic, retain) IBOutlet UITableView *companyTableView;
@property (nonatomic, retain) IBOutlet UITableView *orderHistoryTableView;
@property (nonatomic, retain) IBOutlet UISearchBar *search;
@property (nonatomic, retain) NSArray *customerArray;
@property (nonatomic, retain) NSMutableArray *customerArray_Copied;
@property (nonatomic, retain) IBOutlet UILabel *companyNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *picLabel;
@property (nonatomic, retain) IBOutlet UILabel *contactNoLabel;
@property (nonatomic, retain) IBOutlet UILabel *telLabel;
@property (nonatomic, retain) IBOutlet UILabel *emailLabel;
@property (nonatomic, retain) UIView *companyTableViewHeader;
@property (nonatomic, retain) UIView *orderHistoryTableViewHeader;
@property (nonatomic, retain) IBOutlet UIView *customerInfoView;;
@property (nonatomic, retain) IBOutlet UITextView *addressTextView;
@property (nonatomic, retain) OrderHistoryTableViewController *orderHistoryTableViewController;
@property (nonatomic, retain) Customer *currentSelectedCell;
-(void)generateOderHistoryTable:(id)sender;
-(void)listView;
-(void)galleryView;
@end
