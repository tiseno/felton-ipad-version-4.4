//
//  DownloadItemImagesReaponse.h
//  KDS
//
//  Created by Tiseno Mac 2 on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XMLResponse.h"

@interface DownloadItemImagesReaponse :  XMLResponse{
    NSString *getImagepath;
    NSArray *ImagePathArr;
}
@property (nonatomic,retain) NSString *getImagepath;
@property (nonatomic, retain) NSArray *ImagePathArr;
@end
