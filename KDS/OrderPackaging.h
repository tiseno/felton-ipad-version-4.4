//
//  OrderPackaging.h
//  KDS
//
//  Created by Tiseno Mac 2 on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrderPackaging : NSObject{
    
    NSMutableArray* _Order_Package;
    
}
@property (nonatomic, retain) NSArray *orderPackagearr;
@property (nonatomic, retain) NSString *orderID;
@property (nonatomic, retain) NSString *packageID;
//@property (nonatomic, retain) NSMutableArray* _Order_Package;
-(NSMutableArray*) Order_Package;
-(void) addOrderPackage:(NSArray*)orderItem;

@end
