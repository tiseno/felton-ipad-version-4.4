//
//  ButtonWithProduct.h
//  KDS
//
//  Created by Tiseno on 12/8/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Item.h"
#import "UIButtonWithImageFromURL.h"

@interface ButtonWithProduct : UIButtonWithImageFromURL {
    Item* item;
}
@property (nonatomic, retain) Item* item;
@end
