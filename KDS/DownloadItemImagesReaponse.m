//
//  DownloadItemImagesReaponse.m
//  KDS
//
//  Created by Tiseno Mac 2 on 4/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DownloadItemImagesReaponse.h"

@implementation DownloadItemImagesReaponse
@synthesize getImagepath,ImagePathArr;

-(void)dealloc
{
    [ImagePathArr release];
    [getImagepath release];
    [super dealloc];
}

@end
