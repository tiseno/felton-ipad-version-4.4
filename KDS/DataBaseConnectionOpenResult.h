//
//  DataBaseConncetionOpenResult.h
//  Holiday Villa
//
//  Created by Jermin Bazazian on 4/27/11.
//  Copyright 2011 tiseno integrated solutions sdn bhd. All rights reserved.
//
typedef enum
{
    DataBaseConnectionOpened,
    DataBaseConnectionFailed
} DataBaseConncetionOpenResult;

typedef enum 
{
    DataBaseInsertionSuccessful,
    DataBaseInsertionFailed
} DataBaseInsertionResult;
typedef enum 
{
    DataBaseUpdateSuccessful,
    DataBaseUpdateFailed
} DataBaseUpdateResult;
typedef enum 
{
    DataBaseDeletionSuccessful,
    DataBaseDeletionFailed
} DataBaseDeletionResult;