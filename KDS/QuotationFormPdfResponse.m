//
//  QuotationFormPdfResponse.m
//  KDS
//
//  Created by Tiseno Mac 2 on 10/17/12.
//
//

#import "QuotationFormPdfResponse.h"

@implementation QuotationFormPdfResponse
@synthesize getPDFPath;

-(void)dealloc
{
    [getPDFPath release];
    [super dealloc];
}

@end
