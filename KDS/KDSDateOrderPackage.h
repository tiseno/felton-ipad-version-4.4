//
//  KDSDateOrderPackage.h
//  KDS
//
//  Created by Tiseno Mac 2 on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KDSDataLayerBase.h"
#import "OrderPackaging.h"

@interface KDSDateOrderPackage : KDSDataLayerBase{
    BOOL runBatch;
}

@property (nonatomic)BOOL runBatch;
-(DataBaseInsertionResult)insertPackage:(OrderPackaging*)tPackage;
-(DataBaseInsertionResult)insertOrderPackageArray:(NSArray*)iorderpackage;
-(DataBaseDeletionResult)deletOrderpackage;

-(NSArray*)selectOrderPackageID;

@end
