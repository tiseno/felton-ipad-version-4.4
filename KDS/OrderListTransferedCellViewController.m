//
//  OrderListTransferedCellViewController.m
//  KDS
//
//  Created by Tiseno on 11/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "OrderListTransferedCellViewController.h"
#define cell1Width 653
#define cell2Width 191
#define cellHeight 45

@implementation OrderListTransferedCellViewController

@synthesize topCell, lineColor, idLabel, customerNameLabel, priceLabel,editButton,salesOrder,delegate;

- (void)dealloc
{
    [delegate release];
    [super dealloc];
    [salesOrder release];
    [editButton release];
    [lineColor release];
    [idLabel release];
    [customerNameLabel release];
    [priceLabel release];
} 
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) 
    {
        topCell = NO;
        
        //self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 846, 183);
        
        

    //self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    //if (self) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 844, 47);
        
        UIImage *editButtonImage = [UIImage imageNamed:@"btn_light_blue.png"];
        UIButton *teditButton = [[UIButton alloc] initWithFrame:CGRectMake(20, (cellHeight/2)-(editButtonImage.size.height/2), 60, 30)];
        
        [teditButton setTitle:@"Transfer"  forState:UIControlStateNormal ];
        teditButton.titleLabel.font = [UIFont systemFontOfSize:13];
        
        [teditButton setBackgroundImage:editButtonImage forState:UIControlStateNormal];
        [teditButton addTarget:self action:@selector(transfer_ButtonTapped:)
              forControlEvents:UIControlEventTouchDown];
        self.editButton = teditButton;
        [teditButton release];
        [self addSubview:editButton];
        
        UILabel *tCustomerNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 0, cell1Width, 40)];
        tCustomerNameLabel.textAlignment = UITextAlignmentLeft;
        tCustomerNameLabel.textColor = [UIColor blackColor];
        tCustomerNameLabel.backgroundColor = [UIColor clearColor];
        self.customerNameLabel = tCustomerNameLabel;
        [tCustomerNameLabel release];
        [self addSubview:customerNameLabel];
        
        UILabel *tPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(cell1Width, 0, cell2Width, 45)];
        tPriceLabel.textAlignment = UITextAlignmentLeft;
        tPriceLabel.textColor = [UIColor blackColor];
        tPriceLabel.backgroundColor = [UIColor clearColor];
        self.priceLabel = tPriceLabel;
        [tPriceLabel release];
        [self addSubview:priceLabel];
                            
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(context, lineColor.CGColor);       
    
	// CGContextSetLineWidth: The default line width is 1 unit. When stroked, the line straddles the path, with half of the total width on either side.
	// Therefore, a 1 pixel vertical line will not draw crisply unless it is offest by 0.5. This problem does not seem to affect horizontal lines.
	CGContextSetLineWidth(context, 1.0);
    
	/*// Add the vertical lines
	CGContextMoveToPoint(context, cell1Width, 0);
	CGContextAddLineToPoint(context, cell1Width, rect.size.height);
    
	CGContextMoveToPoint(context, cell1Width+cell2Width+0.3, 0);
	CGContextAddLineToPoint(context, cell1Width+cell2Width+0.3, rect.size.height);*/
    
	// Add bottom line
	//CGContextMoveToPoint(context, 0, rect.size.height);
	//CGContextAddLineToPoint(context, rect.size.width, rect.size.height-0.5);
	
	// If this is the topmost cell in the table, draw the line on top
	
	// Draw the lines
	CGContextStrokePath(context); 
}

-(IBAction)transfer_ButtonTapped:(id)sender
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Transfer" message:@"Are you sure?" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag=kConfirmingAlertTag;
    [alert show];
    [alert release];
    
    
    
    
    
        
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == kConfirmingAlertTag)
    {
        if(buttonIndex == 1)
        {
            
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Transfer" message:@"The order has been transferred." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=kConfirmedAlertTag;
            [alert show];
            [alert release];*/
            
            
            KDSDataSalesOrderItem* dataSalesOrderItem=[[KDSDataSalesOrderItem alloc] init];
            KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
            NSArray* orderItemArr=[dataSalesOrderItem selectOrderItemsForOrder:self.salesOrder];
            
            /*[dataSalesOrderItem release]; 
             
             if([self.salesOrder NumberOfOderItems]==0)
             {
             for(SalesOrderItem* orderItem in orderItemArr)
             {
             [self.salesOrder addOrderItem:orderItem];
             }
             }
             
             //NSLog(@"pdf tapped!!");
             
             appDelegate.salesOrder=self.salesOrder;
             appDelegate.currentCustomer=self.salesOrder.customer;
             
             NSLog(@"%d", self.salesOrder.Order_id);
             NSLog(@"%@", self.salesOrder.OrderDescription);*/
            
            
            
            
            
            
            
            //KDSDataSalesOrderItem* dataSalesOrderItem=[[KDSDataSalesOrderItem alloc] init];
            //KDSAppDelegate* appdelegate=[UIApplication sharedApplication].delegate;
            
            for (SalesOrder* salesOrder in orderItemArr) 
            { 
                NSArray* orderItemArr=[dataSalesOrderItem selectOrderItemsForOrder:self.salesOrder];
                if([self.salesOrder NumberOfOderItems]==0)
                {
                    for(SalesOrderItem* orderItem in orderItemArr)
                    {
                        [self.salesOrder addOrderItem:orderItem];
                    }
                } 
            }
            [dataSalesOrderItem release];
            
            
             
            
            [self.delegate TransferSaleOrderButton:salesOrder SalesPersonCode:appDelegate.loggedInSalesPerson ];
            
        }
    }
    
}

-(void)setTopCell:(BOOL)newTopCell
{
    topCell = newTopCell;
}


@end
