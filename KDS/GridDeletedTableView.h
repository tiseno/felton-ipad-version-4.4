//
//  GridDeletedTableView.h
//  KDS
//
//  Created by Tiseno Mac 2 on 5/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChangeDiscountTypeButtonDelegate.h"
#import "YourCartTableCellDelegate.h"
#import "KDSAppDelegate.h"
#import "UIImageViewWithImageFromURL.h"
#import "SalesOrderItem.h"
#import "AsyncImageView.h"

@interface GridDeletedTableView : UITableViewCell<UIAlertViewDelegate,UITextFieldDelegate,UITextViewDelegate> {
    
    UIColor *lineColor;
    BOOL topCell;
    
    UIButton *deleteButton;
    UIButton *changeDiscountTypeButton;
    
    UITextField *quantityTextField;
    
    UITextView *descriptionTextView;
    UITextField *priceTextField;
    UITextField *discountTextField;
    
    UIImageViewWithImageFromURL *productImageView;
    
    UILabel *quantityLabel;
    UILabel *productNameLabel;
    UILabel *descriptionLabel;
    UILabel *priceLabel;
    UILabel *discountLabel;
    UILabel *TotalLabel;
    UILabel *discountTypeLabel;
    BOOL isDiscountAmount;
    int equivalantOrderItemIndex;
    id<YourCartTableCellDelegate> delegate;
    SalesOrderItem* salesOrderItem;
    BlindSalesOrderItem* blindOrderItem;
}
@property (nonatomic, retain) UIColor *lineColor;

@property (nonatomic) BOOL topCell;

@property (nonatomic, retain) UIButton *deleteButton;
@property (nonatomic, retain) UIButton *changeDiscountTypeButton;

@property (nonatomic, retain) UITextField *quantityTextField;
@property (nonatomic, retain) UITextField *priceTextField;
@property (nonatomic, retain) UITextField *discountTextField;
@property (nonatomic, retain) UITextView *descriptionTextView;

@property (nonatomic, retain) UIImageViewWithImageFromURL *productImageView;

@property (nonatomic, retain) UILabel *quantityLabel;
@property (nonatomic, retain) UILabel *productNameLabel;
@property (nonatomic, retain) UILabel *descriptionLabel;
@property (nonatomic, retain) UILabel *priceLabel;
@property (nonatomic, retain) UILabel *discountLabel;
@property (nonatomic, retain) UILabel *TotalLabel;
@property (nonatomic, retain) UILabel *discountTypeLabel;
@property (nonatomic) BOOL isDiscountAmount;
@property (nonatomic) int equivalantOrderItemIndex;
@property (nonatomic, retain) id<YourCartTableCellDelegate> delegate;
@property (nonatomic, retain) SalesOrderItem* salesOrderItem;
@property (nonatomic, retain) BlindSalesOrderItem* blindOrderItem;

@property (nonatomic, retain) UILabel *productNameLabelmain;
@property (nonatomic, retain) UILabel *remarkplastic;

/*=============blind=============*/
@property (nonatomic, retain) UILabel *quantityLabelblind;
@property (nonatomic, retain) UILabel *productNameLabelblind;
@property (nonatomic, retain) UILabel *descriptionLabelblind;
@property (nonatomic, retain) UILabel *priceLabelblind;
@property (nonatomic, retain) UILabel *discountLabelblind;
@property (nonatomic, retain) UILabel *TotalLabelblind;
@property (nonatomic, retain) UILabel *discountTypeLabelblind;

@property (nonatomic, retain) UILabel *controlblind;
@property (nonatomic, retain) UILabel *colorblind;
@property (nonatomic, retain) UILabel *sqftblind;
@property (nonatomic, retain) UILabel *totalsqftblind;
@property (nonatomic, retain) UILabel *remarkblind;

@end
