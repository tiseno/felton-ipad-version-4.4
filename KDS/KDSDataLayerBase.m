//
//  KDSDataLayerBase.m
//  KDS
//
//  Created by Tiseno on 12/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "KDSDataLayerBase.h"

@implementation KDSDataLayerBase

-(id)init
{
    self=[super init];
    if(self)
    {
        databaseName=@"KDS_DB";
    }
    return self;
}
-(id)initWithDataBaseName:(NSString*)idataBaseName
{
    self=[super init];
    if(self)
    {
        databaseName=idataBaseName;
    }
    return self;
}
-(void) deallocx
{
    [super dealloc];
}
-(DataBaseConncetionOpenResult)openConnection
{
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.sqlite",databaseName]];
    //[fileManager removeItemAtPath:writableDBPath error:&error];
    success = [fileManager fileExistsAtPath:writableDBPath];
    if(!success)
    {
        NSString *path=[[NSBundle mainBundle]pathForResource:databaseName ofType:@"sqlite"];
        //[
        success = [fileManager copyItemAtPath:path
                                       toPath:writableDBPath
                                        error:&error];
        if(!success)
        {
            NSAssert1(0,@"Failed to create writable database file with Message : '%@'.",
                      [error localizedDescription]);
        }
    }
    const char* dbPath=[writableDBPath UTF8String];
    if(sqlite3_open(dbPath, &dbKDS)==SQLITE_OK)
    {
        return DataBaseConnectionOpened;
    }
    else
    {
        sqlite3_close(dbKDS);
        NSAssert1(0, @"Failed to open the database with message '%s'.", sqlite3_errmsg(dbKDS));
    }
    return DataBaseConnectionFailed;
}
-(void)closeConnection
{
    sqlite3_close(dbKDS);
}

@end
