//
//  CompanyTableCellViewController.m
//  KDS
//
//  Created by Tiseno on 12/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "CompanyTableCellViewController.h"
//#define cellWidth 456
#define cellHeight 45
#define cell1Width 100



@implementation CompanyTableCellViewController

@synthesize companyNameLabel, picLabel, orderHistoryButton, orderHistoryGeneratorDelegate, customer,lineColor,topCell, companyNameIDLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        topCell = NO;
        
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 456, 45);
        
        UILabel *tCompanyNameIDLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20, 0, 150, 45)] autorelease];
        tCompanyNameIDLabel.textAlignment = UITextAlignmentLeft;
        tCompanyNameIDLabel.adjustsFontSizeToFitWidth = YES;
        tCompanyNameIDLabel.minimumFontSize=10;
        tCompanyNameIDLabel.backgroundColor = [UIColor clearColor];
        tCompanyNameIDLabel.textColor = [UIColor blackColor];
        tCompanyNameIDLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        self.companyNameIDLabel = tCompanyNameIDLabel; 
        [self addSubview:companyNameIDLabel];

        
        UILabel *tCompanyNameLabel = [[[UILabel alloc] initWithFrame:CGRectMake(90, 0, 350, 45)] autorelease];
        tCompanyNameLabel.textAlignment = UITextAlignmentLeft;
        tCompanyNameLabel.adjustsFontSizeToFitWidth = YES;
        tCompanyNameLabel.minimumFontSize=10;
        tCompanyNameLabel.backgroundColor = [UIColor clearColor];
        tCompanyNameLabel.textColor = [UIColor blackColor];
        tCompanyNameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        self.companyNameLabel = tCompanyNameLabel;
        [self addSubview:companyNameLabel];
        
        /*UILabel *tPicLabel = [[UILabel alloc] initWithFrame:CGRectMake((cellWidth/2)+10, 0, cellWidth/2+5, cellHeight)];
        tPicLabel.textAlignment = UITextAlignmentLeft;
        tPicLabel.adjustsFontSizeToFitWidth = YES;
        tPicLabel.backgroundColor = [UIColor clearColor];
        tPicLabel.textColor = [UIColor blackColor];
        tPicLabel.font = [UIFont fontWithName:@"Helvetica" size:13];
        self.picLabel = tPicLabel;
        [tPicLabel release];
        [self addSubview:picLabel];*/
        
        UIImage *orderHistoryButtonImage = [UIImage imageNamed:@"btn_order_history.png"];
        UIButton *tOrderHistoryButton = [[UIButton alloc] initWithFrame:CGRectMake(375, 0, 76, 42)];
        [tOrderHistoryButton setBackgroundImage:orderHistoryButtonImage forState:UIControlStateNormal];
        [tOrderHistoryButton addTarget:self action:@selector(orderHistory_ButtonTapped:)
             forControlEvents:UIControlEventTouchDown];
        self.orderHistoryButton = tOrderHistoryButton;
        [tOrderHistoryButton release];
        [self addSubview:orderHistoryButton];
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    //[super setSelected:selected animated:animated];
    if(selected)
    {
        self.contentView.backgroundColor = [UIColor lightGrayColor];
        self.orderHistoryButton.hidden = NO;
        
    }else
    {
        [[self contentView] setBackgroundColor:[UIColor whiteColor]];
        self.orderHistoryButton.hidden = YES;   
    }
    
}

-(void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(context, lineColor.CGColor);       
    
	// CGContextSetLineWidth: The default line width is 1 unit. When stroked, the line straddles the path, with half of the total width on either side.
	// Therefore, a 1 pixel vertical line will not draw crisply unless it is offest by 0.5. This problem does not seem to affect horizontal lines.
	CGContextSetLineWidth(context, 1.0);
    


    CGContextMoveToPoint(context, cell1Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+0.5, rect.size.height);

        
    
	// Add bottom line
	CGContextMoveToPoint(context, 0, rect.size.height);
	CGContextAddLineToPoint(context, rect.size.width, rect.size.height-0.5);
	
	// If this is the topmost cell in the table, draw the line on top
	if (topCell)
	{
		CGContextMoveToPoint(context, 0, 0);
		CGContextAddLineToPoint(context, rect.size.width, 0);
	}
	
	
	// Draw the lines
	CGContextStrokePath(context); 
}

-(void)setTopCell:(BOOL)newTopCell
{
    topCell = newTopCell;
}

-(IBAction)orderHistory_ButtonTapped:(id)sender
{
    [orderHistoryGeneratorDelegate generateOderHistoryTable:sender];
    NSLog(@"btnhistory");
}

-(void)generateOderHistoryTable:(id)sender
{
}
- (void)dealloc
{
    [companyNameLabel release];
    [picLabel release];
    [orderHistoryButton release];
    [orderHistoryGeneratorDelegate release];
    [customer release];
    [lineColor release];
    [companyNameIDLabel release];
    [super dealloc];
}

@end
