//
//  KDSSuperViewController.h
//  KDS
//
//  Created by Tiseno on 12/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol KDSSuperViewController <NSObject>
-(void)loadLogin;
-(void)loadMenu;
-(void)loadMapIP;
@end
