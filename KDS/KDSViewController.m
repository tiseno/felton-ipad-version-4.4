//
//  KDSViewController.m
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "KDSViewController.h"

@implementation KDSViewController
@synthesize logingInViewController,menuViewController,navController;
@synthesize newOrderCatViewCtrr;
- (void)dealloc
{
    [newOrderCatViewCtrr release];
    [logingInViewController release];
    [menuViewController release];
    [navController release];
    [super dealloc];
    
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self loadLogin];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    if (interfaceOrientation==UIInterfaceOrientationLandscapeLeft || interfaceOrientation==UIInterfaceOrientationLandscapeRight) {
        return YES;
    }
    return NO;
}
-(void)loadLogin
{
    for(UIView* uiview in self.view.subviews)
    {
        [uiview removeFromSuperview];
    }
    LoginViewController *tloginViewController=[[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    tloginViewController.superViewController=self;
    tloginViewController.view.frame=CGRectMake(0, 0, 1024, 748);
    self.logingInViewController=tloginViewController;
    [tloginViewController release];
    [self.view addSubview:self.logingInViewController.view];
} 
-(void)loadMenu
{
    for(UIView* uiview in self.view.subviews)
    {
        [uiview removeFromSuperview];
    }
    MenuViewController *tmenuViewController=[[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
    tmenuViewController.superViewController=self;
    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:tmenuViewController];
    tnavController.view.frame=CGRectMake(0, 0, 1024, 748);
    tnavController.navigationBar.tintColor=[UIColor blackColor];
    self.menuViewController=tmenuViewController;
    self.navController=tnavController;
    [tmenuViewController release];
    [tnavController release];
    
    [self.view addSubview:self.navController.view];
}

-(void)loadMapIP
{

    
    MapIPViewController *gMapIPViewController=[[MapIPViewController alloc]initWithNibName:@"MapIPViewController" bundle:nil];
    
    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:gMapIPViewController];
    tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
    
    tnavController.navigationBar.tintColor=[UIColor blackColor];
    //gMapIPViewController.navigationItem.titleView = label;
    UIImage *imagetopbar = [UIImage imageNamed:@"black_bar.png"];
    [tnavController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
    
    [self presentModalViewController:tnavController animated:YES];
}

/*-(void)loadProductCatalog
{
    for(UIView* uiview in self.view.subviews)
    {
        [uiview removeFromSuperview];
    }
    NewOrderCategoryViewController *tCtrViewController=[[NewOrderCategoryViewController alloc] initWithNibName:@"NewOrderCategoryViewController" bundle:nil];
    tCtrViewController.superViewController=self;
    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:tCtrViewController];
    tnavController.view.frame=CGRectMake(0, 0, 1024, 748);
    tnavController.navigationBar.tintColor=[UIColor blackColor];
    self.newOrderCatViewCtrr=tCtrViewController;
    self.navController=tnavController;
    [tCtrViewController release];
    [tnavController release];
    
    [self.view addSubview:self.navController.view];
}*/
@end
