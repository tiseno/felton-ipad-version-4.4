//
//  ProductTableCell.h
//  KDS
//
//  Created by Tiseno on 1/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Item.h"


@interface ProductTableCell : UITableViewCell {
    
    UILabel *productIdLabel;
    UILabel *productNameLabel;
    UILabel *priceLabel;
    UIColor *lineColor;
    Item *itemm;
}
@property (nonatomic, retain) Item *itemm;
@property (nonatomic, retain) UIColor *lineColor;
@property (nonatomic,retain) UILabel *productNameLabel;
@property (nonatomic, retain) UILabel *priceLabel;
@property (nonatomic, retain) UILabel *productIdLabel;
@end