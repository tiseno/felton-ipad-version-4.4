//
//  OrdereditViewController.m
//  KDS
//
//  Created by Tiseno Mac 2 on 6/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OrdereditViewController.h"



@implementation OrdereditViewController
@synthesize newOrderTabelView,newOrderTableViewHeader,editOrderTableController;
@synthesize salesOrder,salesOrderArr,loadingView;
@synthesize selectedOrder;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self initializeTableData];
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(editOrderTableController == nil)
    {
        NewOrderTableViewController *tNewOrderTableViewController = [[NewOrderTableViewController alloc] init];
        self.editOrderTableController = tNewOrderTableViewController;
        self.editOrderTableController.ordereditViewController=self;
        [tNewOrderTableViewController release];
    } 
    
    
    [newOrderTabelView setDataSource:self.editOrderTableController];
    [newOrderTabelView setDelegate:self.editOrderTableController];
    
        
    self.editOrderTableController.view = self.editOrderTableController.tableView;
    
    
    UIView *NewOrdeertableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(newOrderTabelView.frame.origin.x, newOrderTabelView.frame.origin.y-30, self.newOrderTabelView.frame.size.width, 30)];
    NewOrdeertableHeaderView.backgroundColor = [UIColor colorWithRed:0.0196 green:0.513 blue:0.949 alpha:1.0];
    
    UILabel *NewOrderTableHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(13, 5, self.newOrderTabelView.frame.size.width/2, 21)];
    NewOrderTableHeaderLabel.text = @"New Order";
    NewOrderTableHeaderLabel.backgroundColor = [UIColor clearColor];
    NewOrderTableHeaderLabel.textColor = [UIColor whiteColor];
    NewOrderTableHeaderLabel.textAlignment = UITextAlignmentLeft;
    NewOrderTableHeaderLabel.font=[UIFont boldSystemFontOfSize:17];
    [NewOrdeertableHeaderView addSubview:NewOrderTableHeaderLabel];
    newOrderTableViewHeader = NewOrdeertableHeaderView;
    [self.view addSubview:newOrderTableViewHeader];
    
    
    [NewOrdeertableHeaderView release];
    [NewOrderTableHeaderLabel release];
 
}

- (void)viewDidUnload
{
    [self setNewOrderTabelView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

- (void)dealloc 
{ 
    [salesOrderArr release];
    [salesOrder release];

    [selectedOrder release];
    [newOrderTabelView release]; 
    [newOrderTableViewHeader release];
    [editOrderTableController release];
    [super dealloc];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //[[self tblSwitchMainPage] reloadData];
    
    //[gtblSwitchMainPageViewController initwithCase];
    [self initializeTableData];
    
    [self.newOrderTabelView reloadData];
}

-(void)editSaleOrderForEditButton 
{
    YourCartViewController *yourCartViewController=[[[YourCartViewController alloc] initWithNibName:@"YourCartViewController" bundle:nil] autorelease];
    yourCartViewController.title=@"Felton";
    yourCartViewController.EditTag=@"edittag";
    
    
//    UINavigationController *tnavController=[[UINavigationController alloc] initWithRootViewController:yourCartViewController];
//    tnavController.view.frame=CGRectMake(0, 0, self.view.frame.size.width , self.view.frame.size.height);
//    
//    tnavController.navigationBar.tintColor=[UIColor blackColor];
//    //gMapIPViewController.navigationItem.titleView = label;
//    UIImage *imagetopbar = [UIImage imageNamed:@"black_bar.png"];
//    [tnavController.navigationBar setBackgroundImage:imagetopbar forBarMetrics:UIBarMetricsDefault];
//    
//    [self presentModalViewController:tnavController animated:YES];
    
    [self.navigationController pushViewController:yourCartViewController animated:YES];
}

-(BOOL)NetworkStatus
{
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return !(networkStatus==NotReachable);
}

-(void)editSaleOrderPDFButton:(SalesOrder *)isaleOrder SalesPersonCode:(SalesPerson *)isalesPersonCode
//-(void)editSaleOrderPDFButton:(SalesOrder*)isaleOrder SalesPersonCode:(NSString*)isalesPersonCode
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }else
    {
        //-(id)initWithSalesOrder:(SalesOrder*)isaleOrder SalesPersonCode:(NSString*)isalesPersonCode;
        
        
        
        //NSLog(@"isaleOrder.Customer_Name--->%@",isaleOrder.customer.Customer_Name);
        selectedOrder=isaleOrder;
        
        
        /**/TransferOrderPdfRequest* request=[[TransferOrderPdfRequest alloc] initWithSalesOrder:isaleOrder SalesPersonTransfer:isalesPersonCode];
        NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
        [networkHandler setDelegate:self];
        [networkHandler request:request];
        [request release];
        [networkHandler release];
        
        UIView *selfView=self.view;
        LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
        self.loadingView=temploadingView;
    } 
}

-(void)editSaleOrderPDFQuoButton:(SalesOrder *)isaleOrder SalesPersonCode:(SalesPerson *)isalesPersonCode
{
    if(![self NetworkStatus])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"No Wifi Connection is available. Please connect to a Wifi and try again." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        [alert release];
    }else
    {
        //-(id)initWithSalesOrder:(SalesOrder*)isaleOrder SalesPersonCode:(NSString*)isalesPersonCode;
        
        //NSLog(@"isaleOrder.Customer_Name--->%@",isaleOrder.customer.Customer_Name);
        selectedOrder=isaleOrder;
        
        
        /**/QuotationFormPdfRequest* request=[[QuotationFormPdfRequest alloc] initWithSalesOrder:isaleOrder SalesPersonTransfer:isalesPersonCode];
        NetworkHandler *networkHandler=[[NetworkHandler alloc] init];
        [networkHandler setDelegate:self];
        [networkHandler request:request];
        [request release];
        [networkHandler release];
        
        UIView *selfView=self.view;
        LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
        self.loadingView=temploadingView;
    } 
}


-(void)initializeTableData
{
    KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    KDSDataSalesOrder *dataSalesOrder;
    if(appDelegate.salesOrder.Type==SalesOrderMain)
    {
        dataSalesOrder = [[KDSDataSalesOrder alloc] init];
    }
    else if(appDelegate.salesOrder.Type==SalesOrderBlind)
    {
        dataSalesOrder = [[KDSDataBlindSalesOrder alloc] init];
    }
    NSArray* tNewSalesOrderArr=[dataSalesOrder selectSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:NO IsDeleted:NO];
    self.salesOrderArr=tNewSalesOrderArr;
    [dataSalesOrder release];
}

-(void)reloadNewOrderTable
{
    //[self initializeTableData];
    [self.editOrderTableController initializeTableData];
    [newOrderTabelView reloadData];
}

-(void)handleRecievedResponseMessage:(XMLResponse *)responseMessage
{
    if([responseMessage isKindOfClass:[TransferOrderPDFResponse class]])
    {
        NSString *feedBackResult=((TransferOrderPDFResponse*)responseMessage).getPDFPath;
        
        
        
        if (![feedBackResult isEqualToString:@""])
        {
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
                
                mailer.mailComposeDelegate = self;
                
                NSString *mailsubject=[[NSString alloc]initWithFormat:@"%@",selectedOrder.customer.Customer_Name];
                
                [mailer setSubject:mailsubject];
                
                NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
                [mailer setToRecipients:toRecipients];
                
                //NSString *PDFpath=[[NSString alloc]initWithString:@"http://www.mobilesolutions.com.my/TUPPERWARE/AgendaSlides/Carhiremarket&Suncars_Documentation.pdf"];
                
                KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
                NSError *error;
                NSString *strhost=[appDelegate loadHostIntoString];
                NSString *settingFileContents=[NSString stringWithContentsOfFile:strhost encoding:NSASCIIStringEncoding error:&error];
                NSString *HostIP = [NSString stringWithFormat:@"http://%@/KDSOrderPDFConvertor/PDF/%@",settingFileContents,feedBackResult];

                NSString *PDFpath=[[NSString alloc]initWithFormat:@"%@",HostIP];
                //NSString *emailBody = @"Have you seen the MobileTuts+ web site?";
                
                NSString *emailBody = [[NSString alloc]initWithFormat:@"You got a new pdf, click here to view---> %@",PDFpath];
                [mailer setMessageBody:emailBody isHTML:NO];
                
                // only for iPad
                // mailer.modalPresentationStyle = UIModalPresentationPageSheet;
                
                [self presentModalViewController:mailer animated:YES];
                
                [mailer release];
                
                [self.loadingView removeView];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                message:@"Your device doesn't support the composer sheet"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles: nil];
                [alert show];
                [alert release];
            }
            
            //[self.loadingView removeView];
        }
    }else if([responseMessage isKindOfClass:[QuotationFormPdfResponse class]])
    {
        NSString *feedBackResult=((QuotationFormPdfResponse*)responseMessage).getPDFPath;
        
        
        
        if (![feedBackResult isEqualToString:@""])
        {
            if ([MFMailComposeViewController canSendMail])
            {
                MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
                
                mailer.mailComposeDelegate = self;
                
                NSString *mailsubject=[[NSString alloc]initWithFormat:@"%@",selectedOrder.customer.Customer_Name];
                
                [mailer setSubject:mailsubject];
                
                NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
                [mailer setToRecipients:toRecipients];
                
                //NSString *PDFpath=[[NSString alloc]initWithString:@"http://www.mobilesolutions.com.my/TUPPERWARE/AgendaSlides/Carhiremarket&Suncars_Documentation.pdf"];
                
                KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
                NSError *error;
                NSString *strhost=[appDelegate loadHostIntoString];
                NSString *settingFileContents=[NSString stringWithContentsOfFile:strhost encoding:NSASCIIStringEncoding error:&error];
                NSString *HostIP = [NSString stringWithFormat:@"http://%@/KDSQuotationPDFConvertor/PDF/%@",settingFileContents,feedBackResult];
                
                NSString *PDFpath=[[NSString alloc]initWithFormat:@"%@",HostIP];
                //NSString *emailBody = @"Have you seen the MobileTuts+ web site?";
                
                NSString *emailBody = [[NSString alloc]initWithFormat:@"You got a new pdf, click here to view---> %@",PDFpath];
                [mailer setMessageBody:emailBody isHTML:NO];
                
                // only for iPad
                // mailer.modalPresentationStyle = UIModalPresentationPageSheet;
                
                [self presentModalViewController:mailer animated:YES];
                
                [mailer release];
                
                [self.loadingView removeView];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
                                                                message:@"Your device doesn't support the composer sheet"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles: nil];
                [alert show];
                [alert release];
            }
            
            //[self.loadingView removeView];
        }
    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Process Failed!" message:@" \n\n\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
        //alert.frame=CGRectMake(100, 100, 300, 300);
        
        UILabel *myTextField = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 0, 260, 325)];
        myTextField.text=@"Please ensure the order does not consists of any:\n- Special character, for example &$*<>/\n- Only english keyboard is allowed, do not use other keyboard type, for example, pinyin keyboard.\n- Try the same action with other order and if same problem occur again, please check your server status.";
        myTextField.textColor=[UIColor whiteColor];
        [myTextField setBackgroundColor:[UIColor clearColor]];
        myTextField.textAlignment=UITextAlignmentLeft;
        myTextField.lineBreakMode = UILineBreakModeWordWrap;
        myTextField.numberOfLines=20;
        [alert addSubview:myTextField];
        
//        UIButton*testButton=[UIButton buttonWithType:UIButtonTypeRoundedRect];
//        [testButton setFrame:CGRectMake(0, 0, 40, 40)];
//        [testButton addTarget:self action:@selector(someAction) forControlEvents:UIControlEventTouchUpInside];
//        [alert addSubview:testButton];
        //alert.cancelButtonIndex=20;
        //[alert setNumberOfRows:20];
        
//        alert.cancelButtonIndex = 1;
//        //UIButton* button1 = (UIButton*)[alert.subviews objectAtIndex:1];
//        UIButton *button1=[UIButton buttonWithType:UIButtonTypeRoundedRect];
//        //button1 = (UIButton*)[alert.subviews objectAtIndex:1];
//        [button1 setFrame:CGRectMake(0, 0, 40, 30)];
//        [alert addSubview:button1];
        
        [alert show];
        [alert release];
        
        NSLog(@"fail transfer");
        
        [self.loadingView removeView];
    }
}
- (void)willPresentAlertView:(UIAlertView *)alertView {
	[alertView setFrame:CGRectMake(250, self.view.frame.size.height/2, 285, 360)];
	alertView.center = CGPointMake(self.view.frame.size.width/2 , self.view.frame.size.height/2 );
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSInteger cancelIndex = [alertView cancelButtonIndex];
    if (cancelIndex != -1 && cancelIndex == buttonIndex)
    {
       
        NSLog(@"hello calcellllll");
    }else
    {
        NSLog(@"hello bla bla bla");
    }
}

- (IBAction)openMail:(id)sender
{
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
        
        mailer.mailComposeDelegate = self;
        
        [mailer setSubject:@"A Message from Felton"];
        
        NSArray *toRecipients = [NSArray arrayWithObjects:@"", nil];
        [mailer setToRecipients:toRecipients];
        
        UIImage *myImage = [UIImage imageNamed:@"mobiletuts-logo.png"];
        NSData *imageData = UIImagePNGRepresentation(myImage);
        [mailer addAttachmentData:imageData mimeType:@"image/png" fileName:@"mobiletutsImage"];	
        
        NSString *emailBody = @"Have you seen the MobileTuts+ web site?";
        [mailer setMessageBody:emailBody isHTML:NO];
        
        // only for iPad 
        // mailer.modalPresentationStyle = UIModalPresentationPageSheet;
        
        [self presentModalViewController:mailer animated:YES];
        
        [mailer release];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure" 
                                                        message:@"Your device doesn't support the composer sheet" 
                                                       delegate:nil 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    
}



#pragma mark - MFMailComposeController delegate


- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error 
{	
	switch (result)
	{
		case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled: you cancelled the operation and no email message was queued");
			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved: you saved the email message in the Drafts folder");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail send: the email message is queued in the outbox. It is ready to send the next time the user connects to email");
			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail failed: the email message was nog saved or queued, possibly due to an error");
			break;
		default:
			NSLog(@"Mail not sent");
			break;
	}
    
	[self dismissModalViewControllerAnimated:YES];
}



@end
