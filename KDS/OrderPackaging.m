//
//  OrderPackaging.m
//  KDS
//
//  Created by Tiseno Mac 2 on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "OrderPackaging.h"

@implementation OrderPackaging
@synthesize orderID,packageID;
//@synthesize _Order_Package;
@synthesize orderPackagearr;

-(void)dealloc
{
    [orderPackagearr release];
    [_Order_Package release];
    [packageID release];
    [orderID release];
    [super dealloc];
}

-(void) addOrderPackage:(NSArray*)orderItem
{ 
    
        if(_Order_Package==nil)
        { 
            _Order_Package=[[NSMutableArray alloc] init];
        }
        [_Order_Package addObject:orderItem];
        //return YES;
   
}

-(NSMutableArray*) Order_Package
{
    return _Order_Package;
}

@end
