//
//  DefaultPriceAndUOM.h
//  KDS
//
//  Created by Tiseno on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface DefaultPriceAndUOM : NSObject {
    
    NSString * _Default_Price;
    NSString * _Uom;
}
@property (nonatomic, retain) NSString * Default_Price, *Uom;
@end
