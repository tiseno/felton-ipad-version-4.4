//
//  ProductTableCell.m
//  KDS
//
//  Created by Tiseno on 1/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ProductTableCell.h"
#define cell1Width 170
#define cell2Width 452

@implementation ProductTableCell
@synthesize productNameLabel, priceLabel,productIdLabel, lineColor, itemm;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, 843, 45);
        
        UILabel *tProductIdLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, 170, 45)];
        tProductIdLabel.textAlignment = UITextAlignmentLeft;
        tProductIdLabel.textColor = [UIColor blackColor];
        tProductIdLabel.backgroundColor = [UIColor clearColor];
        self.productIdLabel = tProductIdLabel;
        [tProductIdLabel release];
        [self addSubview:productIdLabel];
        
        UILabel *tProductLabel = [[UILabel alloc] initWithFrame:CGRectMake(175, 0, 465, 45)];
        tProductLabel.textAlignment = UITextAlignmentLeft;
        tProductLabel.textColor = [UIColor blackColor];
        tProductLabel.backgroundColor = [UIColor clearColor];
        self.productNameLabel = tProductLabel;
        [tProductLabel release];
        [self addSubview:productNameLabel];
        
        UILabel *tPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(645, 0, 190, 45)];
        tPriceLabel.textAlignment = UITextAlignmentLeft;
        tPriceLabel.textColor = [UIColor blackColor];
        tPriceLabel.backgroundColor = [UIColor clearColor];
        self.priceLabel = tPriceLabel;
        [tPriceLabel release];
        [self addSubview:priceLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetStrokeColorWithColor(context, lineColor.CGColor);       
    
	// CGContextSetLineWidth: The default line width is 1 unit. When stroked, the line straddles the path, with half of the total width on either side.
	// Therefore, a 1 pixel vertical line will not draw crisply unless it is offest by 0.5. This problem does not seem to affect horizontal lines.
	CGContextSetLineWidth(context, 0.5);
    
	// Add the vertical lines
	CGContextMoveToPoint(context, cell1Width+0.5, 0);
	CGContextAddLineToPoint(context, cell1Width+0.5, rect.size.height);
    
	CGContextMoveToPoint(context, cell1Width+cell2Width+20.5, 0);
	CGContextAddLineToPoint(context, cell1Width+cell2Width+20.5, rect.size.height);
    
	
	// Draw the lines
	CGContextStrokePath(context); 
}
- (void)dealloc
{
    //[itemm release];
    [lineColor release];
    [productIdLabel release];
    [productNameLabel release];
    [priceLabel release];
    [super dealloc];
}

@end
