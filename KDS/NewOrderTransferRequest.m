//
//  NewOrderTransferRequest.m
//  KDS
//
//  Created by Tiseno on 2/1/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "NewOrderTransferRequest.h"


@implementation NewOrderTransferRequest
@synthesize newSalesOrder,salesPersonCode;
-(id)initWithSalesOrder:(SalesOrder*)isaleOrder SalesPersonCode:(SalesPerson*)isalesPersonCode
{
    self=[super init];
    if(self)
    {
        KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
        NSError *error;
        NSString *strhost=[appDelegate loadHostIntoString];
        NSString *settingFileContents=[NSString stringWithContentsOfFile:strhost encoding:NSASCIIStringEncoding error:&error];
        NSString *HostIP = [NSString stringWithFormat:@"http://%@/KDSOrderTransfer/service.asmx",settingFileContents];

        self.webserviceURL=HostIP;
        self.SOAPAction=@"TransferOrder";
        self.requestType=WebserviceRequest;
        self.newSalesOrder=isaleOrder;
        self.salesPersonCode=isalesPersonCode;
    }
    return self;
}
-(NSString*) generateHTTPPostMessage
{ 
	NSString* xmlRequest=[self.newSalesOrder convertToXMLWithSalesPersonCode:self.salesPersonCode];
    NSString *xmlStr=[NSString stringWithFormat:@"XMLRequest=%@",xmlRequest];
    //NSLog(@"%@",xmlStr);
	return xmlStr;

}
-(void) dealloc
{
    [newSalesOrder release];
    [salesPersonCode release];
    [super dealloc];
}
@end
