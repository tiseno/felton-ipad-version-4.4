//
//  SynchDataViewController.h
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NetworkHandler.h"
#import "NetworkHandlerDelegate.h"
#import "CustomersBySalespersonIDRequest.h"
#import "CustomersBySalespersonIDResponse.h"
#import "AllItemDetailsRequest.h"
#import "AllItemDetailsResponse.h"
#import "KDSDataCustomer.h"
#import "KDSDataItem.h"
#import "LoadingView.h"
#import "KDSAppDelegate.h"
#import "Reachability.h"
#import "ActiveCaseItemsRequest.h"
#import "DownloadItemImagesRequest.h"
#import "DownloadItemImagesReaponse.h"
#import "ActiveCaseblindItemsRequest.h"
#import "ActiveCasemainItemsRequest.h"
#import "OrderPackagingRequest.h"
#import "OrderPackagingResponse.h"
#import "KDSDateOrderPackage.h"
#import "KDSDataSalesOrder.h"

@class Reachability;
@interface SynchDataViewController : UIViewController<NetworkHandlerDelegate> {
    UISwitch *productSwitch;
    UISwitch *customerSwitch;
    UISwitch *DownloadImagesSwitch;
    
    UILabel *lblproduct;
    UILabel *lbldlimg;
    UILabel *lblcustomerimg;
    
    LoadingView *loadingView;
    BOOL productSelected;
    BOOL customerSelected;
    BOOL productRecieved;
    BOOL customerRecieved;
    BOOL DownloadImageSelected;
    BOOL DownloadImageRecieved;
    BOOL OrderPackageSelected;
    BOOL OrderPackageRecieved;
    
    int counterA;
    bool startA;
    NSString *CountDownTime;
    NSTimer *timerA;
    BOOL loadData;
    NSString *productTypes;
    
    
}
-(IBAction)saveHost;
@property (retain, nonatomic) IBOutlet UITextField *txtMapip;
@property (nonatomic, retain) NSString *productTypes;
@property (nonatomic,retain) NSString *CountDownTime;
@property (nonatomic, retain) IBOutlet UISwitch *productSwitch;
@property (nonatomic, retain) IBOutlet UISwitch *customerSwitch;
@property (nonatomic, retain) LoadingView *loadingView;
@property (retain, nonatomic) IBOutlet UISwitch *DownloadImagesSwitch;

@property (retain, nonatomic) IBOutlet UILabel *lblproduct;
@property (retain, nonatomic) IBOutlet UILabel *lbldlimg;
@property (retain, nonatomic) IBOutlet UILabel *lblcustomerimg;

-(void)requestOrderPackage;
-(void)requestCustomers;
-(void)requestProducts;
-(BOOL)NetworkStatus;
-(void)requestDownloadImages;
-(void)requestBlindProducts;
-(void)requestMainProducts;
-(void)requestDownloadImagesblind;
-(void)alertcloseapp;

-(IBAction)startPackageIDTapped;
-(IBAction)startproductTapped;
-(IBAction)startcustomerTapped;
-(IBAction)startproductimageTapped;
-(IBAction)startproductBlindTapped;
-(IBAction)startproductimageTappedblind;
-(void)removeMainProductTapped;
-(IBAction)removeMainProductTapped;
-(IBAction)removeBlindProductTapped;

@end
