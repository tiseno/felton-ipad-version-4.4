//
//  NewOrderTableViewController.m
//  KDS
//
//  Created by Tiseno on 11/25/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "NewOrderTableViewController.h"


@implementation NewOrderTableViewController
@synthesize ordereditViewController;// orderListViewController;
@synthesize neworderSalesOrderArray;
-(id)init
{ 
    self=[super init];
    if(self)
    {
        [self initializeTableData];
    }
    return  self;
}
- (void)dealloc
{
    [neworderSalesOrderArray release];
    //[orderListViewController release];
    [ordereditViewController release];
    [super dealloc];
} 
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}
-(NSInteger) tableView:(UITableView *)table numberOfRowsInSection:(NSInteger)section
{
    if(self.neworderSalesOrderArray!=nil)
    {
        return [self.neworderSalesOrderArray count];
    }
    return 0;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    //NewOrderTableViewCell *cell = (NewOrderTableViewCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    SalesOrder* salesOrder=[self.neworderSalesOrderArray objectAtIndex:indexPath.row];
    //if(cell == nil) 
    //{ 
    NewOrderTableViewCell* cell = [[NewOrderTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.delegate =self.ordereditViewController;
    //cell.delegate = self.orderListViewController; 
    cell.salesOrder = salesOrder;
    cell.lineColor = [UIColor blackColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.nameLabel.text = salesOrder.customer.Customer_Name; 
    cell.orderIdLabel.text = salesOrder.systemOrderId;
    NSString *grandPrice = [NSString stringWithFormat:@"RM %.2f", salesOrder.grand_Price];
    cell.priceLabel.text = grandPrice;
    
    if(indexPath.row == 0)
        cell.topCell = YES;
    else
        cell.topCell = NO;
    //}
    return cell;
}
-(void)initializeTableData
{
    KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    KDSDataSalesOrder *dataSalesOrder;
    if(appDelegate.salesOrder.Type==SalesOrderMain)
    {
        dataSalesOrder = [[KDSDataSalesOrder alloc] init];
    }
    else //if(appDelegate.salesOrder.Type==SalesOrderBlind)
    {
        dataSalesOrder = [[KDSDataBlindSalesOrder alloc] init];
    } 
    NSArray* tNewSalesOrderArr=[dataSalesOrder selectSalesOrdersforSalesPerson:appDelegate.loggedInSalesPerson IsUploaded:NO IsDeleted:NO];
    self.neworderSalesOrderArray=tNewSalesOrderArr;
    [dataSalesOrder release];
}

@end
