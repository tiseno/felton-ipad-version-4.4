//
//  KDSDataLayerBase.h
//  KDS
//
//  Created by Tiseno on 12/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "sqlite3.h"
#import "DataBaseConnectionOpenResult.h"

@interface KDSDataLayerBase : NSObject {
    
    sqlite3 *dbKDS;
    NSString *databaseName;
}

-(id)initWithDataBaseName:(NSString*)idataBaseName;
-(DataBaseConncetionOpenResult)openConnection;
-(void)closeConnection;

@end
