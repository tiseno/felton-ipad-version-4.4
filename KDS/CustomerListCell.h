//
//  CustomerListCell.h
//  KDS
//
//  Created by Tiseno Mac 2 on 2/6/13.
//
//

#import <UIKit/UIKit.h>
#import "Customer.h"
@interface CustomerListCell : UITableViewCell{
    
}
@property (nonatomic, retain) Customer *customer;
@property (nonatomic, retain) UILabel *companyNameLabel;
@property (nonatomic, retain) UILabel *companyNameIDLabel;
@property (nonatomic, retain) UIButton *orderHistoryButton;

@end
