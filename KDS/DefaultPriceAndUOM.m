//
//  DefaultPriceAndUOM.m
//  KDS
//
//  Created by Tiseno on 12/5/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "DefaultPriceAndUOM.h"


@implementation DefaultPriceAndUOM

@synthesize Uom = _Uom, Default_Price = _Default_Price;
-(void)dealloc
{
    [_Uom release];
    [_Default_Price release];
    [super dealloc];
}

@end
