//
//  DeletedCartViewController.m
//  KDS
//
//  Created by Tiseno Mac 2 on 5/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DeletedCartViewController.h"

//@interface DeletedCartViewController ()

//@end
 
@implementation DeletedCartViewController
@synthesize _tableView;
@synthesize remarkTextView;
@synthesize DeliveryDateTextField,itemCategoryDictionary;
@synthesize orderItemsArr,orderItem,totalDiscountOfItems;

@synthesize lbldeliveryDate;
@synthesize DescripttionTextView;
@synthesize lblDescription;
@synthesize tableViewHeader;
@synthesize  installationNameTextField, cityTextField, stateTextField, dateTextField, timeTextField, telTextField, faxTextField, emailTextField, transportationTextField, installationTextField, CurrentSaleOrder, totalDiscountTextField, backgroundImageView, discountOnTotalPrice, alertUser, backButton;
@synthesize reviewUIView, reviewTableViewController, reviewTableView;
@synthesize grandTotalLabel, totalPriceLabel, orderIdLabel, orderIdValueLabel,allsalesItem;
@synthesize UpdateCartUIView;
@synthesize editCartViewcoltroller;
@synthesize EditCartSmallView,editCartController;
@synthesize UpdaCartPrice;
@synthesize gpriceTextField;
@synthesize UpdateOrderItemIndex,getsumOfOrderItemPrice;
@synthesize EditTag;
@synthesize lbltransportation;
@synthesize lblinstallation;
@synthesize lblinstallationName;
@synthesize lblinstallationAddress;
@synthesize lbldate;
@synthesize lbltime;
@synthesize lbltel;
@synthesize lblfax;
@synthesize lblcontact;
@synthesize lblcustomerCompanyName;
@synthesize lblcustomerTel;
@synthesize txtcusRemark;
@synthesize lblcusRemark;
@synthesize txtorderref;
@synthesize installationAddressTextView;
@synthesize lblreference;
@synthesize ShippingAddress;
@synthesize lblshippingAddress;
@synthesize lblPO;
@synthesize txtPO;
@synthesize mainScrolView;
@synthesize btnordersummary;
@synthesize lblremarkmain;
@synthesize lblQtymain;
@synthesize lblunitPricemain;
@synthesize lbldiscountmain;
@synthesize lbltotalmain;
@synthesize lbldescblind;
@synthesize lblcontrolblind;
@synthesize lblqtyblind;
@synthesize lblsqftblind;
@synthesize lbltotalsqftblind;
@synthesize lblunitpriceblind;
@synthesize lbldiscountblind;
@synthesize lbltotalblind;
@synthesize lbldeliverydateblind;
@synthesize txtdeliveryDateblind;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        isTableLoaded = NO;
        numberOfCellsGenerated=0;
        isDiscountAmount = YES;
        shouldUpdateCart=NO;

    }
    return self;
}






- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //KDSDataItem *dataItem=[[KDSDataItem alloc] init];
    //NSArray *items=[dataItem selectItem];
    //[self fillItemCateogryDictionaryWithItemArr:items];
    //[dataItem release];

    
    
    KDSAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    if(appDelegate.TransferOrdersalesOrder.remark != nil && ![appDelegate.TransferOrdersalesOrder.remark isEqualToString:@""])
    {
        remarkTextView.text = appDelegate.TransferOrdersalesOrder.remark;
    }
    
    
    NSString *customerName=[NSString stringWithFormat:@"%@",appDelegate.OrderTransfercurrentCustomer.Customer_Name];
    lblcustomerCompanyName.text=customerName;
    NSString *customerTel=[NSString stringWithFormat:@"%@",appDelegate.OrderTransfercurrentCustomer.Customer_Id];
    lblcustomerTel.text=customerTel;
    
    BOOL result;
    result = [customerTel hasPrefix: @"CASH"];
    
    
    if (result) {
         
        lblcusRemark.hidden=NO;
        txtcusRemark.hidden=NO;
        
        if(appDelegate.TransferOrdersalesOrder.CashRemarks != nil && ![appDelegate.TransferOrdersalesOrder.CashRemarks isEqualToString:@""])
        {
            if ([appDelegate.TransferOrdersalesOrder.CashRemarks isEqualToString:@"(null)"]) 
            {
                txtcusRemark.text= @"";
            }else 
            {
                txtcusRemark.text = appDelegate.TransferOrdersalesOrder.CashRemarks;
            }
        }
        
    }
    else {
        
        lblcusRemark.hidden=YES;
        txtcusRemark.hidden=YES;
        
    }
    
    
    NSString *totalDiscountStr=[NSString stringWithFormat:@"%.2f",appDelegate.TransferOrdersalesOrder.order_discount];
    totalDiscountTextField.text=totalDiscountStr;

    //NSLog(@"%d",appDelegate.TransferOrdersalesOrder.Type);
    if (appDelegate.TransferOrdersalesOrder.Type==SalesOrderBlind) 
    {
        
        if(appDelegate.TransferOrdersalesOrder.Order_Reference != nil && ![appDelegate.salesOrder.Order_Reference isEqualToString:@""])
        {
            txtPO.text = appDelegate.TransferOrdersalesOrder.Order_Reference;
        }
        
        backgroundImageView.image = [UIImage imageNamed:@"your_cart_page2.png"];
        installationTextField.hidden=NO;
        transportationTextField.hidden=NO;
        installationAddressTextView.hidden = NO;
        cityTextField.hidden = YES;
        stateTextField.hidden = YES;
        dateTextField.hidden = NO;
        timeTextField.hidden = NO;
        telTextField.hidden = NO;
        faxTextField.hidden = NO;
        lblreference.hidden=YES;
        ShippingAddress.hidden=YES;
        lblshippingAddress.hidden=YES;
        txtPO.hidden=NO;
        lblPO.hidden=NO;
        emailTextField.hidden = NO;
        lblDescription.hidden=NO;
        DescripttionTextView.hidden=NO;
        lbldeliveryDate.hidden=YES;
        DeliveryDateTextField.hidden=YES;
    
        
        lblremarkmain.hidden=YES;
        lblQtymain.hidden=YES;
        lblunitPricemain.hidden=YES;
        lbldiscountmain.hidden=YES;
        lbltotalmain.hidden=YES;
        
        lbldescblind.hidden=NO;
        lblcontrolblind.hidden=NO;
        lblqtyblind.hidden=NO;
        lblsqftblind.hidden=NO;
        lbltotalsqftblind.hidden=NO;
        lblunitpriceblind.hidden=NO;
        lbldiscountblind.hidden=NO;
        lbltotalblind.hidden=NO;
        
        txtdeliveryDateblind.hidden=NO;
        lbldeliverydateblind.hidden=NO;
        
        
    DescripttionTextView.text=appDelegate.TransferOrdersalesOrder.OrderDescription;
    
        if(appDelegate.TransferOrdersalesOrder.DeliveryDate != nil && ![appDelegate.TransferOrdersalesOrder.DeliveryDate isEqualToString:@""])
        {
            if ([appDelegate.TransferOrdersalesOrder.DeliveryDate isEqualToString:@"(null)"]) 
            {
                txtdeliveryDateblind.text= @"";
            }else 
            {
                txtdeliveryDateblind.text=appDelegate.TransferOrdersalesOrder.DeliveryDate;
            }
        }
    
    if ([((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_Name isEqualToString:@"(null)"]) {
        installationNameTextField.text= @"";
    }else {
        installationNameTextField.text= ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_Name;
    }
    
    
    installationAddressTextView.text = ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installationorder_Address;
    
    if ([((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_City isEqualToString:@"(null)"]) {
        dateTextField.text=@"";
    }else {
        dateTextField.text = ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_City;
    }
    
    if ([((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_State isEqualToString:@"(null)"]) {
        timeTextField.text=@"";
    }else {
        timeTextField.text = ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_State;
    }
    
    if ([((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_Phone isEqualToString:@"(null)"]) {
        telTextField.text=@"";
    }else {
        telTextField.text=((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_Phone;
    }
    
    if ([((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_Fax isEqualToString:@"(null)"]) {
        faxTextField.text=@"";
    }else {
        faxTextField.text=((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_Fax;
    }
    if ([((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_Email isEqualToString:@"(null)"]) {
        emailTextField.text=@"";
    }else {
        emailTextField.text=((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_Email;
    }
    
    
}
    else
        if(appDelegate.TransferOrdersalesOrder.Type==SalesOrderMain)
    {
        backgroundImageView.image = [UIImage imageNamed:@"your_cart_page2.png"];
        installationNameTextField.hidden = YES;
        installationAddressTextView.hidden = YES;
        installationTextField.hidden=YES;
        transportationTextField.hidden=YES;
        cityTextField.hidden = YES;
        stateTextField.hidden = YES;
        dateTextField.hidden = YES;
        timeTextField.hidden = YES;
        telTextField.hidden = YES;
        faxTextField.hidden = YES;
        emailTextField.hidden = YES;
        txtPO.hidden=YES;
        lblPO.hidden=YES;
        
        lbltransportation.hidden=YES;
        lblinstallation.hidden=YES;
        lblinstallationName.hidden=YES;
        lblinstallationAddress.hidden=YES;
        lbldate.hidden=YES;
        lbltime.hidden=YES;
        lbltel.hidden=YES;
        lblfax.hidden=YES;
        lblcontact.hidden=YES;
        
        lblshippingAddress.hidden=NO;
        ShippingAddress.hidden=NO;
        DescripttionTextView.hidden=YES;
        lblDescription.hidden=YES;
        
        lbldeliveryDate.hidden=NO;
        DeliveryDateTextField.hidden=NO;
    
        lblremarkmain.hidden=NO;
        lblQtymain.hidden=NO;
        lblunitPricemain.hidden=NO;
        lbldiscountmain.hidden=NO;
        lbltotalmain.hidden=NO;
        
        lbldescblind.hidden=YES;
        lblcontrolblind.hidden=YES;
        lblqtyblind.hidden=YES;
        lblsqftblind.hidden=YES;
        lbltotalsqftblind.hidden=YES;
        lblunitpriceblind.hidden=YES;
        lbldiscountblind.hidden=YES;
        lbltotalblind.hidden=YES;

        txtdeliveryDateblind.hidden=YES;
        lbldeliverydateblind.hidden=YES;
    
    DeliveryDateTextField.text=appDelegate.TransferOrdersalesOrder.DeliveryDate;
        
        if(appDelegate.TransferOrdersalesOrder.DeliveryDate != nil && ![appDelegate.TransferOrdersalesOrder.DeliveryDate isEqualToString:@""])
        {
            if ([appDelegate.TransferOrdersalesOrder.DeliveryDate isEqualToString:@"(null)"]) 
            {
                DeliveryDateTextField.text= @"";
            }else 
            {
                DeliveryDateTextField.text=appDelegate.TransferOrdersalesOrder.DeliveryDate;
            }
        }
        
        if(appDelegate.TransferOrdersalesOrder.Shipping_Address != nil && ![appDelegate.TransferOrdersalesOrder.Shipping_Address isEqualToString:@""])
        {
            //NSLog(@"appDelegate.TransferOrdersalesOrder.Shipping_Address>>>%@",appDelegate.TransferOrdersalesOrder.Shipping_Address);
            ShippingAddress.text = appDelegate.TransferOrdersalesOrder.Shipping_Address;
        }
        
        if(appDelegate.TransferOrdersalesOrder.Order_Reference != nil && ![appDelegate.TransferOrdersalesOrder.Order_Reference isEqualToString:@""])
        {
            txtorderref.text = appDelegate.TransferOrdersalesOrder.Order_Reference;
        }
    
}
    if(tableViewHeader == nil)
    {
    self._tableView.rowHeight = 55;
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(_tableView.frame.origin.x, _tableView.frame.origin.y-30, self._tableView.frame.size.width, 30)];
    tableHeaderView.backgroundColor = [UIColor colorWithRed:0.0196 green:0.513 blue:0.949 alpha:1.0];
    
    /*UILabel *tableHeaderLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, self._tableView.frame.size.width/2, 21)];
     tableHeaderLabel.text = @"Your Order";
     tableHeaderLabel.backgroundColor = [UIColor clearColor];
     tableHeaderLabel.textColor = [UIColor whiteColor];
     tableHeaderLabel.textAlignment = UITextAlignmentLeft;
     [tableHeaderView addSubview:tableHeaderLabel];
     
     UILabel *itemIdLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 150, 35)];
     itemIdLabel.text = @"Product ID";
     itemIdLabel.textAlignment = UITextAlignmentCenter;
     itemIdLabel.backgroundColor = [UIColor clearColor];
     itemIdLabel.textColor = [UIColor whiteColor];
     [tableHeaderView addSubview:itemIdLabel];
     [itemIdLabel release];*/
    
    UILabel *itemNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(90, 0, 200, 30)];
    itemNameLabel.text = @"Product Description";
    itemNameLabel.textAlignment = UITextAlignmentCenter;
    itemNameLabel.backgroundColor = [UIColor clearColor];
    itemNameLabel.textColor = [UIColor whiteColor];
        itemNameLabel.font=[UIFont boldSystemFontOfSize:17];
    [tableHeaderView addSubview:itemNameLabel];
    [itemNameLabel release];
    
    UILabel *itemQuantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(365, 0, 90, 30)];
    itemQuantityLabel.text = @"Quantity";
    itemQuantityLabel.textAlignment = UITextAlignmentCenter;
    itemQuantityLabel.backgroundColor = [UIColor clearColor];
    itemQuantityLabel.textColor = [UIColor whiteColor];
        itemQuantityLabel.font=[UIFont boldSystemFontOfSize:17];
    [tableHeaderView addSubview:itemQuantityLabel];
    [itemQuantityLabel release];
    
    UILabel *orderPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(430, 0, 100, 30)];
    orderPriceLabel.text = @"Price";
    orderPriceLabel.textAlignment = UITextAlignmentCenter;
    orderPriceLabel.backgroundColor = [UIColor clearColor];
    orderPriceLabel.textColor = [UIColor whiteColor];
        orderPriceLabel.font=[UIFont boldSystemFontOfSize:17];
    [tableHeaderView addSubview:orderPriceLabel];
    [orderPriceLabel release];
    
    UILabel *DiscountLabel = [[UILabel alloc] initWithFrame:CGRectMake(510, 0, 100, 30)];
    DiscountLabel.text = @"Discount";
    DiscountLabel.textAlignment = UITextAlignmentCenter;
    DiscountLabel.backgroundColor = [UIColor clearColor];
    DiscountLabel.textColor = [UIColor whiteColor];
        DiscountLabel.font=[UIFont boldSystemFontOfSize:17];
    [tableHeaderView addSubview:DiscountLabel];
    [DiscountLabel release];
    
    UILabel *TotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(605, 0, 100, 30)];
    TotalLabel.text = @"Total";
    TotalLabel.textAlignment = UITextAlignmentCenter;
    TotalLabel.backgroundColor = [UIColor clearColor];
    TotalLabel.textColor = [UIColor whiteColor];
        TotalLabel.font=[UIFont boldSystemFontOfSize:17];
    [tableHeaderView addSubview:TotalLabel];
    [TotalLabel release];
    
    //self.tableViewHeader = tableHeaderView;
    //[self.view addSubview:tableViewHeader];
    
    
    
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
    
    [tableHeaderView release];
    //[tableHeaderLabel release];
}
    if(appDelegate.TransferOrdersalesOrder.systemOrderId != nil)
    {
    orderIdLabel.hidden = NO;
    orderIdValueLabel.hidden=NO;
    orderIdValueLabel.text = appDelegate.TransferOrdersalesOrder.systemOrderId;
    if(appDelegate.TransferOrdersalesOrder.Type==SalesOrderBlind)
    {
        NSString* installationCostStr = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).installation];
        installationTextField.text=installationCostStr;
        NSString* transportationCostStr = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).transportation];
        transportationTextField.text=transportationCostStr;
    }
}else
    {
    orderIdLabel.hidden = YES;
    orderIdValueLabel.hidden=YES;
    /*orderIdValueLabel.text = appDelegate.salesOrder.systemOrderId;
     if(appDelegate.salesOrder.Type==SalesOrderBlind)
     {
     NSString* installationCostStr = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrder*)appDelegate.salesOrder).installation];
     installationTextField.text=installationCostStr;
     NSString* transportationCostStr = [NSString stringWithFormat:@"%.2f", ((BlindSalesOrder*)appDelegate.salesOrder).transportation];
     transportationTextField.text=transportationCostStr;
     }*/
}

    btnordersummary.hidden=YES;
    self.mainScrolView.contentSize=CGSizeMake(self.mainScrolView.frame.size.width, 1000);
    [self updateCart];

}

- (void)viewDidUnload
{
    /*[self set_tableView:nil];
    [self setRemarkTextView:nil];
    [self setDeliveryDateTextField:nil];
    [self setLbltransportation:nil];
    [self setLblinstallation:nil];
    [self setLblinstallationName:nil];
    [self setLblinstallationAddress:nil];
    [self setLbldate:nil];
    [self setLbltime:nil];
    [self setLbltel:nil];
    [self setLblfax:nil];
    [self setLblcontact:nil];
    [self setDeliveryDateTextField:nil];
    [self setLbldeliveryDate:nil];
    [self setDescripttionTextView:nil];
    [self setLblDescription:nil];
    [self setLblcustomerCompanyName:nil];
    [self setLblcustomerTel:nil];
    [self setTxtcusRemark:nil];
    [self setLblcusRemark:nil];
    [self setTxtorderref:nil];
    [self setInstallationAddressTextView:nil];*/
    [self setLblreference:nil];
    [self setShippingAddress:nil];
    [self setLblshippingAddress:nil];
    [self setLblPO:nil];
    [self setTxtPO:nil];
    [self setMainScrolView:nil];
    [self setBtnordersummary:nil];
    [self setLblremarkmain:nil];
    [self setLblQtymain:nil];
    [self setLblunitPricemain:nil];
    [self setLbldiscountmain:nil];
    [self setLbltotalmain:nil];
    [self setLbldescblind:nil];
    [self setLblcontrolblind:nil];
    [self setLblqtyblind:nil];
    [self setLblsqftblind:nil];
    [self setLbltotalsqftblind:nil];
    [self setLblunitpriceblind:nil];
    [self setLbldiscountblind:nil];
    [self setLbltotalblind:nil];
    [self setLbldeliverydateblind:nil];
    [self setTxtdeliveryDateblind:nil];
    [super viewDidUnload];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}



- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return YES;
}

/*-(IBAction)orderListButtonTapped
{
    OrderListViewController *orderListViewController=[[OrderListViewController alloc] initWithNibName:@"OrderListViewController" bundle:nil];
    orderListViewController.title=@"Felton";
    [self.navigationController pushViewController:orderListViewController animated:YES];
    [orderListViewController release];
}*/

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    numberOfCellsGenerated++;
    static NSString *CellIdentifier = @"Cell";
    GridTableView *cell = (GridTableView*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    self.orderItemsArr=[appDelegate.TransferOrdersalesOrder order_Items];
    
    if(cell == nil)
    {
        
        cell = [[[GridTableView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
        
	} else if(cell != nil)
    {
        //NSLog(@"reused cell! (%d)", numberOfCellsGenerated);
    }
    orderItem = [orderItemsArr objectAtIndex:[indexPath row]];
    
    //orderItem=[self.orderItemsArr objectAtIndex:indexPath.row];
    cell.delegate=self;
    cell.lineColor = [UIColor blackColor];
    cell.equivalantOrderItemIndex=indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    
    if (orderItem.item.Category!=nil) 
    {
        if((![orderItem.item.Category isEqualToString:kBlindCategory]) && (![orderItem.item.Category isEqualToString:kblindACategoryA]))
        {
            NSString* quantityStr=[NSString stringWithFormat:@"%.0f",orderItem.Quantity];
            cell.quantityLabel.text=quantityStr;
            
            
        }
        else
        {

            if ([orderItem.item.Category isEqualToString:kBlindCategory]) 
            {
                cell.blindOrderItem=(BlindSalesOrderItem*)orderItem;
                //NSString* quantitySetStr =[NSString stringWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Quantity_in_Set*orderItem.Quantity];
                NSString* quantitySetStr=[NSString stringWithFormat:@"%.0f",orderItem.Quantity];
                cell.quantityLabelblind.text = quantitySetStr;
                
                
            }else 
            {
                cell.blindOrderItem=(BlindSalesOrderItem*)orderItem;
                NSString* quantityStr=[NSString stringWithFormat:@"%.0f",orderItem.Quantity];
                cell.quantityLabelblind.text=quantityStr;
            }
            
        }
        
    }

    
    cell.descriptionTextView.text=orderItem.Comment;
    NSString* priceStr=[NSString stringWithFormat:@"%.2f",orderItem.Unit_Price];
    
    cell.salesOrderItem=orderItem;
    
    if(![orderItem.item.Image_Path isEqualToString:@""])
    {
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* documentsPath = [paths objectAtIndex:0];
        NSString* filePath = [documentsPath stringByAppendingPathComponent:orderItem.item.Image_Path];
        NSData* pngData = [NSData dataWithContentsOfFile:filePath];
        UIImage* timage = [UIImage imageWithData:pngData];
        cell.productImageView.image=timage;
        
    }
    
    float tatalOrderItemPrice=0.0;
    float sumOfOrderItemPrice=0.0;
    if (priceStr!=nil) 
    {
        
        //if (![orderItem.item.Category isEqualToString:kBlindCategory]) 
        if ((![orderItem.item.Category isEqualToString:kBlindCategory]) && (![orderItem.item.Category isEqualToString:kblindACategoryA]))
        {
            sumOfOrderItemPrice = ([cell.quantityLabel.text floatValue]*[priceStr floatValue]);
            
            if (orderItem.Discount_Amount != 0.0) 
            {
                NSString *discountAmount =[[NSString alloc] initWithFormat:@"RM %.2f",orderItem.Discount_Amount];
                cell.discountLabel.text=discountAmount;
                
                NSString *fdiscountAmount =[[NSString alloc] initWithFormat:@"%.2f",orderItem.Discount_Amount];
                
                float Discount_Amount=[fdiscountAmount floatValue];
                totalDiscountOfItems=Discount_Amount;
                tatalOrderItemPrice = sumOfOrderItemPrice - totalDiscountOfItems;
                
                
            }else if (orderItem.Discount_Percent != 0.0) 
            {
                NSString *discountPercent =[[NSString alloc] initWithFormat:@"%.2f %%",orderItem.Discount_Percent];
                cell.discountLabel.text=discountPercent;
                
                NSString *fdiscountPercent =[[NSString alloc] initWithFormat:@"%.2f",orderItem.Discount_Percent];
                
                float Discount_Percent=[fdiscountPercent floatValue];
                totalDiscountOfItems=(Discount_Percent*[priceStr floatValue]*[cell.quantityLabel.text floatValue])/100;
                tatalOrderItemPrice = sumOfOrderItemPrice - totalDiscountOfItems;/**/
            }else 
            {
                NSString *discountPercent =[[NSString alloc] initWithFormat:@"0.00"];
                cell.discountLabel.text=discountPercent;
                
                tatalOrderItemPrice=sumOfOrderItemPrice;
            }
            
        }else 
        {
            if (![orderItem.item.Category isEqualToString:kblindACategoryA])
            {
                NSString* quantitySetStr =[NSString stringWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Quantity_in_Set*orderItem.Quantity];
                sumOfOrderItemPrice = ([quantitySetStr floatValue]*[priceStr floatValue]);
                
                if (orderItem.Discount_Amount != 0.0) 
                {
                    NSString *discountAmount =[[NSString alloc] initWithFormat:@"RM %.2f",orderItem.Discount_Amount];
                    cell.discountLabelblind.text=discountAmount;
                    
                    NSString *fdiscountAmount =[[NSString alloc] initWithFormat:@"%.2f",orderItem.Discount_Amount];
                    
                    float Discount_Amount=[fdiscountAmount floatValue];
                    totalDiscountOfItems=Discount_Amount;
                    tatalOrderItemPrice = sumOfOrderItemPrice - totalDiscountOfItems;
                    
                    
                }else if (orderItem.Discount_Percent != 0.0) 
                {
                    NSString *discountPercent =[[NSString alloc] initWithFormat:@"%.2f %%",orderItem.Discount_Percent];
                    cell.discountLabelblind.text=discountPercent;
                    
                    NSString *fdiscountPercent =[[NSString alloc] initWithFormat:@"%.2f",orderItem.Discount_Percent];
                    
                    float Discount_Percent=[fdiscountPercent floatValue];
                    totalDiscountOfItems=(Discount_Percent*[priceStr floatValue]*[cell.quantityLabelblind.text floatValue])/100;
                    tatalOrderItemPrice = sumOfOrderItemPrice - totalDiscountOfItems;/**/
                }else 
                {
                    NSString *discountPercent =[[NSString alloc] initWithFormat:@"0.00"];
                    cell.discountLabelblind.text=discountPercent;
                    
                    tatalOrderItemPrice=sumOfOrderItemPrice;
                }
            }else 
            {
                sumOfOrderItemPrice = ([cell.quantityLabelblind.text floatValue]*[priceStr floatValue]);
                
                if (orderItem.Discount_Amount != 0.0) 
                {
                    NSString *discountAmount =[[NSString alloc] initWithFormat:@"RM %.2f",orderItem.Discount_Amount];
                    cell.discountLabelblind.text=discountAmount;
                    
                    NSString *fdiscountAmount =[[NSString alloc] initWithFormat:@"%.2f",orderItem.Discount_Amount];
                    
                    float Discount_Amount=[fdiscountAmount floatValue];
                    totalDiscountOfItems=Discount_Amount;
                    tatalOrderItemPrice = sumOfOrderItemPrice - totalDiscountOfItems;
                    
                    
                }else if (orderItem.Discount_Percent != 0.0) 
                {
                    NSString *discountPercent =[[NSString alloc] initWithFormat:@"%.2f %%",orderItem.Discount_Percent];
                    cell.discountLabelblind.text=discountPercent;
                    
                    NSString *fdiscountPercent =[[NSString alloc] initWithFormat:@"%.2f",orderItem.Discount_Percent];
                    
                    float Discount_Percent=[fdiscountPercent floatValue];
                    totalDiscountOfItems=(Discount_Percent*[priceStr floatValue]*[cell.quantityLabelblind.text floatValue])/100;
                    tatalOrderItemPrice = sumOfOrderItemPrice - totalDiscountOfItems;/**/
                }else 
                {
                    NSString *discountPercent =[[NSString alloc] initWithFormat:@"0.00"];
                    cell.discountLabelblind.text=discountPercent;
                    
                    tatalOrderItemPrice=sumOfOrderItemPrice;
                }
            }
            
        }
        
        
        
        
        
        
        if ((![orderItem.item.Category isEqualToString:kBlindCategory]) && (![orderItem.item.Category isEqualToString:kblindACategoryA])) 
            //if (![orderItem.item.Category isEqualToString:kBlindCategory]) 
        {
            NSString *gTotalPrice =[[NSString alloc] initWithFormat:@"RM %.2f",tatalOrderItemPrice];
            cell.TotalLabel.text=gTotalPrice;
            cell.priceLabel.text=priceStr;
            cell.productNameLabelmain.text=orderItem.item.Description;
            
        }else 
        {
            cell.productNameLabel.text=orderItem.item.Description;
            if (![orderItem.item.Category isEqualToString:kblindACategoryA])
            {
                NSString *gTotalPrice =[[NSString alloc] initWithFormat:@"RM %.2f",tatalOrderItemPrice];
                cell.TotalLabelblind.text=gTotalPrice;
                cell.priceLabelblind.text=priceStr;
                
                NSString *gWidthHeight =[[NSString alloc] initWithFormat:@"(%.2f x %.2f)",((BlindSalesOrderItem*)orderItem).Width, ((BlindSalesOrderItem*)orderItem).Height];
                NSString *gcolor =[[NSString alloc] initWithFormat:@"%@",((BlindSalesOrderItem*)orderItem).Color];
                NSString *gcontrol =[[NSString alloc] initWithFormat:@"%@",((BlindSalesOrderItem*)orderItem).Control];
                NSString *gSqft =[NSString stringWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Quantity_in_Set];
                NSString *gtotalSqft =[NSString stringWithFormat:@"%.2f", ((BlindSalesOrderItem*)orderItem).Quantity_in_Set*((BlindSalesOrderItem*)orderItem).Quantity];
                
                cell.widthHeightblind.text=gWidthHeight;
                cell.colorblind.text=gcolor;
                cell.controlblind.text=gcontrol;
                cell.sqftblind.text=gSqft;
                cell.totalsqftblind.text=gtotalSqft;
                //cell.remarkblind.text=[NSString stringWithFormat:@"%@", ((BlindSalesOrderItem*)orderItem).Comment];
                 
            }else 
            {
                NSString *gTotalPrice =[[NSString alloc] initWithFormat:@"RM %.2f",tatalOrderItemPrice];
                cell.TotalLabelblind.text=gTotalPrice;
                cell.priceLabelblind.text=priceStr;
                
                
                NSString *gWidthHeight =[[NSString alloc] initWithFormat:@"" ];
                NSString *gcolor =[[NSString alloc] initWithFormat:@"" ];
                NSString *gcontrol =[[NSString alloc] initWithFormat:@"" ];
                NSString *gSqft =[NSString stringWithFormat:@"" ];
                NSString *gtotalSqft =[NSString stringWithFormat:@"" ];
                
                cell.widthHeightblind.text=gWidthHeight;
                cell.colorblind.text=gcolor;
                cell.controlblind.text=gcontrol;
                cell.sqftblind.text=gSqft;
                cell.totalsqftblind.text=gtotalSqft;
                //cell.remarkblind.text=[NSString stringWithFormat:@"%@", ((BlindSalesOrderItem*)orderItem).Comment];
                
            }
            
        }
        
        
    }
    
    
    
    
    
    if(indexPath.row == 0)
        cell.topCell = YES;
    else
        cell.topCell = NO;
    
    
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    if(appDelegate.TransferOrdersalesOrder!=nil)
    {
        orderItemsArr=[appDelegate.TransferOrdersalesOrder order_Items];
        numberOfRows = orderItemsArr.count;
        return numberOfRows;
    }
    return [orderItemsArr count];
}

-(IBAction)reviewButtonTapped
{
    [self manageReviewView];
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.4];
    reviewUIView.alpha = 1;
    [UIView commitAnimations];
}
-(void)manageReviewView
{
    KDSAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    if(reviewUIView == nil)
    {
        UIView* tReviewUIView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y, self.view.frame.size.width, self.view.frame.size.height)];
        UIImageView* bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, tReviewUIView.frame.size.width, tReviewUIView.frame.size.height)];
        bgImageView.backgroundColor = [UIColor blackColor];
        bgImageView.alpha = 0.5;
        [tReviewUIView addSubview:bgImageView];
        [bgImageView release];
        self.reviewUIView = tReviewUIView;
        [tReviewUIView release];
    }
    if(reviewTableViewController == nil) 
    {
        TransferedReviewTableViewController *treviewTableViewController = [[TransferedReviewTableViewController alloc] init];
        self.reviewTableViewController = treviewTableViewController;
        reviewTableViewController.scrollDelegate = self;
        [treviewTableViewController release];
    }
    if(reviewTableView == nil)
    {
        UITableView *tReviewTableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 104, 1015, 496) style:UITableViewStylePlain];
        self.reviewTableView = tReviewTableView;
        [tReviewTableView release];
    }
    [reviewTableView setDelegate:self.reviewTableViewController];
    [reviewTableView setDataSource:self.reviewTableViewController];
    self.reviewTableViewController.view = self.reviewTableViewController.tableView;
    [reviewTableView reloadData];
    
    UIView *tHeaderView = [[UIView alloc] initWithFrame:CGRectMake(reviewTableView.frame.origin.x, reviewTableView.frame.origin.y-35, reviewTableView.frame.size.width, 35)];
    tHeaderView.backgroundColor = [UIColor lightGrayColor];
    
    if(appDelegate.TransferOrdersalesOrder.Type==SalesOrderBlind)
    {
        /*UILabel *itemIdLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 35)];
         itemIdLabel.text = @"Product ID";
         itemIdLabel.textAlignment = UITextAlignmentCenter;
         itemIdLabel.backgroundColor = [UIColor clearColor];
         itemIdLabel.textColor = [UIColor whiteColor];
         [tHeaderView addSubview:itemIdLabel];
         [itemIdLabel release];*/
        
        /*UILabel *itemSqFeetLabel = [[UILabel alloc] initWithFrame:CGRectMake(430, 0, 480, 35)];
         itemSqFeetLabel.text = @"Sq.Ft";
         itemSqFeetLabel.textAlignment = UITextAlignmentCenter;
         itemSqFeetLabel.backgroundColor = [UIColor clearColor];
         itemSqFeetLabel.textColor = [UIColor whiteColor];
         [tHeaderView addSubview:itemSqFeetLabel];
         [itemSqFeetLabel release];*/
        
        UILabel *itemNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
        itemNameLabel.text = @"Product Description";
        itemNameLabel.textAlignment = UITextAlignmentCenter;
        itemNameLabel.backgroundColor = [UIColor clearColor];
        itemNameLabel.textColor = [UIColor whiteColor];
        itemNameLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemNameLabel];
        [itemNameLabel release];
        
        UILabel *itemWidth_DropLabel = [[UILabel alloc] initWithFrame:CGRectMake(160, 0, 210, 30)];
        itemWidth_DropLabel.text = @"Width x Drop";
        itemWidth_DropLabel.textAlignment = UITextAlignmentCenter;
        itemWidth_DropLabel.backgroundColor = [UIColor clearColor];
        itemWidth_DropLabel.textColor = [UIColor whiteColor];
        itemWidth_DropLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemWidth_DropLabel];
        [itemWidth_DropLabel release];
        
        UILabel *itemQuantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(210, 0, 330, 35)];
        itemQuantityLabel.text = @"Quantity";
        itemQuantityLabel.textAlignment = UITextAlignmentCenter;
        itemQuantityLabel.backgroundColor = [UIColor clearColor];
        itemQuantityLabel.textColor = [UIColor whiteColor];
        itemQuantityLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemQuantityLabel];
        [itemQuantityLabel release];
        
        
        UILabel *itemAreaLabel = [[UILabel alloc] initWithFrame:CGRectMake(320, 0, 270, 35)];
        itemAreaLabel.text = @"Area";
        itemAreaLabel.textAlignment = UITextAlignmentCenter;
        itemAreaLabel.backgroundColor = [UIColor clearColor];
        itemAreaLabel.textColor = [UIColor whiteColor];
        itemAreaLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemAreaLabel];
        [itemAreaLabel release];    
        
        UILabel *orderPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(340, 0, 390, 30)];
        orderPriceLabel.text = @"Unit Price";
        orderPriceLabel.textAlignment = UITextAlignmentCenter;
        orderPriceLabel.backgroundColor = [UIColor clearColor];
        orderPriceLabel.textColor = [UIColor whiteColor];
        orderPriceLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:orderPriceLabel];
        [orderPriceLabel release];
        
        UILabel *orderControlLabel = [[UILabel alloc] initWithFrame:CGRectMake(400, 0, 450, 30)];
        orderControlLabel.text = @"Control";
        orderControlLabel.textAlignment = UITextAlignmentCenter;
        orderControlLabel.backgroundColor = [UIColor clearColor];
        orderControlLabel.textColor = [UIColor whiteColor];
        orderControlLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:orderControlLabel];
        [orderControlLabel release];
        
        UILabel *orderColorLabel = [[UILabel alloc] initWithFrame:CGRectMake(460, 0, 510, 30)];
        orderColorLabel.text = @"Color";
        orderColorLabel.textAlignment = UITextAlignmentCenter;
        orderColorLabel.backgroundColor = [UIColor clearColor];
        orderColorLabel.textColor = [UIColor whiteColor];
        orderColorLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:orderColorLabel];
        [orderColorLabel release];
        
        UILabel *itemDiscountLabel = [[UILabel alloc] initWithFrame:CGRectMake(520, 0, 570, 30)];
        itemDiscountLabel.text = @"Discount";
        itemDiscountLabel.textAlignment = UITextAlignmentCenter;
        itemDiscountLabel.backgroundColor = [UIColor clearColor];
        itemDiscountLabel.textColor = [UIColor whiteColor];
        itemDiscountLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemDiscountLabel];
        [itemDiscountLabel release];
        
        UILabel *orderTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(580, 0, 630, 30)];
        orderTotalLabel.text = @"Total Price";
        orderTotalLabel.textAlignment = UITextAlignmentCenter;
        orderTotalLabel.backgroundColor = [UIColor clearColor];
        orderTotalLabel.textColor = [UIColor whiteColor];
        orderTotalLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:orderTotalLabel];
        [orderTotalLabel release];
        
        UIImage *backButtonImage = [UIImage imageNamed:@"btn_cancel_circle.png"];
        UIButton *tBackButton = [[UIButton alloc] initWithFrame:CGRectMake(985, 0, 29, 30)];
        [tBackButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
        [tBackButton addTarget:self action:@selector(back_ButtonTapped:)
              forControlEvents:UIControlEventTouchDown];
        [tHeaderView addSubview:tBackButton];
        [tBackButton release];
    }
    //else if(salesoderTypes==1)
    //{
    else if(appDelegate.TransferOrdersalesOrder.Type==SalesOrderMain)
    {
        UILabel *itemNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 350, 30)];
        itemNameLabel.text = @"Product Description";
        itemNameLabel.textAlignment = UITextAlignmentCenter;
        itemNameLabel.backgroundColor = [UIColor clearColor];
        itemNameLabel.textColor = [UIColor whiteColor];
        itemNameLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemNameLabel];
        [itemNameLabel release];
        
        UILabel *itemQuantityLabel = [[UILabel alloc] initWithFrame:CGRectMake(360, 0, 410, 30)];
        itemQuantityLabel.text = @"Quantity";
        itemQuantityLabel.textAlignment = UITextAlignmentCenter;
        itemQuantityLabel.backgroundColor = [UIColor clearColor];
        itemQuantityLabel.textColor = [UIColor whiteColor];
        itemQuantityLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemQuantityLabel];
        [itemQuantityLabel release];
        
        UILabel *orderPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(420, 0, 470, 30)];
        orderPriceLabel.text = @"Unit Price";
        orderPriceLabel.textAlignment = UITextAlignmentCenter;
        orderPriceLabel.backgroundColor = [UIColor clearColor];
        orderPriceLabel.textColor = [UIColor whiteColor];
        orderPriceLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:orderPriceLabel];
        [orderPriceLabel release];
        
        UILabel *itemDiscountLabel = [[UILabel alloc] initWithFrame:CGRectMake(480, 0, 530, 30)];
        itemDiscountLabel.text = @"Discount";
        itemDiscountLabel.textAlignment = UITextAlignmentCenter;
        itemDiscountLabel.backgroundColor = [UIColor clearColor];
        itemDiscountLabel.textColor = [UIColor whiteColor];
        itemDiscountLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:itemDiscountLabel];
        [itemDiscountLabel release];
        
        UILabel *orderTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(540, 0, 590, 30)];
        orderTotalLabel.text = @"Total Price";
        orderTotalLabel.textAlignment = UITextAlignmentCenter;
        orderTotalLabel.backgroundColor = [UIColor clearColor];
        orderTotalLabel.textColor = [UIColor whiteColor];
        orderTotalLabel.font=[UIFont boldSystemFontOfSize:17];
        [tHeaderView addSubview:orderTotalLabel];
        [orderTotalLabel release];
        
        UIImage *backButtonImage = [UIImage imageNamed:@"btn_cancel_circle.png"];
        UIButton *tBackButton = [[UIButton alloc] initWithFrame:CGRectMake(985, 0, 29, 31)];
        [tBackButton setBackgroundImage:backButtonImage forState:UIControlStateNormal];
        [tBackButton addTarget:self action:@selector(back_ButtonTapped:)
              forControlEvents:UIControlEventTouchDown];
        [tHeaderView addSubview:tBackButton];
        [tBackButton release];
    }
    
    
    [reviewUIView addSubview:tHeaderView];
    [tHeaderView release];
    [reviewUIView addSubview:reviewTableView];
    [self.view addSubview:reviewUIView];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==kCancelAlertTag)
    {
        if(buttonIndex == 1)
        {
            KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            [appDelegate clearSalesOrder];
            
            UIViewController* newOrderCategoryViewController=nil;
            for(UIViewController* viewController in self.navigationController.viewControllers)
            {
                if([viewController isKindOfClass:[OrderListViewController class]])
                {
                    newOrderCategoryViewController=viewController;
                    break;
                }
            }
            [self.navigationController popToViewController:newOrderCategoryViewController animated:YES];
            
            /*OrderListViewController *orderListViewController=[[OrderListViewController alloc] initWithNibName:@"OrderListViewController" bundle:nil];
            orderListViewController.title=@"Felton";
            [self.navigationController pushViewController:orderListViewController animated:YES];
            [orderListViewController release];*/

            
        }
    }
    else if(alertView.tag==kConfirmingAlertTag)
    {
        if(buttonIndex == 1)
        {
            KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
            appDelegate.TransferOrdersalesOrder.grand_Price = grandPrice;
            appDelegate.TransferOrdersalesOrder.order_discount = discountOnTotalPrice;
            appDelegate.TransferOrdersalesOrder.remark = remarkTextView.text;
            KDSDataSalesOrder *dataSalesOrder;
            //KDSDataSalesPerson *dataSalesPerson = [[KDSDataSalesPerson alloc] init];
            
            if (appDelegate.TransferOrdersalesOrder.Type==SalesOrderMain) {
                dataSalesOrder=[[KDSDataSalesOrder alloc] init];
                
                appDelegate.TransferOrdersalesOrder.DeliveryDate=DeliveryDateTextField.text;
            }
            else if(appDelegate.TransferOrdersalesOrder.Type==SalesOrderBlind)
            {
                appDelegate.TransferOrdersalesOrder.OrderDescription=DescripttionTextView.text;
                dataSalesOrder=[[KDSDataBlindSalesOrder alloc] init];
                float installationCost = 0.0f;
                float transportationCost = 0.0f;
                if(![transportationTextField.text isEqualToString:@""])
                {
                    transportationCost=[transportationTextField.text floatValue];
                }
                if(![installationTextField.text isEqualToString:@""])
                {
                    installationCost=[installationTextField.text floatValue];
                }
                ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).installation = installationCost;
                ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).transportation = transportationCost;
                if(installationNameTextField.text != nil)
                {
                    NSString *installationNameText=[[NSString alloc]initWithFormat:@"%@",installationNameTextField.text];
                    ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_Name = installationNameText;
                }
                
                if(installationAddressTextView.text != nil)
                {
                    //(((BlindSalesOrder*)appDelegate.salesOrder).Installation_Address).City = cityTextField.text;
                    NSString *installationAddressText=[[NSString alloc]initWithFormat:@"%@",installationAddressTextView.text];
                    ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installationorder_Address = installationAddressText;
                    
                }
                
                if(![dateTextField.text isEqualToString:@""])
                {
                    //(((BlindSalesOrder*)appDelegate.salesOrder).Installation_Address).City = cityTextField.text;
                    ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_City = dateTextField.text;
                }else {
                    ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_City=@" ";
                }
                
                if(timeTextField.text != nil)
                {
                    ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_State = timeTextField.text;
                }
                
                
                if(telTextField.text != nil)
                {
                    ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_Phone = telTextField.text;
                }
                if(faxTextField.text != nil)
                {
                    ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_Fax = faxTextField.text;
                }
                if(emailTextField.text != nil)
                {
                    ((BlindSalesOrder*)appDelegate.TransferOrdersalesOrder).Installation_Email = emailTextField.text;
                }
            }
            if(appDelegate.TransferOrdersalesOrder.Order_id!=-1)
            {
                [dataSalesOrder deleteSaleOrder:appDelegate.TransferOrdersalesOrder];
            }
            [dataSalesOrder insertSalesOrder:appDelegate.TransferOrdersalesOrder forSalesperson:appDelegate.loggedInSalesPerson];
            appDelegate.loggedInSalesPerson.orderId_Prefix = [self generateOrderIdPrefixforOrderId:appDelegate.TransferOrdersalesOrder.Order_id];
            [dataSalesOrder updateSaleOrder:appDelegate.TransferOrdersalesOrder ForSaleOrderId:appDelegate.loggedInSalesPerson];
            [dataSalesOrder release];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Confirmed" message:@"Your order is confirmed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag=kConfirmedAlertTag;
            [alert show];
            [alert release];
        }
    }
    else if(alertView.tag==kConfirmedAlertTag)
    {
        KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
        [appDelegate clearSalesOrder];
        
        UIViewController* menuViewController=nil;
        for(UIViewController* viewController in self.navigationController.viewControllers)
        {
            if([viewController isKindOfClass:[MenuViewController class]])
            {
                menuViewController=viewController;
                break;
            }
        }
        [self.navigationController popToViewController:menuViewController animated:YES];
    }
}

-(IBAction)back_ButtonTapped:(id)sender
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.4];
    reviewUIView.alpha = 0.0;
    [UIView commitAnimations];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    heightOfEditedArea = textField.frame.size.height;
    heightOffset=textField.frame.origin.y;
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOffset+ heightOfEditedArea), self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
}
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    heightOfEditedArea = textView.frame.size.height;
    heightOffset=textView.frame.origin.y;
    CGRect rectToShow = CGRectMake(self.view.frame.origin.x, 352-(heightOffset+ heightOfEditedArea), self.view.frame.size.width, self.view.frame.size.height);
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.2];
    self.view.frame = rectToShow;
    [UIView commitAnimations];
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    alertUser.hidden=NO;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    alertUser.hidden=NO;
}

-(void)loadDataFinished
{
    if(numberOfRows != 0)
    {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:numberOfRows-1 inSection:0];
        [self._tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        [self updateCart];
        //[self updatetotalprice];
    }
    
}

-(void)updateAlertText
{
    alertUser.hidden = NO;
    
}
-(void)scrollToItem:(NSIndexPath *)indexPath
{
    [UIView beginAnimations:@"" context:nil];
    [UIView setAnimationDuration:0.4];
    reviewUIView.alpha = 0.0;
    [UIView commitAnimations];
    [_tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    //[_tableView selectRowAtIndexPath:indexPath animated:YES scrollPosition:UITableViewScrollPositionTop];
    
}
-(void)focusOnCell:(NSIndexPath*)indexPath
{
    for(int i=0;i<[_tableView visibleCells].count;i++)
    {
        if(i == indexPath.row)
        {
            GridTableView* cell = [[_tableView visibleCells] objectAtIndex:i];
            cell.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.75 alpha:0.6];
        }
    }
}


- (NSIndexPath *)indexPathForCellController:(id)cellController
{
    float sumOfOrderItemPrice = 0.0;
	NSInteger sectionIndex;
	NSInteger sectionCount = [orderItemsArr count];
	for (sectionIndex = 0; sectionIndex < sectionCount; sectionIndex++)
	{
		NSArray *section = [orderItemsArr objectAtIndex:sectionIndex];
		NSInteger rowIndex;
		NSInteger rowCount = [section count];
		for (rowIndex = 0; rowIndex < rowCount; rowIndex++)
		{
			NSArray *row = [section objectAtIndex:rowIndex];
			if ([row isEqual:cellController])
			{
				return [NSIndexPath indexPathForRow:rowIndex inSection:sectionIndex];
                sumOfOrderItemPrice += salesOrderItem.TotalPrice;
                NSLog(@"sumOfOrderItemPrice--->%@",sumOfOrderItemPrice);
			}
		}
	}
	
	return nil;
}

-(void)updateCart 
{
    KDSAppDelegate* appDelegate=[UIApplication sharedApplication].delegate;
    self.orderItemsArr=[appDelegate.TransferOrdersalesOrder order_Items];
    //NSLog(@"update cart Category--->%d",appDelegate.salesOrder.Type);
    int salesoderTypes=appDelegate.TransferOrdersalesOrder.Type;
    
    float sumOfOrderItemPrice = 0.0f;
    float transportationCost=0.0f;
    float installationCost=0.0f;
    float sumOfOrderItemPrice8 = 0.0f;
    float sumOfOrderItemPriceedit=0.0f;
    
    for(int i=0;i<orderItemsArr.count;i++)
    {
        //GridTableView* cell=[_tableView cellForRowAtIndexPath: [(GridTableView *)tableView.delegate indexPathForCellController:self]];
        //salesOrderItem.TotalPrice; [_tableView cellForRowAtIndexPath:i]
        //NSString *orderItemsArrTOTAL=orderItemsArr;
        //sumOfOrderItemPrice += ;
        
        SalesOrderItem* orderItem2=[[appDelegate.TransferOrdersalesOrder order_Items] objectAtIndex:i];
        BlindSalesOrderItem* blindorderItem2=[[appDelegate.TransferOrdersalesOrder order_Items] objectAtIndex:i];
        //SalesOrder* SorderItemType=[[appDelegate.salesOrder order_Items] objectAtIndex:i];
        /*NSLog(@"Unit_Price--->%.2f",orderItem2.Unit_Price);
         NSLog(@"Quantity--->%.2f",orderItem2.Quantity);
         NSLog(@"TotalPrice--->%.2f",orderItem2.TotalPrice);
         NSLog(@"Discount_Amount--->%.2f",orderItem2.Discount_Amount);
         NSLog(@"Discount_Percent--->%.2f",orderItem2.Discount_Percent);
         NSLog(@"TotalPrice--->%.2f",orderItem2.TotalPrice);*/
        
        //NSLog(@"Category--->%@",orderItem2.item.Category);
        
        //NSLog(@"SorderItemType--->%@",blindorderItem2.item.Category);
        
        
        
        
        if (orderItem2.Unit_Price) 
        {
            if ([self.EditTag isEqualToString:@"edittag"]) 
            {
                
                if (salesoderTypes==0) 
                {
                    
                    
                    sumOfOrderItemPrice8 = (orderItem2.Quantity*orderItem2.Unit_Price);
                    
                    if (orderItem2.Discount_Percent != 0.0) 
                    {
                        //salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                        totalDiscountOfItems=(orderItem2.Discount_Percent*orderItem2.Unit_Price*orderItem2.Quantity)/100;
                        sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                    }else if(orderItem2.Discount_Amount != 0.0)
                    {
                        //salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                        totalDiscountOfItems=orderItem2.Discount_Amount;
                        sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                    }else 
                    {
                        sumOfOrderItemPriceedit =sumOfOrderItemPrice8;
                        //sumOfOrderItemPrice += orderItem2.TotalPrice;
                    }
                    sumOfOrderItemPrice+=sumOfOrderItemPriceedit;
                    
                    
                }else if (salesoderTypes==1)
                {
                    
                    //NSLog(@"Quantity_in_Set--->%.2f",blindorderItem2.Quantity_in_Set);
                    
                    if (blindorderItem2.Quantity_in_Set != 0) {
                        NSString* blindorderItem2Quantity_in_Set = [NSString stringWithFormat:@"%.2f", blindorderItem2.Quantity_in_Set];
                        NSString* orderItem2Unit_Price = [NSString stringWithFormat:@"%.2f", orderItem2.Unit_Price];
                        
                        sumOfOrderItemPrice8 = [blindorderItem2Quantity_in_Set floatValue] *[orderItem2Unit_Price floatValue] ;
                        
                        
                        sumOfOrderItemPrice8=sumOfOrderItemPrice8*orderItem2.Quantity;

                        
                        if (orderItem2.Discount_Percent != 0.0) 
                        {
                            //salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                            totalDiscountOfItems=(orderItem2.Discount_Percent*orderItem2.Unit_Price*blindorderItem2.Quantity_in_Set)/100;
                            sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else if(orderItem2.Discount_Amount != 0.0)
                        {
                            //salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                            totalDiscountOfItems=orderItem2.Discount_Amount;
                            sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else 
                        {
                            sumOfOrderItemPriceedit =sumOfOrderItemPrice8;
                            //sumOfOrderItemPrice += orderItem2.TotalPrice;
                        }
                        sumOfOrderItemPrice+=sumOfOrderItemPriceedit;
                        
                    }else {
                        sumOfOrderItemPrice8 = (blindorderItem2.Quantity*orderItem2.Unit_Price);
                        
                        if (orderItem2.Discount_Percent != 0.0) 
                        {
                            //salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                            totalDiscountOfItems=(orderItem2.Discount_Percent*orderItem2.Unit_Price*blindorderItem2.Quantity)/100;
                            sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else if(orderItem2.Discount_Amount != 0.0)
                        {
                            //salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                            totalDiscountOfItems=orderItem2.Discount_Amount;
                            sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else 
                        {
                            sumOfOrderItemPriceedit =sumOfOrderItemPrice8;
                            //sumOfOrderItemPrice += orderItem2.TotalPrice;
                        }
                        sumOfOrderItemPrice+=sumOfOrderItemPriceedit;
                        
                    }
                    
                    
                }
                
                
            }else if (![self.EditTag isEqualToString:@"edittag"]) 
            {
                if (salesoderTypes==0) 
                {
                    float sumOfOrderItemPriceedit=0.0;
                    sumOfOrderItemPrice8 = (orderItem2.Quantity*orderItem2.Unit_Price);
                    
                    if (orderItem2.Discount_Percent != 0.0) 
                    {
                        //salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                        totalDiscountOfItems=(orderItem2.Discount_Percent*orderItem2.Unit_Price*orderItem2.Quantity)/100;
                        sumOfOrderItemPriceedit= sumOfOrderItemPrice8 - totalDiscountOfItems;
                    }else if(orderItem2.Discount_Amount != 0.0)
                    {
                        //salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                        totalDiscountOfItems=orderItem2.Discount_Amount;
                        sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                    }else 
                    {
                        sumOfOrderItemPriceedit =sumOfOrderItemPrice8;
                        //sumOfOrderItemPrice += orderItem2.TotalPrice;
                    }
                    sumOfOrderItemPrice+=sumOfOrderItemPriceedit;
                    
                }else if (salesoderTypes==1)
                {
                    //NSLog(@"Quantity_in_Set--->%.2f",blindorderItem2.Quantity_in_Set);
                    if (blindorderItem2.Quantity_in_Set != 0) {
                        float sumOfOrderItemPriceedit=0.0;
                        sumOfOrderItemPrice8 = (blindorderItem2.Quantity_in_Set*orderItem2.Unit_Price);
                        
                        if (orderItem2.Discount_Percent != 0.0) 
                        {
                            //salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                            totalDiscountOfItems=(orderItem2.Discount_Percent*orderItem2.Unit_Price*blindorderItem2.Quantity_in_Set)/100;
                            sumOfOrderItemPriceedit= sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else if(orderItem2.Discount_Amount != 0.0)
                        {
                            //salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                            totalDiscountOfItems=orderItem2.Discount_Amount;
                            sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else 
                        {
                            sumOfOrderItemPriceedit =sumOfOrderItemPrice8;
                            //sumOfOrderItemPrice += orderItem2.TotalPrice;
                        }
                        sumOfOrderItemPrice+=sumOfOrderItemPriceedit;
                        
                    }else {
                        float sumOfOrderItemPriceedit=0.0;
                        sumOfOrderItemPrice8 = (blindorderItem2.Quantity*orderItem2.Unit_Price);
                        
                        if (orderItem2.Discount_Percent != 0.0) 
                        {
                            //salesOrderItem.Discount_Percent=[discountTextField.text floatValue];
                            totalDiscountOfItems=(orderItem2.Discount_Percent*orderItem2.Unit_Price*blindorderItem2.Quantity)/100;
                            sumOfOrderItemPriceedit= sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else if(orderItem2.Discount_Amount != 0.0)
                        {
                            //salesOrderItem.Discount_Amount=[discountTextField.text floatValue];
                            totalDiscountOfItems=orderItem2.Discount_Amount;
                            sumOfOrderItemPriceedit = sumOfOrderItemPrice8 - totalDiscountOfItems;
                        }else 
                        {
                            sumOfOrderItemPriceedit =sumOfOrderItemPrice8;
                            //sumOfOrderItemPrice += orderItem2.TotalPrice;
                        }
                        sumOfOrderItemPrice+=sumOfOrderItemPriceedit;
                        
                    }
                    
                }
                
                
            }
            
        }
        
    }
    
    totalPricce = sumOfOrderItemPrice;
    
    
    //NSLog(@"sumOfOrderItemPrice--->%.2f",sumOfOrderItemPrice);
    
    //NSLog(@"totalPricce--->%.2f",totalPricce);
    NSString* totalPriceWithDiscountStr = [NSString stringWithFormat:@"%.2f", totalPricce];
    
    if (sumOfOrderItemPrice) {
        
        
        
        
        if(![transportationTextField.text isEqualToString:@""])
        {
            transportationCost=[transportationTextField.text floatValue];
        }
        
        if(![installationTextField.text isEqualToString:@""])
        {
            installationCost=[installationTextField.text floatValue];
        }
        
        if([totalDiscountTextField.text floatValue]>totalPricce)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Warning!" message:@"The Amount of Total Discount is more than the Total Price." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
            [alert release];
            totalDiscountTextField.text = [NSString stringWithFormat:@"%d", 0];
            
            discountOnTotalPrice=[totalDiscountTextField.text floatValue];
            
        }else if([totalDiscountTextField.text floatValue]<totalPricce)
        {
            discountOnTotalPrice=[totalDiscountTextField.text floatValue];
        }
        
        appDelegate.TransferOrdersalesOrder.order_discount=discountOnTotalPrice;
        
        
        totalPriceLabel.text = totalPriceWithDiscountStr;
        
        grandTotalLabel.text = totalPriceWithDiscountStr;
        
        grandPrice = totalPricce - discountOnTotalPrice+transportationCost+installationCost;
        NSString* grandPriceStr = [NSString stringWithFormat:@"%.2f", grandPrice];
        grandTotalLabel.text = grandPriceStr;
        shouldUpdateCart=NO;
    }
    
}


-(IBAction)cancelButtonTapped
{
    /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Order Cancel" message:@"Please Confirm cancelling your order." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alert.tag=kCancelAlertTag;
    [alert show];
    [alert release];*/
    
    KDSAppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
    [appDelegate clearSalesOrder];
    
    UIViewController* newOrderCategoryViewController=nil;
    for(UIViewController* viewController in self.navigationController.viewControllers)
    {
        if([viewController isKindOfClass:[OrderListViewController class]])
        {
            newOrderCategoryViewController=viewController;
            break;
        }
    }
    [self.navigationController popToViewController:newOrderCategoryViewController animated:YES];

    
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)dealloc 
{
    [_tableView release];
    [remarkTextView release];
    [DeliveryDateTextField release];
    
    [UpdaCartPrice release];
    [EditCartSmallView release];
    [editCartViewcoltroller release];
    [UpdateCartUIView release];
    [allsalesItem release];
    [orderItem release];
    [orderItemsArr release];
    [backButton release];
    [reviewTableView release];
    [reviewTableViewController release];
    [reviewUIView release];
   
    [backgroundImageView release];
    [tableViewHeader release];
    [installationTextField release];
    [cityTextField release];
    [stateTextField release];
    [dateTextField release];
    [timeTextField release];
    [telTextField release];
    [faxTextField release];
    [emailTextField release];
    [transportationTextField release];
    
    
    [grandTotalLabel release];
    [orderIdLabel release];
    [orderIdValueLabel release];
    [totalPriceLabel release];
    [CurrentSaleOrder release];
    [totalDiscountTextField release];
    [lbltransportation release];
    [lblinstallation release];
    [lblinstallationName release];
    [lblinstallationAddress release];
    [lbldate release];
    [lbltime release];
    [lbltel release];
    [lblfax release];
    [lblcontact release];
    
    [lbldeliveryDate release];
    [DescripttionTextView release];
    [lblDescription release];
    
    [lblcustomerCompanyName release];
    [lblcustomerTel release];
    [txtcusRemark release];
    [lblcusRemark release];
    [txtorderref release];
    [installationAddressTextView release];
    [lblreference release];
    [ShippingAddress release];
    [lblshippingAddress release];
    [lblPO release];
    [txtPO release];
    [mainScrolView release];
    [btnordersummary release];
    [lblremarkmain release];
    [lblQtymain release];
    [lblunitPricemain release];
    [lbldiscountmain release];
    [lbltotalmain release];
    [lbldescblind release];
    [lblcontrolblind release];
    [lblqtyblind release];
    [lblsqftblind release];
    [lbltotalsqftblind release];
    [lblunitpriceblind release];
    [lbldiscountblind release];
    [lbltotalblind release];
    [lbldeliverydateblind release];
    [txtdeliveryDateblind release];
    [super dealloc];
}
@end
