//
//  CustomerListCell.m
//  KDS
//
//  Created by Tiseno Mac 2 on 2/6/13.
//
//

#import "CustomerListCell.h"

@implementation CustomerListCell
@synthesize companyNameIDLabel, companyNameLabel, orderHistoryButton;
@synthesize customer;

-(void)dealloc
{
    [orderHistoryButton release];
    [customer release];
    [companyNameLabel release];
    [companyNameIDLabel release];
    [super dealloc];
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        UILabel *tCompanyNameIDLabel = [[[UILabel alloc] initWithFrame:CGRectMake(20, 0, 150, 45)] autorelease];
        tCompanyNameIDLabel.textAlignment = UITextAlignmentLeft;
        tCompanyNameIDLabel.adjustsFontSizeToFitWidth = YES;
        tCompanyNameIDLabel.minimumFontSize=10;
        tCompanyNameIDLabel.backgroundColor = [UIColor clearColor];
        tCompanyNameIDLabel.textColor = [UIColor blackColor];
        tCompanyNameIDLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        self.companyNameIDLabel = tCompanyNameIDLabel;
        [self addSubview:companyNameIDLabel];
        
        
        UILabel *tCompanyNameLabel = [[[UILabel alloc] initWithFrame:CGRectMake(90, 0, 350, 45)] autorelease];
        tCompanyNameLabel.textAlignment = UITextAlignmentLeft;
        tCompanyNameLabel.adjustsFontSizeToFitWidth = YES;
        tCompanyNameLabel.minimumFontSize=10;
        tCompanyNameLabel.backgroundColor = [UIColor clearColor];
        tCompanyNameLabel.textColor = [UIColor blackColor];
        tCompanyNameLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
        self.companyNameLabel = tCompanyNameLabel;
        [self addSubview:companyNameLabel];
        
        /*
        UIImage *orderHistoryButtonImage = [UIImage imageNamed:@"btn_order_history.png"];
        UIButton *tOrderHistoryButton = [[[UIButton alloc] initWithFrame:CGRectMake(375, 0, 76, 42)] autorelease];
        [tOrderHistoryButton setBackgroundImage:orderHistoryButtonImage forState:UIControlStateNormal];
        //[tOrderHistoryButton addTarget:self action:@selector(orderHistory_ButtonTapped:) forControlEvents:UIControlEventTouchDown];
        self.orderHistoryButton = tOrderHistoryButton;
        [self addSubview:orderHistoryButton];
        */
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
