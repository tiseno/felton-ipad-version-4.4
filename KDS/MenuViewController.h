//
//  MenuViewController.h
//  KDS
//
//  Created by Tiseno on 11/21/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KDSSuperViewController.h"
#import "NewOrderViewController.h"
#import "NewOrderCategoryViewController.h"
//#import "OrderListViewController.h"
#import "SynchDataViewController.h"
#import "OrderListTransfered.h"
#import "ListOfProductsViewController.h"
#import "CustomerListViewController.h"
#import "KDSAppDelegate.h"
#import "LoginViewController.h"
#import "YourCartViewController.h"
#import "OrderListTransferedsystem.h"
#import "OrderMenupage.h"
#import "KDSDataListGallery.h"

#import "NewCustomerListViewController.h"

@interface MenuViewController : UIViewController {
    
    UINavigationController *navController;
    LoadingView *loadingView;
    
    id<KDSSuperViewController> superViewController;
}
@property (nonatomic, retain) UINavigationController *navController;
@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic, retain) id<KDSSuperViewController> superViewController;
-(void)listView;
-(void)galleryView;

@end
