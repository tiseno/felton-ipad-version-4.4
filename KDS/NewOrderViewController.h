//
//  NewOrderViewController.h
//  KDS
//
//  Created by Tiseno on 1/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductTableCell.h"
#import "NSDictionary-MutableDeepCopy.h"
#import "KDSAppDelegate.h"
#import "NewOrder_ConfirmViewController.h"
#import "LoadingView.h"
#import "KDSDataListGallery.h"
#import "YourCartViewController.h"

@interface NewOrderViewController : UIViewController <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource> {
    
    UILabel *customerNameLabel;
    UILabel *companyNameLabel;
    UILabel *telLabel;
    UILabel *faxLabel;
    UILabel *emailLabel;
    
    NSMutableDictionary *names;
    NSDictionary *allNames;
    
    NSMutableArray *keys;
    
    BOOL searchById, searchByName;
    int heightOfEditedView;
    int heightOffset;
    
    UISearchBar *search; 
    
    UISegmentedControl *searchMethodSC;
    
    UITableView *productTableView;
    
    LoadingView *loadingView;
    
    int counterA;
    bool startA;
    NSString *_CountDownTime;
    NSTimer *timerA; 
    //IBOutlet UILabel *secondsA;
    BOOL loadData;
    UIView *tableViewHeader;
}
-(IBAction)viewYourCartButtonTapped;
@property (nonatomic, retain) NSString *lstGallery;
@property (nonatomic,retain) UIView *selfView;
@property (nonatomic, retain) UIView *tableViewHeader;
@property (retain, nonatomic) IBOutlet UITableView *customerDetailtable;
@property (nonatomic, retain) LoadingView *loadingView;
@property (nonatomic,retain) NSString *_CountDownTime;

//@property (nonatomic, retain) KDSAppDelegate* appDelegate;
@property (retain, nonatomic) IBOutlet UISegmentedControl *listGallery;

@property (nonatomic) BOOL searchById, searchByName;
@property (nonatomic, retain) IBOutlet UISearchBar *search;
@property (nonatomic, retain) IBOutlet UISegmentedControl *searchMethodSC;
@property (nonatomic, retain) IBOutlet UILabel *customerNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *companyNameLabel;
@property (nonatomic, retain) IBOutlet UILabel *telLabel;
@property (nonatomic, retain) IBOutlet UILabel *faxLabel;
@property (nonatomic, retain) IBOutlet UILabel *emailLabel;
@property (nonatomic, retain) IBOutlet UITableView *productTableView;
@property (nonatomic, retain) NSMutableDictionary *names;
@property (nonatomic, retain) NSMutableArray *keys;
@property (nonatomic, retain) NSDictionary *allNames;
@property (retain, nonatomic) IBOutlet UISwitch *PrefixOnOff;
-(IBAction)GalleryTapped;
-(void)searchBarSearchButtonClicked:(UISearchBar *)searcchBar;
@end
