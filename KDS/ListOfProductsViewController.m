//
//  ListOfProductsViewController.m
//  KDS
//
//  Created by Tiseno on 12/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ListOfProductsViewController.h"


@implementation ListOfProductsViewController
@synthesize PrefixOnOff,loadingView;
@synthesize _CountDownTime;
@synthesize secondsA;

@synthesize _tableView, search, names, keys, allNames, searchById, searchByName, searchMethodSc, searchBar;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        loadData=NO;
    }
    return self;
}

- (void)dealloc
{
    [loadingView release];
    [searchBar release];
    [searchMethodSc release];
    [_tableView release];
    [search release];
    [names release];
    [keys release];
    //[allNames release];
    [PrefixOnOff release];
    [secondsA release];
    [super dealloc];
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(IBAction)searchMethodScTapped
{
    /*if(self.searchMethodSc.selectedSegmentIndex==0)
    {
        searchById = YES;
        if(![searchBar.text isEqualToString:@""] && searchBar.text != nil)
        {
            [self searchBarSearchButtonClicked:searchBar];
            [_tableView reloadData];
        }
        
    }
    else if(self.searchMethodSc.selectedSegmentIndex==1)
    {
        searchById = NO;
        if(![searchBar.text isEqualToString:@""] && searchBar.text != nil)
        {
            [self searchBarSearchButtonClicked:searchBar];
            [_tableView reloadData];
        }
    }*/
    
    if(self.searchMethodSc.selectedSegmentIndex == 0)
    {
        searchByName = YES;
        searchById = NO;
        
        if(![searchBar.text isEqualToString:@""] && searchBar.text != nil)
        {
            [self searchBarSearchButtonClicked:searchBar];
            [_tableView reloadData];
        }
        
    }
    else if(self.searchMethodSc.selectedSegmentIndex == 1)
    {
        searchById = YES;
        searchByName = NO;
        
        if(![searchBar.text isEqualToString:@""] && searchBar.text != nil)
        {
            [self searchBarSearchButtonClicked:searchBar];
            [_tableView reloadData];
        }
    }
    /*else if(self.searchMethodSc.selectedSegmentIndex == 2)//added new code here
    {
        searchById = NO;
        searchByName = NO;
        
        if(![searchBar.text isEqualToString:@""] && searchBar.text != nil)
        {
            [self searchBarSearchButtonClicked:searchBar];
            [_tableView reloadData];
        }
    }*/ 
}
-(void)resetSearch
{
    NSMutableDictionary *allNamesCopy = [self.allNames MutableDeepCopy];
    self.names = allNamesCopy;
    //[allNamesCopy release];
    NSMutableArray *keyArray = [[NSMutableArray alloc] init];
    [keyArray addObjectsFromArray:[[self.allNames allKeys] sortedArrayUsingSelector:@selector(compare:)]];
    self.keys = keyArray;
    [keyArray release];
}
-(void)handleSearchForTerm:(NSString*)searchTerm
{
    NSMutableArray *sectionsToRemove = [[NSMutableArray alloc] init];
    [self resetSearch];
    
    for(NSString *key in self.keys)
    {
        NSMutableArray *array = [names valueForKey:key];
        NSMutableArray *toRemove = [[NSMutableArray alloc] init];
        
        for(Item* name in array)
        {
            /*if(!searchById)
            {
                if([name.Description rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location == NSNotFound)
                [toRemove addObject:name];
            }
            else
            {
                if([name.Item_No rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location == NSNotFound)
                [toRemove addObject:name];
            }*/
            
            if (PrefixOnOff.on) 
            {
                if(searchById == NO && searchByName == YES)
                {
                    if([name.Description rangeOfString:searchTerm options:(NSAnchoredSearch | NSCaseInsensitiveSearch)].location == NSNotFound)
                        [toRemove addObject:name];
                }
                else if(searchById == YES && searchByName == NO)
                {
                    if([name.Item_No rangeOfString:searchTerm options:(NSAnchoredSearch | NSCaseInsensitiveSearch)].location == NSNotFound)
                        [toRemove addObject:name];
                }
                
                /*else
                {
                    //new code for search by prefix
                    if([name.Description rangeOfString:searchTerm options:(NSAnchoredSearch | NSCaseInsensitiveSearch)].location == NSNotFound)
                        [toRemove addObject:name];
                }*/
            }else 
            {
                if(searchById == NO && searchByName == YES)
                {
                    if([name.Description rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location == NSNotFound)
                        [toRemove addObject:name];
                }
                else if(searchById == YES && searchByName == NO)
                {
                    if([name.Item_No rangeOfString:searchTerm options:NSCaseInsensitiveSearch].location == NSNotFound)
                        [toRemove addObject:name];
                }
            }
            
            
            
        }
        
        if([array count] == [toRemove count])
            
            [sectionsToRemove addObject:key];
        
        [array removeObjectsInArray:toRemove];
        [toRemove release];
    }
    [self.keys removeObjectsInArray:sectionsToRemove];
    [sectionsToRemove release];
    [_tableView reloadData];
}


- (void)updateTimer{ //Happens every time updateTimer is called. Should occur every second.
    
    counterA -= 1;
    _CountDownTime = [NSString stringWithFormat:@"%i", counterA];
    
    if (counterA < 0) // Once timer goes below 0, reset all variables.
    {
        [timerA invalidate];
        startA = TRUE;
        //counterA = 1;
        loadData=YES;
        NSLog(@"%@",_CountDownTime);
        
        if (loadData==YES) 
        {
            //searchById = YES;
            KDSAppDelegate* appDelegate = (KDSAppDelegate*)[UIApplication sharedApplication].delegate;
            if(appDelegate.itemCategoryDictionary == nil)
            {
                
                [appDelegate loadProducts];
                
                
            }
            NSDictionary *dict = appDelegate.itemCategoryDictionary;
            self.allNames = dict;
            
            [self resetSearch];
            [_tableView reloadData];
            [_tableView setContentOffset:CGPointMake(0.0, 44.0) animated:NO];
        }

        [self.loadingView removeView];
    }
    
    
}



#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    
        
    counterA = 1;
    startA = TRUE;
    
    if(startA == TRUE) //Check that another instance is not already running.
    {
        _CountDownTime = @"1";
        startA = FALSE;
        timerA = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
        
    

    }
    
    searchById = NO;
    searchByName=YES;
        //[self resetSearch];
        //[_tableView reloadData];
        ///[_tableView setContentOffset:CGPointMake(0.0, 44.0) animated:NO];
        
       
    UIView *selfView=self.view;
    LoadingView *temploadingView =[LoadingView loadingViewInView:selfView];
    self.loadingView=temploadingView;
    
    
        
}

- (void)viewDidUnload
{
    [self setPrefixOnOff:nil];
    [self setSecondsA:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return ([keys count] > 0)?[keys count] :1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if([keys count] == 0)
        return 0;
    NSString *key = [self.keys objectAtIndex:section];
    NSArray *nameSection = [self.names objectForKey:key];
    return [nameSection count];
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSUInteger section = [indexPath section];
    NSUInteger row = [indexPath row];
    NSString *key = [self.keys objectAtIndex:section];
    NSArray *nameSection = [self.names objectForKey:key];
    
    static NSString *sectionTableIdentifier = @"SectionsTableIdentifier";
     
    ListOfProductsTableViewCellController *cell = (ListOfProductsTableViewCellController*)[tableView dequeueReusableCellWithIdentifier:sectionTableIdentifier];
    if(cell == nil)
    {
        cell = [[[ListOfProductsTableViewCellController alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:sectionTableIdentifier]autorelease];
         
    } 
    Item *item=[nameSection objectAtIndex:row];
    if(item.Default_Price!=nil &&item.Default_Price.count>0)
    {
        DefaultPriceAndUOM *defaultPrice=[item.Default_Price objectAtIndex:0];
        double dprice=[defaultPrice.Default_Price doubleValue];
        NSString *price=[NSString stringWithFormat:@"RM %.2f/ unit",dprice];
        cell.priceLabel.text = price;
    }else {
        NSString *price=[NSString stringWithFormat:@"RM 0.00/ unit"];
        cell.priceLabel.text = price;
    }
    //NSString* productIdStr = [NSString stringWithFormat:@"%d",item.Item_id];
    cell.productIdLabel.text = item.Item_No;
    cell.productNameLabel.text = item.Description;
    cell.selectionStyle = UITableViewCellEditingStyleNone;
    return cell; 
    
   
}

-(NSString*) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if([keys count] == 0)
        return nil;
    NSString *key = [self.keys objectAtIndex:section];
    return key;
} 
-(NSArray*)sectionIndexTitlesForTableView:(UITableView *)tableView
{ 
    return keys;
}
-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [search resignFirstResponder];
    return indexPath;
}
#pragma mark -
#pragma mark Search Bar Delegate Methods
-(void)searchBarSearchButtonClicked:(UISearchBar *)searcchBar
{
    NSString *searchTerm = [searcchBar text];
    [self handleSearchForTerm:searchTerm];
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if([searchText length] == 0)
    {
        [self resetSearch];
        [_tableView reloadData];
        return;
    }
    [self handleSearchForTerm:searchText];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searcchBar
{
    search.text = @"";
    [self resetSearch];
    [_tableView reloadData];
    [searcchBar resignFirstResponder];
}

@end
