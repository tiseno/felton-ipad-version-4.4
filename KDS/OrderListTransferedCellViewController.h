//
//  OrderListTransferedCellViewController.h
//  KDS
//
//  Created by Tiseno on 11/29/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KDSDataSalesOrderItem.h"
#import "SalesOrder.h"
#import "KDSAppDelegate.h"
#import "OrderListTransferedDelegate.h"

#define kConfirmingAlertTag 0
#define kConfirmedAlertTag 1

@interface OrderListTransferedCellViewController : UITableViewCell {
    
    UIColor *lineColor;
    
    UIButton *transferNowButton;
    BOOL topCell;
    UILabel *idLabel;
    UILabel *customerNameLabel;
    UILabel *priceLabel;
    UIButton *editButton;
    id<OrderListTransferedDelegate> delegate;
}
@property (nonatomic) BOOL topCell;
@property (nonatomic, retain) UIColor *lineColor;
@property (nonatomic, retain) id<OrderListTransferedDelegate> delegate;
@property (nonatomic, retain)  UIButton *editButton;
@property (nonatomic, retain) UILabel *idLabel;
@property (nonatomic, retain) UILabel *customerNameLabel;
@property (nonatomic, retain) UILabel *priceLabel;
@property (nonatomic, retain) SalesOrder* salesOrder;
@end
