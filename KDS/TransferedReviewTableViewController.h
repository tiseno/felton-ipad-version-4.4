//
//  TransferedReviewTableViewController.h
//  KDS
//
//  Created by Tiseno Mac 2 on 5/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReviewTableViewCell.h"
#import "ReviewTableViewCellBlind.h"
#import "KDSAppDelegate.h"
#import "ReviewTableViewDelegate.h"

@interface TransferedReviewTableViewController : UITableViewController<UITableViewDelegate, UITableViewDataSource> {
    
    int numberOfRows;
    id<ReviewTableViewDelegate> scrollDelegate;
}
@property (nonatomic) int numberOfRows;
@property (nonatomic, retain) id<ReviewTableViewDelegate> scrollDelegate;
@end
