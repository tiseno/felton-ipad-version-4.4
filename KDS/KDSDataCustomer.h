//
//  KDSDataCustomer.h
//  KDS
//
//  Created by Tiseno on 12/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KDSDataLayerBase.h"
#import "Customer.h"

@interface KDSDataCustomer : KDSDataLayerBase {
    BOOL runBatch;
}
@property (nonatomic)BOOL runBatch;
-(NSArray*)selectCustomerWithSalespersonID:(NSString*)salespersonID;
-(DataBaseInsertionResult)insertCustomer:(Customer*)tCustomer;
-(DataBaseUpdateResult)updateCustomer:(Customer*)tCustomer;
-(DataBaseInsertionResult)insertCustomerArray:(NSArray*)customerArr forSalespersonID:(NSString*)salesPersonID;

//-(void)updateCustomerRemark:(Customer*)tCustomer;

@end
